UC- Prices of Materials and Finishes

Module: ARQSI

1.Brief Description
The use case is based on the fact that the price of each product is based on the quantity of material it has. Each type of material has a base price per square meter and it can be incremented depending on if a finishing is applied to the product or not. The finishing's incremental price may not be the same for different materials. As such, the process of defining a price for each material and its finishes must be intuitive for the manager. At the same time, the user who is buying the product must know exactly how much he's going to pay depending on what material/finishing combination he chose as well as the quantity of both of those.

2. Flow of Events

2.1 - Product Manager
At any given time, a product manager can change a given material's base price. This price affects all products using this material. Additionally, the manager can opt to add a finishing to a material, with it's own incremental value. The final price is calculated based on the amount of material used plus it's finishing.



2.2 - Authenticated user
When buying a product, after all the configuration process, the final price value is shown to the user.
