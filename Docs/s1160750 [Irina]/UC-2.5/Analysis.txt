UC- Register User 

Module: ARQSI

1.Brief Description
The context of this use case is the creation of an account to gain acess to the application resources. In order to manage the products' catalog, 
the manager needs to have an account and be authenticated. In order to order a closet, the user can have an account already and be logged in or
it's possible to customize the closet and the request of the registration is done at the end. 

The actor who starts this use case is an user that can be a client or a product manager.

2.    Flow of Events
The use case starts when the user clicks the registration button. This button is shown in the home page and at the confirmation 
of an order. 

	2.1    Basic Flow - See Customer Orders and their status
	User inserts necessary data for authentication. 
	System validates data.
	User inserts password.
	User repeats password.
	System validates passwords and logs user into the system. 
	The system redirects user to previous page if the user was ordering a custom closet. 
	
	2.2    Alternative Flows

	Account already exists: 
	If the account already exists a warning is showed and only disappears and lets the user proceed when data is changed.  

	Password too weak: 
	Is the password chosen is too weak to meet the password requirements, a warning is shown and the user has to change the password until 
	the warning disappears. 
