UC- See Customer Orders and their status

Module: ARQSI

1.Brief Description
This use case describes how the user can check the orders made by you and the status of them.

The actor who starts this use case is the logged user.

2.    Flow of Events
The use case starts when the user loads Show Orders button.

	2.1    Basic Flow - See Customer Orders and their status
	
	The system displays the user's orders and status.
	
	2.2    Alternative Flows

	No Orders

	If in the basic flow the system can not find any order from this user, a message will be displayed informing the u
