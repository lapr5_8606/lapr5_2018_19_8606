const Order = require('../models/order');

exports.GetById = function(id) {
    return new Promise((resolve, reject) => {
        Order.findById(id, function(err, order) {
            if(err) reject(err);
            resolve(order);
        });
    });
}

exports.SaveOrder = function(order) {
    order.save(function(err) {
        console.log(err);
        if(err) return next(err);

    });
};

exports.DeleteOrder = function(id) {
    return new Promise((resolve, reject) => {
        Order.findByIdAndDelete(id, function(err, order) {
            if(err) reject(err);
            resolve(order);
        });
    })
}
//added by joao lapr5
exports.GetAllByClientId = function(_clientId){
    return new Promise((resolve,reject) =>{
        Order.find({clientId:_clientId},function(err,orders){
            if(err) reject(err);
            resolve(orders);
        })
    });
}

exports.GetAllOrders = function(){
    return new Promise((resolve,reject) =>{
        Order.find(function(err,orders){
            if(err) reject(err);
            resolve(orders);
        })
    });
}

exports.GetAllReadyToDeliverOrders = function(){
    return new Promise((resolve,reject) =>{
        Order.find({status:'readyDeliver'},function(err,orders){
            if(err) reject(err);
            resolve(orders);
        })
    });
}