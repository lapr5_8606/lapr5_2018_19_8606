const orderRepository = require('../repository/orderRepository')
const factoryService = require('../services/factoryService')

exports.GetAllReadyToDeliverOrders = async function () {
    var orders = await orderRepository.GetAllReadyToDeliverOrders();

    var factoriesWithOrders = allFactoriesWithCitiesFromOrders(orders);

    var allPathsWithDistances = [];

    if (factoriesWithOrders != null) {
        for (var factory in factoriesWithOrders) {
            var allData = await factoryService.getBestPathForDeliveryForCity(factory, factoriesWithOrders[factory]);
            
            if(allData)
                allPathsWithDistances.push(allData);

        }

        return allPathsWithDistances;
    }
    return null;
}

function allFactoriesWithCitiesFromOrders(orders) {
    var factories = [];
    orders.forEach(order => {
        factories.push(order.placeOfManufacture);
    });
    factories = uniq(factories);
    var map = [];
    factories.forEach(factory => {
        map[factory] = [];
        orders.forEach(order => {
            //mesmo que nao haja encomenda na cidade da fabrica, esta tem que ser incluida
            map[factory].push(factory);

            if (order.placeOfManufacture == factory) map[factory].push(extractCityFromPlace(order.placeOfDelivery));
        })
        map[factory] = uniq(map[factory]);
    });
    if (map) {
        return map;
    }
    return null;
}
function extractCityFromPlace(place) {
    var data = place.split('-');
    return data[2];
}

function uniq(a) {
    return Array.from(new Set(a));
}