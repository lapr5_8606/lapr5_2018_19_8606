
const axios = require('axios');
const config = require("../config");
const baseURL = config.prologApi;

const algav_closestFactory_url = '/nearestFactory?city=' //+CODIGOPOSTAL;
const algav_bestPathDelivery_url = '/bestPath';

//deve retornar a cidade com factory mais perto da cidade do cliente ou false caso a cidade cliente seja invalida
exports.getClosestCityFactory = async function (postalCode) {
    //fazer pedido api ALGAV

    //90 a 94 madeira
    //95 a 99 açores

    var firstTwoDigits = postalCode.substring(0, 2);
    if (firstTwoDigits >= 90 && firstTwoDigits <= 99) {
        //ilhas
        postalCode = firstTwoDigits;
    } else {
        //portugal continental
        postalCode = postalCode.substring(0, 4);
    }

    var data = await axios.get(baseURL + algav_closestFactory_url + postalCode,
        {
            headers: {
                'Accept': 'application/json'
            }
        }).then(response => {
            return response.data;
        }).catch(error => {
            console.log('PROLOG ERROR:' + error)
            return null;
        });
    return data;
}

//deve retornar um array de cidades ou false caso a cidade factory seja invalida
exports.getBestPathForDeliveryForCity = async function (factoryCity, citiesForDelivery) {
    
    var data = await axios.post(baseURL + algav_bestPathDelivery_url,
        {
            factoryName: factoryCity,
            client: citiesForDelivery

        }).then(response => {
            return response.data;
        }).catch(error => {
            console.log('PROLOG ERROR:' + error)
            return null;
        });
    return data;
}