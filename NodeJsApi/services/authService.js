const axios = require('axios');
const config = require("../config");
const baseURL = config.authorizationApi;

exports.getUserRoles = async function(clientToken) {
    var roles = await axios.get(baseURL+'/auth/roles',
        {
            headers: {
                'Accept': 'application/json',
                'Authorization': clientToken
            }
        }).then(response =>{
            return response.data;
        }).catch(error=>{
            console.log('ROLES ERROR:'+error)
            return null;
        });
        return roles;
}
exports.getUserInfo = async function (clientToken) {
    //console.log('token',clientToken);
    var info = await axios.get(baseURL+'/auth/info',
        {
            headers: {
                'Accept': 'application/json',
                'Authorization': clientToken
            }
        }).then(response =>{
            return response.data;
        }).catch(error=>{
            console.log('INFO ERROR:'+error);
            return null;
        });
        return info;
}


