const mongoose = require('mongoose');
const schema = mongoose.Schema;
const StateMachine = require('javascript-state-machine');



let OrderSchema = new mongoose.Schema({
    item: { type: mongoose.Schema.Types.ObjectId, ref: 'Item' },
    placeOfManufacture: String/*{ type: mongoose.Schema.Types.ObjectId, ref: 'GeographicalLocation' }*/,
    placeOfDelivery: String/*{ type: mongoose.Schema.Types.ObjectId, ref: 'GeographicalLocation' }*/,
    status: { type:String , default: 'valid'},
    clientId : String,
    creationDate : String

});

OrderSchema.statics.stateMachine = function(currentState){
    return new StateMachine({
        init: currentState,
        transitions: [
            { name: 'assignmentByAlgorithm', from: 'valid', to: 'inProduction' },
            { name: 'producing', from: 'inProduction', to: 'readyDeliver' },
            { name: 'expedition', from: 'readyDeliver', to: 'inExpedition' },
            { name: 'delivering', from: 'inExpedition', to: 'delivered' },
            { name: 'clientSuccess', from: 'delivered', to: 'accepted' },
            { name: 'noAnswer', from: 'delivered', to: 'lost' }
        ],
        methods: {
            onAssignmentByAlgorithm: function () { console.log('Order was assigned') },
            onProducing: function () { console.log('Order was produced') },
            onExpedition: function () { console.log('Order was expedited') },
            onDelivering: function () { console.log('Order was delivered') },
            onClientSuccess: function(){ console.log('Order was successfully received')},
            onNoAnswer:function(){ console.log('Something went wrong')}
        }
    });
}

module.exports = mongoose.model('Order', OrderSchema);