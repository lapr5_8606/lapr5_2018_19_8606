const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/orderController');

router.get("/getBestDeliveryPath",OrderController.get_best_delivering_path);
router.post("/createMockOrders",OrderController.create_mock_order_for_testing_purpose);
router.post("/", OrderController.create_order);
router.get("/:id", OrderController.get_order);
router.get("/:id/items", OrderController.get_order_items);
router.get("/:id/items/:itemId", OrderController.get_items_order);
//lapr5 joao oliveira added
router.get("/", OrderController.get_all_user_orders);
router.delete("/:id",OrderController.delete_order);

module.exports = router;