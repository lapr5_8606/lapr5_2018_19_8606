const configBase = {
    catalogApi: "http://localhost:5000/api/",
    authorizationApi: "http://localhost:1337",
    prologApi: "http://localhost:8090",
    mongoDbConn: "mongodb://nodetutorial:nodetutorial@node-tutorial-shard-00-00-cp4go.mongodb.net:27017,node-tutorial-shard-00-01-cp4go.mongodb.net:27017,node-tutorial-shard-00-02-cp4go.mongodb.net:27017/test?ssl=true&replicaSet=node-tutorial-shard-0&authSource=admin&retryWrites=true"
};

const configDev = {};
const configProd = {};

const config = (process.env.NODE_ENV == "production") ?
    Object.assign(configBase, configProd) :
    Object.assign(configBase, configDev);

module.exports = config
