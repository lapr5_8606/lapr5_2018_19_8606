const Service = require("../services/itemService");
const AuthService = require("../services/authService");
const bestPathService = require("../services/bestPathService");
const ORDER_NOT_FOUND = "Encomenda não encontrada";
const ITEM_NOT_FOUND = "Item não encontrado";
const PRODUCT_NOT_FOUND = "Produto não encontrado";
const ITEM_DIM_WRONG = "Dimensões inválidas";
const RESTRICTION_VIOLATION = "Item viola restricoes";
const MANDATORY_ERROR = "Não estão incluidos todo so produtos obrigatórios";
const AREA_NOT_ENOUGH = "Área insuficiente";
const INVALID_CLIENT_CITY = "Cidade de cliente inválida";
const NOT_ALLOWED = "Não tem permissões para criar encomendas";

exports.get_order = async (req, res) => {
  var success = await Service.getOrder(req.params.id);

  if (success == false) {
    res.status(404).send(ORDER_NOT_FOUND);
  } else {
    res.send(success);
  }
};

exports.delete_order = async function(req, res) {
  var success = await Service.deleteOrder(req.params.id);

  if (!success) {
    res.status(404).send(ORDER_NOT_FOUND);
  } else {
    res.status(204).send();
  }
};

exports.get_order_items = async function(req, res) {
  var success = await Service.getOrderItems(req.params.id);

  if (!success) {
    res.status(404).send(ORDER_NOT_FOUND);
  } else {
    res.send(success);
  }
};

exports.get_items_order = async function(req, res) {
  var userRoles = await AuthService.getUserRoles(req.headers.authorization);
  //info contem varios dados como postalCode, id de utilizador, etc
  //var userInfo = await AuthService.getUserInfo(req.headers.authorization);

  //verificação de backend de permissão de user loggado
  if (userRoles[0] == "client" || userRoles[0] == "manager") {
    var success = await Service.getOrderItems(req.params.id, req.params.itemId);
  } else {
    success = "notAllowed";
  }

  if (success == false) {
    res.status(404).send(ORDER_NOT_FOUND);
  } else if (success == null) {
    res.status(404).send(ITEM_NOT_FOUND);
  } else if (success == "notAllowed") {
    res.status(400).send(NOT_ALLOWED);
  } else {
    res.send(success);
  }
};

exports.create_order = async (req, res) => {
  var userRoles = await AuthService.getUserRoles(req.headers.authorization);
  //info contem varios dados como postalCode, id de utilizador, etc
  var userInfo = await AuthService.getUserInfo(req.headers.authorization);

  //verificação de backend de permissão de user loggado
  if (userRoles[0] != "client") {
    success = "notAllowed";
  } else {
    var success = await Service.createOrder(req.body, userInfo);
  }

  //falta validar user e cidade existente e atribuir a cidade de fabrico

  if (success == false) {
    res.status(400).send(ITEM_DIM_WRONG);
  } else if (success == null) {
    res.status(404).send(PRODUCT_NOT_FOUND);
  } else if (success == "bad" || success == "child") {
    res.status(404).send(RESTRICTION_VIOLATION);
  } else if (success == "mandatory") {
    res.status(400).send(MANDATORY_ERROR);
  } else if (success == "DontFit") {
    res.status(400).send(AREA_NOT_ENOUGH);
  } else if (success == "invalidClientCity") {
    res.status(400).send(INVALID_CLIENT_CITY);
  } else if (success == "notAllowed") {
    res.status(400).send(NOT_ALLOWED);
  } else {
    res.status(201).send(success);
  }
};

exports.get_all_user_orders = async (req, res) => {
  //validar autorização de user loggado com info do header do req

  //validar autorização de user loggado com info do header do req

  var userRoles = await AuthService.getUserRoles(req.headers.authorization);
  //verificação de backend de permissão de user loggado
  if (userRoles[0] != "client" && userRoles[0] != "tester") {
    success = "notAllowed";
  } else {
    var userInfo = await AuthService.getUserInfo(req.headers.authorization);
    var success = await Service.getAllOrdersByUser(userInfo);
  }
  if (success == false) {
    res.status(404).send(ORDER_NOT_FOUND);
  } else if (success == null) {
    res.status(404).send(ITEM_NOT_FOUND);
  } else if (success == "notAllowed") {
    res.status(404).send(NOT_ALLOWED);
  } else {
    res.send(success);
  }
};

exports.create_mock_order_for_testing_purpose = async (req, res) => {
  /*
  var userRoles = await AuthService.getUserRoles(req.headers.authorization);
  //info contem varios dados como postalCode, id de utilizador, etc
  var userInfo = await AuthService.getUserInfo(req.headers.authorization);
*/
  var success;
  //verificação de backend de permissão de user loggado
  if (/*userRoles[1] != "tester"*/ false) {
    return NOT_ALLOWED;
  } else {
    success = await Service.createMockOrderForTestingPurpose(
      req.body.postalCodes,
      req.body.item
    );
  }
  if (success)
    return res.status(200).send({
      message: "Mocked orders created!"
    });
  return res.status(400).send({
    message: "Error at mocked orders creation."
  });
};

exports.get_best_delivering_path = async (req, res) => {
  /*
    //validar autorização de user loggado com info do header do req
    var userRoles = await AuthService.getUserRoles(req.headers.authorization);
    //verificação de backend de permissão de user loggado
    if (userRoles[0] != 'client' && userRoles[0] != 'manager') {
        success = 'notAllowed'
    } else {
        console.log(bestPathService.GetAllReadyToDeliverOrders());
    }
*/
  var success = await bestPathService.GetAllReadyToDeliverOrders();
  // a corrigir
  if (success == false) {
    res.status(404).send(ORDER_NOT_FOUND);
  } else if (success == null) {
    res.status(404).send(ITEM_NOT_FOUND);
  } else if (success == "notAllowed") {
    res.status(404).send(NOT_ALLOWED);
  } else {
    res.send(success);
  }
};
