// Server js
const mongoose = require('mongoose');
const cors = require('cors')

const express = require('express');
const bodyParser = require('body-parser');

const schedule = require('node-schedule');
const config = require('./config');

const bestPathService = require('./services/bestPathService');
var fs = require('fs');
var path = require('path')


// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'allPathsRegistry.log'), { flags: 'a' })

const app = express();
//app.use(cors)

let dev_db_url = config.mongoDbConn;
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const product = require('./routes/product'); //Import product routes
const order = require('./routes/order');
const item = require('./routes/item');


//schedule orders analysis

var j = schedule.scheduleJob({hour: 23, minute: 30, dayOfWeek: 0}, function(){
    accessLogStream.write(bestPathService.GetAllReadyToDeliverOrders());
});

//finish schedule
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Authorization , Accept");
    next();
});
app.use('/products', product);
app.use('/order', order);
app.use('/item', item);

let port = process.env.PORT || 8082;

app.listen(port, () => {
    console.log('Server is up and running on port number ' + port);
});