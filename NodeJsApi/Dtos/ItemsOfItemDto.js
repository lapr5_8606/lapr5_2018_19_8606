class ItemsOfItemsDto  {
    constructor(itemId, items) {
        this.itemId = itemId;
        this.items = items;
    }
};

module.exports = ItemsOfItemsDto;
