class OrderAndItemsDto  {
    constructor(OrderId, items) {
        this.OrderId = OrderId;
        this.items = items;
    }
};

module.exports = OrderAndItemsDto;
