class OrderDto  {
    constructor(orderId, itemId,clientId,status,placeOfDelivery,placeOfManufacture,creationDate) {
        this.OrderId = orderId;
        this.ItemId = itemId;
        this.clientId = clientId;
        this.status = status;
        this.placeOfDelivery = placeOfDelivery;
        this.placeOfManufacture = placeOfManufacture;
        this.creationDate = creationDate
    }
};

module.exports = OrderDto;
