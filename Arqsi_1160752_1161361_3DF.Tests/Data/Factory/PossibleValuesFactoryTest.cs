using System.Collections.Generic;
using Arqsi_1160752_1161361_3DF.Data.Factory;
using Arqsi_1160752_1161361_3DF.Dtos;
using Arqsi_1160752_1161361_3DF.Models;
using NUnit.Framework;

namespace Arqsi_1160752_1161361_3DF.Tests.Data.Factory
{
    [TestFixture]
    public class PossibleValuesFactoryTest
    {
        private PossibleValuesFactory factory;
        private PossibleValuesOfDimensionDto dto1;
        private PossibleValuesOfDimensionDto dto2;
        private PossibleValuesOfDimensionDto dto3;
        private PossibleValuesOfDimensionDto dto4;
        private PossibleValuesOfDimensionDto dto5;
        private PossibleValuesOfDimensionDto dto6;

        [SetUp]
        public void SetUp()
        {
            // creating the factory
            factory = new PossibleValuesFactory();

            // creating dto1
            dto1 = new PossibleValuesOfDimensionDto
            {
                IsDiscrete = true,
                Values = new List<float>()
            };
            dto1.Values.Add(1.893f);

            // creating dto2
            dto2 = new PossibleValuesOfDimensionDto
            {
                IsDiscrete = true,
                Values = new List<float>()
            };
            dto2.Values.Add(1);
            dto2.Values.Add(1.01f);
            dto2.Values.Add(5);
            dto2.Values.Add(21.41f);

            // creating dto3
            dto3 = new PossibleValuesOfDimensionDto
            {
                IsDiscrete = false,
                Values = new List<float>()
            };
            dto3.Values.Add(56.1f);
            dto3.Values.Add(23);

            // creating dto4
            dto4 = new PossibleValuesOfDimensionDto
            {
                IsDiscrete = false,
                Values = new List<float>()
            };
            dto4.Values.Add(23);
            dto4.Values.Add(56.1f);

            // creating dto5
            dto5 = new PossibleValuesOfDimensionDto
            {
                IsDiscrete = false,
                Values = new List<float>()
            };
            dto5.Values.Add(1);

            // creating dto6
            dto6 = new PossibleValuesOfDimensionDto
            {
                IsDiscrete = false,
                Values = new List<float>()
            };
            dto6.Values.Add(1);
            dto6.Values.Add(1.01f);
            dto6.Values.Add(1.001f);
        }

        [Test]
        public void TestCreateNewPossibleValuesForDimension()
        {
            PossibleValues actualValues;
            DiscretePossibleValues actualDiscreteValues;
            ContinuousPossibleValues actualContinuousValues;
            double delta = 0.00001;

            // test with dto1
            actualValues = factory.CreateNewPossibleValuesForDimension(dto1);
            Assert.IsFalse(actualValues is ContinuousPossibleValues);
            Assert.IsTrue(actualValues is DiscretePossibleValues);
            Assert.AreEqual(0, actualValues.ID);

            actualDiscreteValues = (DiscretePossibleValues)actualValues;
            Assert.AreEqual(1, actualDiscreteValues.PossibleValues.Count);
            Assert.IsTrue(actualDiscreteValues.PossibleValues.Contains(new Float { FloatValue = 1.893f }));

            // test with dto2
            actualValues = factory.CreateNewPossibleValuesForDimension(dto2);
            Assert.IsTrue(actualValues is DiscretePossibleValues);
            Assert.AreEqual(0, actualValues.ID);

            actualDiscreteValues = (DiscretePossibleValues)actualValues;
            Assert.AreEqual(4, actualDiscreteValues.PossibleValues.Count);
            Assert.IsTrue(actualDiscreteValues.PossibleValues.Contains(new Float { FloatValue = 1 }));
            Assert.IsTrue(actualDiscreteValues.PossibleValues.Contains(new Float { FloatValue = 1.01f }));
            Assert.IsTrue(actualDiscreteValues.PossibleValues.Contains(new Float { FloatValue = 5 }));
            Assert.IsTrue(actualDiscreteValues.PossibleValues.Contains(new Float { FloatValue = 21.41f }));

            // test with dto3
            actualValues = factory.CreateNewPossibleValuesForDimension(dto3);
            Assert.IsTrue(actualValues is ContinuousPossibleValues);
            Assert.AreEqual(0, actualValues.ID);

            actualContinuousValues = (ContinuousPossibleValues)actualValues;
            Assert.AreEqual(23, actualContinuousValues.MinValue);
            Assert.AreEqual(56.1f, actualContinuousValues.MaxValue, delta);

            // test with dto4
            actualValues = factory.CreateNewPossibleValuesForDimension(dto4);
            Assert.IsTrue(actualValues is ContinuousPossibleValues);
            Assert.AreEqual(0, actualValues.ID);

            actualContinuousValues = (ContinuousPossibleValues)actualValues;
            Assert.AreEqual(23, actualContinuousValues.MinValue);
            Assert.AreEqual(56.1f, actualContinuousValues.MaxValue, delta);

            Assert.IsNotNull(actualValues);
            // test with dto5
            actualValues = factory.CreateNewPossibleValuesForDimension(dto5);
            Assert.IsNull(actualValues);

            // test with dto6
            actualValues = factory.CreateNewPossibleValuesForDimension(dto6);
            Assert.IsNull(actualValues);
        }
    }
}