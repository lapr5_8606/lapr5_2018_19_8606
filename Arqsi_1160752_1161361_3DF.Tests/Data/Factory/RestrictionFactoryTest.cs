using System.Collections.Generic;
using Arqsi_1160752_1161361_3DF.Data.Factory;
using Arqsi_1160752_1161361_3DF.Dtos;
using Arqsi_1160752_1161361_3DF.Models;
using NUnit.Framework;

namespace Arqsi_1160752_1161361_3DF.Tests.Data.Factory
{
    [TestFixture]
    public class RestrictionFactoryTest
    {
        private RestrictionFactory factory;
        private DiscretePossibleValues possibleValues1;
        private ContinuousPossibleValues possibleValues2;
        private DiscretePossibleValues possibleValues3;
        private ContinuousPossibleValues possibleValues4;
        private Product product1;
        private Product product2;
        private Product product3;
        private Material material1;
        private Material material2;
        private Material material3;
        private ICollection<ProductMaterialRelationship> productMaterialRelationshipList1;
        private ICollection<ProductMaterialRelationship> productMaterialRelationshipList2;
        private Restriction restriction1;
        private Restriction restriction2;
        private NewPercentageRestrictionDto newPercentageRestrictionDto1;
        private NewPercentageRestrictionDto newPercentageRestrictionDto2;
        private NewPercentageRestrictionDto newPercentageRestrictionDto3;
        private NewPercentageRestrictionDto newPercentageRestrictionDto4;
        private NewPercentageRestrictionDto newPercentageRestrictionDto5;

        [SetUp]
        public void SetUp()
        {
            // creation of the factory
            factory = new RestrictionFactory();

            // creation of possibleValues1
            possibleValues1 = new DiscretePossibleValues
            {
                PossibleValues = new List<Float>()
            };
            possibleValues1.PossibleValues.Add(new Float { FloatValue = 2.67f });
            possibleValues1.PossibleValues.Add(new Float { FloatValue = 2.17f });
            possibleValues1.PossibleValues.Add(new Float { FloatValue = 2.97f });

            // creation of possibleValues2
            possibleValues2 = new ContinuousPossibleValues
            {
                MinValue = 0.001f,
                MaxValue = 1000301.312f
            };

            // creation of possibleValues3
            possibleValues3 = new DiscretePossibleValues
            {
                PossibleValues = new List<Float>()
            };
            possibleValues3.PossibleValues.Add(new Float { FloatValue = 5 });

            // creation of possibleValues4
            possibleValues4 = new ContinuousPossibleValues
            {
                MinValue = 1,
                MaxValue = 1
            };

            // creation of material1
            material1 = new Material
            {
                ID = 1,
                MaterialName = "material 1",
                AvailableFinishes = new List<Finish>()
            };
            material1.AvailableFinishes.Add(new Finish { FinishName = "mat 1 finish 1" });
            material1.AvailableFinishes.Add(new Finish { FinishName = "mat 1 finish 2" });

            // creation of material2
            material2 = new Material
            {
                ID = 2,
                MaterialName = "material 2",
                AvailableFinishes = new List<Finish>()
            };

            // creation of material3
            material3 = new Material
            {
                ID = 3,
                MaterialName = "material 3",
                AvailableFinishes = new List<Finish>()
            };

            // creation of product1
            DiscretePossibleValues heightPossibleValues = new DiscretePossibleValues
            {
                PossibleValues = new List<Float>()
            };
            heightPossibleValues.PossibleValues.Add(new Float { FloatValue = 2.34f });
            heightPossibleValues.PossibleValues.Add(new Float { FloatValue = 1.04f });
            heightPossibleValues.PossibleValues.Add(new Float { FloatValue = 30 });

            ContinuousPossibleValues widthPossibleValues = new ContinuousPossibleValues
            {
                MinValue = 0.54f,
                MaxValue = 32.7f
            };

            ContinuousPossibleValues depthPossibleValues = new ContinuousPossibleValues
            {
                MinValue = 13,
                MaxValue = 45
            };

            PossibleDimensions possibleDimensions = new PossibleDimensions
            {
                HeightPossibleValues = heightPossibleValues,
                WidthPossibleValues = widthPossibleValues,
                DepthPossibleValues = depthPossibleValues
            };

            product1 = new Product
            {
                ID = 1,
                Name = "product 1",
                Price = 15.99f,
                ProductCategory = null,
                PossibleDimensions = possibleDimensions
            };

            // creation of product2
            ContinuousPossibleValues heightValues2 = new ContinuousPossibleValues
            {
                MinValue = 0.1f,
                MaxValue = 50.50f
            };

            DiscretePossibleValues widthValues2 = new DiscretePossibleValues
            {
                PossibleValues = new List<Float>(),
            };
            widthValues2.PossibleValues.Add(new Float { FloatValue = 1 });

            ContinuousPossibleValues depthValues2 = new ContinuousPossibleValues
            {
                MinValue = 14.92f,
                MaxValue = 60
            };

            possibleDimensions = new PossibleDimensions
            {
                HeightPossibleValues = heightValues2,
                WidthPossibleValues = widthValues2,
                DepthPossibleValues = depthValues2
            };

            product2 = new Product
            {
                ID = 2,
                Name = "product 2",
                Price = 16,
                ProductCategory = null,
                PossibleDimensions = possibleDimensions
            };

            // creation of product3
            ContinuousPossibleValues heightValues3 = new ContinuousPossibleValues
            {
                MinValue = 0.1f,
                MaxValue = 50.50f
            };

            DiscretePossibleValues widthValues3 = new DiscretePossibleValues
            {
                PossibleValues = new List<Float>(),
            };
            widthValues3.PossibleValues.Add(new Float { FloatValue = 1 });

            ContinuousPossibleValues depthValues3 = new ContinuousPossibleValues
            {
                MinValue = 45.01f,
                MaxValue = 60
            };

            possibleDimensions = new PossibleDimensions
            {
                HeightPossibleValues = heightValues3,
                WidthPossibleValues = widthValues3,
                DepthPossibleValues = depthValues3
            };

            product3 = new Product
            {
                ID = 3,
                Name = "product 3",
                Price = 16,
                ProductCategory = null,
                PossibleDimensions = possibleDimensions
            };

            // creation of productMaterialRelationshipList1
            productMaterialRelationshipList1 = new List<ProductMaterialRelationship>();
            productMaterialRelationshipList1.Add(new ProductMaterialRelationship { Product = product1, Material = material1 });
            productMaterialRelationshipList1.Add(new ProductMaterialRelationship { Product = product1, Material = material2 });

            // creation of productMaterialRelationshipList2
            productMaterialRelationshipList2 = new List<ProductMaterialRelationship>();
            productMaterialRelationshipList2.Add(new ProductMaterialRelationship { Product = product2, Material = material3 });

            // creation of restriction1
            restriction1 = new DimensionsRestriction
            {
                ID = 1,
                MinHeightValue = 1,
                MaxHeightValue = 2,
                MinWidthValue = 5,
                MaxWidthValue = 24,
                MinDepthValue = 7.45f,
                MaxDepthValue = 8.45f
            };

            // creation of restriction 2
            restriction2 = new MaterialRestriction
            {
                ID = 2
            };

            // creation of newPercentageRestrictionDto1
            newPercentageRestrictionDto1 = new NewPercentageRestrictionDto
            {
                MinHeightPercentage = 0.5f,
                MaxHeightPercentage = 0.7f,
                MinWidthPercentage = 1,
                MaxWidthPercentage = 1,
                MinDepthPercentage = 0.356f,
                MaxDepthPercentage = 0.4f
            };

            // creation of newPercentageRestrictionDto2
            newPercentageRestrictionDto2 = new NewPercentageRestrictionDto
            {
                MinHeightPercentage = 1000.0f,
                MaxHeightPercentage = 1000.5f,
                MinWidthPercentage = 1,
                MaxWidthPercentage = 1,
                MinDepthPercentage = 0.356f,
                MaxDepthPercentage = 0.4f
            };

            // creation of newPercentageRestrictionDto3
            newPercentageRestrictionDto3 = new NewPercentageRestrictionDto
            {
                MinHeightPercentage = 0.5f,
                MaxHeightPercentage = 0.7f,
                MinWidthPercentage = 0.001f,
                MaxWidthPercentage = 0.01f,
                MinDepthPercentage = 0.356f,
                MaxDepthPercentage = 0.4f
            };

            // creation of newPercentageRestrictionDto4
            newPercentageRestrictionDto4 = new NewPercentageRestrictionDto
            {
                MinHeightPercentage = 0.5f,
                MaxHeightPercentage = 0.7f,
                MinWidthPercentage = 1,
                MaxWidthPercentage = 1,
                MinDepthPercentage = 0.1f,
                MaxDepthPercentage = 0.2f
            };

            // creation of newPercentageRestrictionDto5
            newPercentageRestrictionDto5 = new NewPercentageRestrictionDto
            {
                MinHeightPercentage = 0.7f,
                MaxHeightPercentage = 0.5f,
                MinWidthPercentage = 1,
                MaxWidthPercentage = 1,
                MinDepthPercentage = 0.356f,
                MaxDepthPercentage = 0.4f
            };
        }

        [Test]
        public void TestFindMinValueOfPossibleValues()
        {
            double delta = 0.00001;
            float unexpectedMin = 2.171f;
            float expectedMin = 2.17f;

            Assert.AreNotEqual(unexpectedMin, factory.FindMinValueOfPossibleValues(possibleValues1));
            Assert.AreEqual(expectedMin, factory.FindMinValueOfPossibleValues(possibleValues1), delta);

            unexpectedMin = 0.0011f;
            expectedMin = 0.001f;

            Assert.AreNotEqual(unexpectedMin, factory.FindMinValueOfPossibleValues(possibleValues2));
            Assert.AreEqual(expectedMin, factory.FindMinValueOfPossibleValues(possibleValues2), delta);

            unexpectedMin = 5.0001f;
            expectedMin = 5;

            Assert.AreNotEqual(unexpectedMin, factory.FindMinValueOfPossibleValues(possibleValues3));
            Assert.AreEqual(expectedMin, factory.FindMinValueOfPossibleValues(possibleValues3), delta);

            unexpectedMin = 1.0001f;
            expectedMin = 1;

            Assert.AreNotEqual(unexpectedMin, factory.FindMinValueOfPossibleValues(possibleValues4));
            Assert.AreEqual(expectedMin, factory.FindMinValueOfPossibleValues(possibleValues4), delta);
        }

        [Test]
        public void TestFindMaxValueOfPossibleValues()
        {
            double delta = 0.00001;
            float unexpectedMax = 2.97001f;
            float expectedMax = 2.97f;

            Assert.AreNotEqual(unexpectedMax, factory.FindMaxValueOfPossibleValues(possibleValues1));
            Assert.AreEqual(expectedMax, factory.FindMaxValueOfPossibleValues(possibleValues1), delta);

            unexpectedMax = 1000301.2f;
            expectedMax = 1000301.312f;

            Assert.AreNotEqual(unexpectedMax, factory.FindMaxValueOfPossibleValues(possibleValues2));
            Assert.AreEqual(expectedMax, factory.FindMaxValueOfPossibleValues(possibleValues2), delta);

            unexpectedMax = 5.001f;
            expectedMax = 5;

            Assert.AreNotEqual(unexpectedMax, factory.FindMaxValueOfPossibleValues(possibleValues3));
            Assert.AreEqual(expectedMax, factory.FindMaxValueOfPossibleValues(possibleValues3), delta);

            unexpectedMax = 1.001f;
            expectedMax = 1;

            Assert.AreNotEqual(unexpectedMax, factory.FindMaxValueOfPossibleValues(possibleValues4));
            Assert.AreEqual(expectedMax, factory.FindMaxValueOfPossibleValues(possibleValues4), delta);
        }

        [Test]
        public void TestCreateDimensionsRestriction()
        {
            float unexpectedHeightMin = 0.101f;
            float expectedHeightMin = 0.1f;
            float expectedHeightMax = 30;
            float expectedWidthMin = 1.0f;
            float expectedWidthMax = 32.7f;
            float expectedDepthMin = 14.92f;
            float expectedDepthMax = 45;
            int expectedId = 0;
            int unexpectedId = 1;
            double delta = 0.00001;

            DimensionsRestriction restriction = factory.CreateDimensionsRestriction(product1, product2);

            Assert.AreNotEqual(unexpectedHeightMin, restriction.MinHeightValue);
            Assert.AreEqual(expectedHeightMin, restriction.MinHeightValue, delta);
            Assert.AreEqual(expectedHeightMax, restriction.MaxHeightValue, delta);
            Assert.AreEqual(expectedWidthMin, restriction.MinWidthValue, delta);
            Assert.AreEqual(expectedWidthMax, restriction.MaxWidthValue, delta);
            Assert.AreEqual(expectedDepthMin, restriction.MinDepthValue, delta);
            Assert.AreEqual(expectedDepthMax, restriction.MaxDepthValue, delta);

            Assert.AreNotEqual(unexpectedId, restriction.ID);
            Assert.AreEqual(expectedId, restriction.ID);

            // testing width product1 and product3 (doesn't fit because of the depth)
            restriction = factory.CreateDimensionsRestriction(product1, product3);

            Assert.Null(restriction);
        }

        [Test]
        public void TestCreateMaterialRestriction()
        {
            Assert.Null(factory.CreateMaterialRestriction(productMaterialRelationshipList1,
                productMaterialRelationshipList2));

            productMaterialRelationshipList2.Add(new ProductMaterialRelationship { Product = product2, Material = material2 });
            MaterialRestriction matRestriction = factory.CreateMaterialRestriction(productMaterialRelationshipList1,
                productMaterialRelationshipList2);

            Assert.NotNull(matRestriction);
            Assert.AreEqual(0, matRestriction.ID);
        }

        [Test]
        public void TestCreatesRestrictionDto()
        {
            double delta = 0.00001;

            GetRestrictionDto dto = factory.CreatesRestrictionDto(restriction1);

            Assert.True(dto is GetDimensionsRestrictionDto);

            GetDimensionsRestrictionDto dimsDto = (GetDimensionsRestrictionDto)dto;

            Assert.AreEqual(1, dimsDto.ID);
            Assert.AreEqual("Dimensions Restriction", dimsDto.RestrictionType);
            Assert.AreEqual(1, dimsDto.MinHeightValue);
            Assert.AreEqual(2, dimsDto.MaxHeightValue);
            Assert.AreEqual(5, dimsDto.MinWidthValue);
            Assert.AreEqual(24, dimsDto.MaxWidthValue);
            Assert.AreEqual(7.45, dimsDto.MinDepthValue, delta);
            Assert.AreEqual(8.45, dimsDto.MaxDepthValue, delta);

            dto = factory.CreatesRestrictionDto(restriction2);
            Assert.True(dto is GetMaterialRestrictionDto);
            Assert.AreEqual(2, dto.ID);
            Assert.AreEqual("Material Restriction", dto.RestrictionType);
        }

        [Test]
        public void TestCreatePercentageRestriction()
        {
            double delta = 0.00001;

            // test with newPercentageRestrictionDto1 (fits)
            Restriction restriction = factory.CreatePercentageRestriction(newPercentageRestrictionDto1, product1, product2);

            Assert.NotNull(restriction);
            Assert.AreEqual(0, restriction.ID);
            Assert.True(restriction is PercentageRestriction);

            PercentageRestriction percentageRestriction = (PercentageRestriction)restriction;

            Assert.AreEqual(0.5f, percentageRestriction.MinHeightPercentage, delta);
            Assert.AreEqual(0.7f, percentageRestriction.MaxHeightPercentage, delta);
            Assert.AreEqual(1.0f, percentageRestriction.MinWidthPercentage, delta);
            Assert.AreEqual(1.0f, percentageRestriction.MaxWidthPercentage, delta);
            Assert.AreEqual(0.356f, percentageRestriction.MinDepthPercentage, delta);
            Assert.AreEqual(0.4f, percentageRestriction.MaxDepthPercentage, delta);

            // test with newPercentageRestrictionDto2 (doesn't fit)
            restriction = factory.CreatePercentageRestriction(newPercentageRestrictionDto2, product1, product2);
            Assert.Null(restriction);

            // test with newPercentageRestrictionDto3 (doesn't fit)
            restriction = factory.CreatePercentageRestriction(newPercentageRestrictionDto3, product1, product2);
            Assert.Null(restriction);

            // test with newPercentageRestrictionDto4 (doesn't fit)
            restriction = factory.CreatePercentageRestriction(newPercentageRestrictionDto4, product1, product2);
            Assert.Null(restriction);

            // test with newPercentageRestrictionDto5 (doesn't fit)
            restriction = factory.CreatePercentageRestriction(newPercentageRestrictionDto5, product1, product2);
            Assert.Null(restriction);
        }
    }
}