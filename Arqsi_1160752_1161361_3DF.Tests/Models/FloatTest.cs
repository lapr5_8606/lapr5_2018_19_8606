using Arqsi_1160752_1161361_3DF.Models;
using NUnit.Framework;

namespace Arqsi_1160752_1161361_3DF.Tests.Models
{
    [TestFixture]
    public class FloatTest
    {
        private Float float1;
        private Float float2;
        private Float float3;
        private Float float4;
        private Float float5;
        private Float float6;
        private Finish finish1;

        [SetUp]
        public void SetUp()
        {
            float1 = new Float { ID = 1, FloatValue = 41 };
            float2 = new Float { ID = 2, FloatValue = 41 };
            float3 = new Float { ID = 3, FloatValue = 31.123f };
            float4 = new Float { ID = 4, FloatValue = 31.123f };
            float5 = new Float { ID = 5, FloatValue = 31 };
            float6 = new Float { ID = 6, FloatValue = 41.001f };

            finish1 = new Finish { ID = 1, FinishName = "finish 1" };
        }

        [Test]
        public void TestEquals()
        {
            Assert.AreEqual(float1, float1);
            Assert.AreEqual(float1, float2);
            Assert.AreNotEqual(float1, float3);
            Assert.AreNotEqual(float1, float5);
            Assert.AreNotEqual(float1, float6);

            Assert.AreEqual(float3, float3);
            Assert.AreEqual(float3, float4);
            Assert.AreEqual(float4, float3);
            Assert.AreNotEqual(float3, float2);
            Assert.AreNotEqual(float3, float5);
            Assert.AreNotEqual(float3, float6);

            Assert.AreNotEqual(float5, float1);
            Assert.AreNotEqual(float5, float2);
            Assert.AreNotEqual(float5, float3);
            Assert.AreNotEqual(float5, float4);
            Assert.AreNotEqual(float5, float6);
            Assert.AreEqual(float5, float5);

            Assert.AreNotEqual(float6, float1);
            Assert.AreNotEqual(float6, float2);
            Assert.AreNotEqual(float6, float3);
            Assert.AreNotEqual(float6, float4);
            Assert.AreNotEqual(float6, float5);
            Assert.AreEqual(float6, float6);

            Assert.AreNotEqual(float1, null);
            Assert.AreNotEqual(float5, null);
            Assert.AreNotEqual(float1, finish1);
            Assert.AreNotEqual(float5, finish1);
        }
    }
}