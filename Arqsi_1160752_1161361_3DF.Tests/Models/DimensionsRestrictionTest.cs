using Arqsi_1160752_1161361_3DF.Models;
using NUnit.Framework;

namespace Arqsi_1160752_1161361_3DF.Tests.Models
{
    [TestFixture]
    public class DimensionsRestrictionTest
    {
        private DimensionsRestriction restriction1;
        private DimensionsRestriction restriction2;

        [SetUp]
        public void Setup()
        {
            restriction1 = new DimensionsRestriction
            {
                MinHeightValue = 1,
                MaxHeightValue = 10,
                MinWidthValue = 4.76f,
                MaxWidthValue = 9.2f,
                MinDepthValue = 5,
                MaxDepthValue = 5.5f
            };

            restriction2 = new DimensionsRestriction
            {
                ID = 2,
                MinHeightValue = 0.123f,
                MaxHeightValue = 2.0f,
                MinWidthValue = 3,
                MaxWidthValue = 4,
                MinDepthValue = 5.5f,
                MaxDepthValue = 241
            };
        }
    }
}