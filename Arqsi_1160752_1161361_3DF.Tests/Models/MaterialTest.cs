using Arqsi_1160752_1161361_3DF.Models;
using NUnit.Framework;

namespace Arqsi_1160752_1161361_3DF.Tests.Models
{
    [TestFixture]
    public class MaterialTest
    {
        private Material material1;
        private Material material2;
        private Material material3;
        private Material material4;
        private Material material5;
        private Material material6;
        private Float float1;

        [SetUp]
        public void SetUp()
        {
            material1 = new Material { ID = 1, MaterialName = "material 1" };
            material2 = new Material { ID = 2, MaterialName = "material 1" };
            material3 = new Material { ID = 1, MaterialName = "material 3" };
            material4 = new Material { ID = 1, MaterialName = "material 1" };
            material5 = new Material { ID = 5, MaterialName = "material 5" };
            material6 = new Material { ID = 6, MaterialName = "material 5" };

            float1 = new Float { ID = 1, FloatValue = 23.1f };
        }

        [Test]
        public void TestEquals()
        {
            Assert.AreNotEqual(material1, null);
            Assert.AreNotEqual(material1, float1);

            Assert.AreEqual(material1, material1);
            Assert.AreEqual(material1, material2);
            Assert.AreEqual(material1, material4);
            Assert.AreEqual(material5, material6);

            Assert.AreNotEqual(material1, material3);
            Assert.AreNotEqual(material1, material5);
        }
    }
}