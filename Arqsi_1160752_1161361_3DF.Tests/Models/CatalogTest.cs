using Arqsi_1160752_1161361_3DF.Models;
using NUnit.Framework;

namespace Arqsi_1160752_1161361_3DF.Tests.Models
{
    [TestFixture]
    public class CatalogTest
    {
        private Catalog catalog1;
        private Catalog catalog2;
        private Catalog catalog3;
        private Catalog catalog4;
        private Catalog catalog5;
        private Catalog catalog6;

        [SetUp]
        public void SetUp()
        {
            catalog1 = new Catalog { ID = 1, Name = "Catalog 1" };
            catalog2 = new Catalog { ID = 2, Name = "Catalog 1" };
            catalog3 = new Catalog { ID = 1, Name = "Catalog 3" };
            catalog4 = new Catalog { ID = 1, Name = "Catalog 1" };
            catalog5 = new Catalog { ID = 5, Name = "Catalog 5" };
            catalog6 = new Catalog { ID = 6, Name = "Catalog 5" };

        }

        [Test]
        public void TestEquals()
        {
            Assert.AreNotEqual(catalog1, null);

            Assert.AreEqual(catalog1, catalog1);
            Assert.AreEqual(catalog1, catalog2);
            Assert.AreEqual(catalog1, catalog4);
            Assert.AreEqual(catalog5, catalog6);

            Assert.AreNotEqual(catalog1, catalog3);
            Assert.AreNotEqual(catalog1, catalog5);
        }
    }
}