using System.Collections.Generic;
using Arqsi_1160752_1161361_3DF.Models;
using NUnit.Framework;

namespace Arqsi_1160752_1161361_3DF.Tests.Models
{
    [TestFixture]
    public class ProductRelationshipTest
    {
        private ProductRelationship prNoRestrictions;
        private ProductRelationship prNoMaterialRestriction;
        private ProductRelationship prNoPercentageRestriction;
        private ProductRelationship prAllRestrictions;

        [SetUp]
        public void SetUp()
        {
            // relationship without any restrictions
            prNoRestrictions = new ProductRelationship
            {
                ID = 1,
                ParentProduct = null,
                ChildProduct = null,
                IsMandatory = false,
                Restrictions = new List<Restriction>()
            };

            // relationship without a material restriction
            prNoMaterialRestriction = new ProductRelationship
            {
                ID = 2,
                ParentProduct = null,
                ChildProduct = null,
                IsMandatory = true,
                Restrictions = new List<Restriction>()
            };
            prNoMaterialRestriction.Restrictions.Add(
                new DimensionsRestriction
                {
                    ID = 1,
                    MinHeightValue = 0,
                    MaxHeightValue = 1,
                    MinWidthValue = 0,
                    MaxWidthValue = 1,
                    MinDepthValue = 0,
                    MaxDepthValue = 1
                });
            prNoMaterialRestriction.Restrictions.Add(
                new PercentageRestriction
                {
                    ID = 2,
                    MinHeightPercentage = 0,
                    MaxHeightPercentage = 1,
                    MinWidthPercentage = 0,
                    MaxWidthPercentage = 1,
                    MinDepthPercentage = 0,
                    MaxDepthPercentage = 1
                });

            // relationship without any percentage restriction
            prNoPercentageRestriction = new ProductRelationship
            {
                ID = 3,
                ParentProduct = null,
                ChildProduct = null,
                IsMandatory = false,
                Restrictions = new List<Restriction>()
            };
            prNoPercentageRestriction.Restrictions.Add(
                new DimensionsRestriction
                {
                    ID = 3,
                    MinHeightValue = 0,
                    MaxHeightValue = 1,
                    MinWidthValue = 0,
                    MaxWidthValue = 1,
                    MinDepthValue = 0,
                    MaxDepthValue = 1
                });
            prNoPercentageRestriction.Restrictions.Add(
                new MaterialRestriction
                {
                    ID = 4
                });

            // relationship with all types of restrictions
            prAllRestrictions = new ProductRelationship
            {
                ID = 4,
                ParentProduct = null,
                ChildProduct = null,
                IsMandatory = true,
                Restrictions = new List<Restriction>()
            };
            prAllRestrictions.Restrictions.Add(
                new DimensionsRestriction
                {
                    ID = 5,
                    MinHeightValue = 0,
                    MaxHeightValue = 1,
                    MinWidthValue = 0,
                    MaxWidthValue = 1,
                    MinDepthValue = 0,
                    MaxDepthValue = 1
                });
            prAllRestrictions.Restrictions.Add(
                new MaterialRestriction
                {
                    ID = 6
                });
            prAllRestrictions.Restrictions.Add(
            new PercentageRestriction
            {
                ID = 7,
                MinHeightPercentage = 0,
                MaxHeightPercentage = 1,
                MinWidthPercentage = 0,
                MaxWidthPercentage = 1,
                MinDepthPercentage = 0,
                MaxDepthPercentage = 1
            });
        }

        [Test]
        public void TestHasMaterialRestriction()
        {
            Assert.False(prNoRestrictions.HasMaterialRestriction());
            Assert.False(prNoMaterialRestriction.HasMaterialRestriction());
            Assert.True(prNoPercentageRestriction.HasMaterialRestriction());
            Assert.True(prAllRestrictions.HasMaterialRestriction());
        }

        [Test]
        public void TestRemoveMaterialRestriction()
        {
            Assert.False(prNoRestrictions.RemoveMaterialRestriction());
            Assert.AreEqual(0, prNoRestrictions.Restrictions.Count);

            Assert.False(prNoMaterialRestriction.RemoveMaterialRestriction());
            Assert.AreEqual(2, prNoMaterialRestriction.Restrictions.Count);

            Assert.True(prNoPercentageRestriction.HasMaterialRestriction());
            Assert.AreEqual(2, prNoPercentageRestriction.Restrictions.Count);
            Assert.True(prNoPercentageRestriction.RemoveMaterialRestriction());
            Assert.AreEqual(1, prNoPercentageRestriction.Restrictions.Count);
            Assert.False(prNoPercentageRestriction.HasMaterialRestriction());

            Assert.True(prAllRestrictions.HasMaterialRestriction());
            Assert.AreEqual(3, prAllRestrictions.Restrictions.Count);
            Assert.True(prAllRestrictions.RemoveMaterialRestriction());
            Assert.AreEqual(2, prAllRestrictions.Restrictions.Count);
            Assert.False(prAllRestrictions.HasMaterialRestriction());
        }

        [Test]
        public void TestGetPercentageRestriction()
        {
            Assert.Null(prNoRestrictions.GetPercentageRestriction());
            Assert.Null(prNoPercentageRestriction.GetPercentageRestriction());

            PercentageRestriction percentageRestriction = prNoMaterialRestriction.GetPercentageRestriction();
            Assert.AreEqual(2, percentageRestriction.ID);
            Assert.AreEqual(0, percentageRestriction.MinHeightPercentage);
            Assert.AreEqual(1, percentageRestriction.MaxHeightPercentage);

            percentageRestriction = prAllRestrictions.GetPercentageRestriction();
            Assert.AreEqual(7, percentageRestriction.ID);
            Assert.AreEqual(0, percentageRestriction.MinWidthPercentage);
            Assert.AreEqual(1, percentageRestriction.MaxWidthPercentage);
        }

        [Test]
        public void TestRemovePercentageRestriction()
        {
            Assert.False(prNoRestrictions.RemovePercentageRestriction());
            Assert.AreEqual(0, prNoRestrictions.Restrictions.Count);

            Assert.False(prNoPercentageRestriction.RemovePercentageRestriction());
            Assert.AreEqual(2, prNoPercentageRestriction.Restrictions.Count);

            Assert.AreEqual(2, prNoMaterialRestriction.Restrictions.Count);
            Assert.True(prNoMaterialRestriction.RemovePercentageRestriction());
            Assert.AreEqual(1, prNoMaterialRestriction.Restrictions.Count);

            Assert.AreEqual(3, prAllRestrictions.Restrictions.Count);
            Assert.True(prAllRestrictions.RemovePercentageRestriction());
            Assert.AreEqual(2, prAllRestrictions.Restrictions.Count);
        }

        [Test]
        public void TestGetMaterialRestriction()
        {
            Assert.Null(prNoRestrictions.GetMaterialRestriction());
            Assert.Null(prNoMaterialRestriction.GetMaterialRestriction());

            MaterialRestriction materialRestriction = prNoPercentageRestriction.GetMaterialRestriction();
            Assert.AreEqual(4, materialRestriction.ID);

            materialRestriction = prAllRestrictions.GetMaterialRestriction();
            Assert.AreEqual(6, materialRestriction.ID);
        }
    }
}