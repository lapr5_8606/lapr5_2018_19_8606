using Arqsi_1160752_1161361_3DF.Models;
using NUnit.Framework;

namespace Arqsi_1160752_1161361_3DF.Tests.Models
{
    [TestFixture]
    public class FinishTest
    {
        private Finish finish1;
        private Finish finish2;
        private Finish finish3;
        private Finish finish4;
        private Finish finish5;
        private Float float1;

        [SetUp]
        public void SetUp()
        {
            finish1 = new Finish { ID = 1, FinishName = "finish 1" };
            finish2 = new Finish { ID = 2, FinishName = "finish 1" };
            finish3 = new Finish { ID = 1, FinishName = "finish 3" };
            finish4 = new Finish { ID = 1, FinishName = "finish 1" };
            finish5 = new Finish { ID = 5, FinishName = "finish 5" };

            float1 = new Float { ID = 1, FloatValue = 23.1f };
        }

        [Test]
        public void TestEquals()
        {
            Assert.AreNotEqual(finish1, null);
            Assert.AreNotEqual(finish1, float1);

            Assert.AreEqual(finish1, finish1);
            Assert.AreEqual(finish1, finish2);
            Assert.AreEqual(finish1, finish4);

            Assert.AreNotEqual(finish1, finish3);
            Assert.AreNotEqual(finish1, finish5);
        }
    }
}