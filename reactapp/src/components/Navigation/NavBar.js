import React from "react";

const navBar = props => {
  let { user } = props;

  let logoutButton = null;
  if (user.token) {
    logoutButton = (
      <button onClick={props.logoutClick} type="button" className="btn btn-light">
        Log out
      </button>
    )
  }

  return (
    <nav className="navbar navbar-dark bg-primary fixed-top">
      <div className="container">
        <a className="navbar-brand" href=".">
          Armários
        </a>
        {logoutButton}
      </div>
    </nav>
  );
};

export default navBar;
