import React from 'react'
import { configure , shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import NavBar from './NavBar'

configure({adapter: new Adapter()})

describe('Barra de navegação', () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<NavBar />)
    })
    it('should render client by default', () => {
        wrapper.setProps({isManager: false})
        let button = wrapper.find({type: 'button'})
        expect(button.text()).toEqual('Cliente')
    })

    it('should render manager if passed in props', () => {
        wrapper.setProps({isManager:true})
        let button = wrapper.find({type: 'button'})
        expect(button.text()).toEqual('Gestor')
    })
});