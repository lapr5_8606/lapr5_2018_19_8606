import React, { Component } from "react";

import Wrapper from "../../../../hoc/Wrapper";
import "../../../Layout/Layout.css";

import NewCollection from "./NewCollection/NewCollection";
import SearchCollection from "./SearchCollection/SearchCollection";
import DeleteCollection from "./DeleteCollection/DeleteCollection";

class Collection extends Component {
  state = {
    operations: ["Add", "Search", "Delete"],
    selected: 0,
    pages: [<NewCollection user={this.props.user} />, <SearchCollection user={this.props.user}/>, <DeleteCollection user={this.props.user} />],
    page: <NewCollection user={this.props.user}/>
  };

  changeOperation = index => {
    this.setState({ selected: index, page: this.state.pages[index] });
  };

  render() {
    return (
      <Wrapper>
        <ul className="nav nav-tabs justify-content-center">
          {this.state.operations.map((operation, i) => {
            return (
              <li className="nav-item" key={i}>
                {/* eslint-disable-next-line */}
                <a
                  onClick={() => this.changeOperation(i)}
                  className={
                    "nav-link" + (i === this.state.selected ? " active" : "")
                  }
                  href="#"
                >
                  {operation}
                </a>
              </li>
            );
          })}
        </ul>
        <div className="subContainer">{this.state.page}</div>
      </Wrapper>
    );
  }
}

export default Collection;
