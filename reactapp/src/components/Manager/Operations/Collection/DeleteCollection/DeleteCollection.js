import React, { Component } from "react";
import FilteredMultiSelect from "react-filtered-multiselect";
import ManagerApi from "../../../../../api/ManagerApi";
import Swall from "sweetalert2";

const FILTEREDMULTISELECT_CLASSES = {
  filter: "form-control",
  select: "form-control",
  button: "btn btn btn-block btn-default",
  buttonActive: "btn btn-primary"
};

class DeleteCollection extends Component {
  constructor() {
    super();
    ManagerApi.get("collection/all", "")
      .then(response => {
        let optionsNew = [];

        let i;
        for (i = 0; i < response.data.collections.length; i++) {
          optionsNew[i] = {
            collectionID: response.data.collections[i].collectionID,
            collectionName: response.data.collections[i].collectionName
          };
        }

        this.setState({
          collectionNameList: optionsNew
        });
      })
      .catch(erro => {});
  }

  state = {
    collectionNameList: [],
    collection: "",
    collectionClass: "form-control",
    selectedCollections: []
  };

  handleCollectionSelectionChange = selectedOptions => {
    selectedOptions.sort((a, b) => a.id - b.id);

    this.state.collection = selectedOptions[0].collectionName;

    let valid = true;

    if (selectedOptions[0].collectionName.length === 0) {
      valid = false;
      this.setState({
        collectionNameClass: this.state.collectionNameClass.concat(
          " is-invalid"
        )
      });
    }

    if (!valid) {
      return;
    }

    ManagerApi.delete("collection/names", this.state.collection)
      .then(() => {
        let collectionNameList = [];
        let x = 0;

        for (var i = 0; i < this.state.collectionNameList.length; i++) {
          if (
            this.state.collectionNameList[i].collectionName ===
            this.state.collection
          ) {
          } else {
            collectionNameList[x] = this.state.collectionNameList[i];
            x++;
          }
        }
        this.setState({ collectionNameList });
        Swall({
          type: "success",
          title: "Sucesso",
          text: "Collection deleted successfully"
        });
      })
      .catch(error => {
        Swall({
          type: "error",
          title: "Erro",
          text: error.response.data.errorMessage
        });
      });
  };

  render() {
    return (
      <div className="form-group">
        <label htmlFor="collection">Collection you want to delete:</label>
        <div className="row">
          <div className="col-md-5">
            <FilteredMultiSelect
              buttonText="Delete"
              classNames={FILTEREDMULTISELECT_CLASSES}
              onChange={this.handleCollectionSelectionChange}
              options={this.state.collectionNameList}
              selectedOptions={this.state.selectedCollections}
              textProp="collectionName"
              valueProp="collectionID"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default DeleteCollection;
