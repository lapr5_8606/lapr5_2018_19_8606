import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import Collection from "./Collection";
import NewCollection from "./NewCollection/NewCollection";
import DeleteCollection from "./DeleteCollection/DeleteCollection";
import SearchCollection from "./SearchCollection/SearchCollection";

configure({ adapter: new Adapter() });

describe("<Collection />", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Collection />);
  });

  it("should display <NewCollection /> by default", () => {
    expect(wrapper.find(NewCollection)).toHaveLength(1);
  });

  it("Should display <DeleteCollection /> if state set to delete", () => {
    wrapper.setState({ page: <DeleteCollection /> });
    expect(wrapper.find(DeleteCollection)).toHaveLength(1);
  });

  it("Should display <SearchCollection/> if state set to search", () => {
    wrapper.setState({ page: <SearchCollection /> });
    expect(wrapper.find(SearchCollection)).toHaveLength(1);
  });

  it("Should change selected state when ChangeOperation is called", () => {
    wrapper.instance().changeOperation(1);
    expect(wrapper.state("selected")).toEqual(1);
    expect(wrapper.find(DeleteCollection)).toHaveLength(1);
  });
});
