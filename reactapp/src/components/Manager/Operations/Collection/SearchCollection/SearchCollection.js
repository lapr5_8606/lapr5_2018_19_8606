import React, { Component } from "react";
import Wrapper from "../../../../../hoc/Wrapper";
import Swall from "sweetalert2";
import FilteredMultiSelect from "react-filtered-multiselect";
import ManagerApi from "../../../../../api/ManagerApi";

const FILTEREDMULTISELECT_CLASSES = {
  filter: "form-control",
  select: "form-control",
  button: "btn btn btn-block btn-default",
  buttonActive: "btn btn btn-block btn-primary"
};

class SearchCollection extends Component {
  state = {
    collectionNameList: [],
    collection: "",
    collectionClass: "form-control",
    selectedCollections: [],
    componentSelectedCollection: "",
    selectedCollection: {}
  };

  constructor() {
    super();
    ManagerApi.get("collection/all", "")
      .then(response => {
        let optionsNew = [];

        let i;
        for (i = 0; i < response.data.collections.length; i++) {
          optionsNew[i] = {
            collectionID: response.data.collections[i].collectionID,
            collectionName: response.data.collections[i].collectionName
          };
        }

        this.setState({
          collectionNameList: optionsNew
        });
      })
      .catch(erro => {});
  }

  handleCollectionSelectionChange = selectedCollections => {
    selectedCollections.sort((a, b) => a.id - b.id);
    let selectedCollectionName = selectedCollections[0].collectionName;

    ManagerApi.get("collection/names", selectedCollectionName)
      .then(response => {
        let data = {
          collectionName: response.data.collectionName,
          AestheticLine: response.data.aestheticLine,
          productsNameList: response.data.productsNameList
        };
        this.state.selectedCollection = data;

        this.setState({
          componentSelectedCollection: (
            <div className="form-group">
              <label htmlFor="collection">Collection name:</label>
              <input
                type="text"
                className={this.state.collectionClass}
                readOnly
                id="collection"
                placeholder="Input collection name"
                value={this.state.selectedCollection.collectionName}
                onChange={this.handleCollectionChange}
              />
              <label htmlFor="collection">Aesthetic Line:</label>
              <input
                type="text"
                className={this.state.collectionClass}
                readOnly
                id="aestheticLine"
                placeholder="Input collection name"
                value={this.state.selectedCollection.AestheticLine}
                onChange={this.handleCollectionChange}
              />
              <label htmlFor="collection">Product List:</label>
              <div>
                {this.state.selectedCollection.productsNameList.map(function(
                  d,
                  idx
                ) {
                  return <li key={idx}>{d}</li>;
                })}
              </div>
            </div>
          )
        });
      })
      .catch(erro => {});
  };

  render() {
    return (
      <Wrapper>
        <form className="needs-validation">
          <div className="form-group">
            <label htmlFor="collection">Collection you want to check:</label>
            <div className="row">
              <div className="col-md-5">
                <FilteredMultiSelect
                  buttonText="Search"
                  classNames={FILTEREDMULTISELECT_CLASSES}
                  onChange={this.handleCollectionSelectionChange}
                  options={this.state.collectionNameList}
                  selectedOptions={this.state.selectedCollections}
                  textProp="collectionName"
                  valueProp="collectionID"
                />
              </div>
            </div>
          </div>
          <div>{this.state.componentSelectedCollection}</div>
        </form>
      </Wrapper>
    );
  }
}

export default SearchCollection;
