import React, { Component } from "react";
import Swall from "sweetalert2";
import FilteredMultiSelect from "react-filtered-multiselect";

import ManagerApi from "../../../../../api/ManagerApi";

const FILTEREDMULTISELECT_CLASSES = {
  filter: "form-control",
  select: "form-control",
  button: "btn btn btn-block btn-default",
  buttonActive: "btn btn btn-block btn-primary"
};

class NewCollection extends Component {
  state = {
    collectionName: "",
    productNameList: [],
    collectionClass: "form-control",
    selectedProducts: [],
    AestheticLine: ""
  };

  componentDidMount() {
    ManagerApi.get("product/all", "", this.props.user.token)
      .then(response => {
        let optionsNew = [];

        let i;

        for (i = 0; i < response.data.produtos.length; i++) {
          optionsNew[i] = {
            id: response.data.produtos[i].id,
            name: response.data.produtos[i].name
          };
        }

        this.setState({
          productNameList: optionsNew
        });
      })
      .catch(erro => {});
  }
  handleCollectionChange = event => {
    this.setState({
      collectionName: event.target.value,
      collectionClass: "form-control"
    });
  };

  handleAestheticChange = event => {
    this.setState({
      AestheticLine: event.target.value,
      collectionClass: "form-control"
    });
  };

  submitCollection = event => {
    event.preventDefault();

    let valid = true;
    let collectionClass = this.state.collectionClass;
    if (this.state.collectionName.length === 0) {
      this.setState({
        collectionClass: collectionClass.concat(" is-invalid")
      });
      return;
    }

    let products = [];

    this.setState({
      collectionClass: collectionClass
    });

    for (let i = 0; i < this.state.selectedProducts.length; i++) {
      products[i] = this.state.selectedProducts[i].name;
    }
    if (!valid) return;

    let data = {
      CollectionName: this.state.collectionName,
      AestheticLine: this.state.AestheticLine,
      ProductsNameList: products
    };

    ManagerApi.post("collection", data, this.props.user.token)
      .then(() => {
        Swall({
          type: "success",
          title: "Success",
          text: "Collection created successfully"
        });
      })
      .catch(error => {
        Swall({
          type: "error",
          title: "Error",
          text: error.response.data.errorMessage
        });
      });
  };

  handleSecondListSelectionChange = deselectedOptions => {
    var selectedProducts = this.state.selectedProducts.slice();
    deselectedOptions.forEach(option => {
      selectedProducts.splice(selectedProducts.indexOf(option), 1);
    });
    this.setState({ selectedProducts });
  };
  handleFirstListSelectionChange = selectedProducts => {
    selectedProducts.sort((a, b) => a.id - b.id);
    this.setState({ selectedProducts });
  };

  render() {
    return (
      <form className="needs-validation">
        <div className="form-group">
          <label htmlFor="collection">Collection name:</label>
          <input
            type="text"
            className={this.state.collectionClass}
            id="collection"
            placeholder="Input collection name"
            value={this.state.collectionName}
            onChange={this.handleCollectionChange}
          />
          <label htmlFor="collection">Aesthetic line:</label>
          <input
            type="text"
            className={this.state.collectionClass}
            id="aestheticLine"
            placeholder="Input aesthetic line"
            value={this.state.aestheticLine}
            onChange={this.handleAestheticChange}
          />
          <div className="invalid-feedback">The field should be filled</div>
          <label htmlFor="collection">Select collection products:</label>
          <div className="row">
            <div className="col-md-5">
              <FilteredMultiSelect
                buttonText="Select"
                classNames={FILTEREDMULTISELECT_CLASSES}
                onChange={this.handleFirstListSelectionChange}
                options={this.state.productNameList}
                selectedOptions={this.state.selectedProducts}
                textProp="name"
                valueProp="id"
              />
            </div>
            <div className="col-md-5">
              <FilteredMultiSelect
                buttonText="Remove"
                classNames={{
                  filter: "form-control",
                  select: "form-control",
                  button: "btn btn btn-block btn-default",
                  buttonActive: "btn btn btn-block btn-danger"
                }}
                onChange={this.handleSecondListSelectionChange}
                options={this.state.selectedProducts}
                textProp="name"
                valueProp="id"
              />
            </div>
          </div>
        </div>
        <button
          onClick={this.submitCollection}
          type="submit"
          className="btn btn-success"
        >
          Create
        </button>
      </form>
    );
  }
}

export default NewCollection;
