import React, { Component } from 'react'
import Swall from 'sweetalert2'

import Wrapper from '../../../../../hoc/Wrapper'

import Axios from '../../../../../api/ManagerApi'


class SearchCategory extends Component {

    state = {
        category: '',
        categoryClass: 'form-control',
        categoryInfo: ''
    }

    handleCategoryChange = (event) => {
        this.setState({ category: event.target.value, categoryClass: 'form-control' })
    }

    searchCategory = (event) => {
        event.preventDefault()

        if (this.state.category.length === 0) {
            this.setState({ categoryClass: this.state.categoryClass.concat(' is-invalid') })
            return
        }

        Axios.get("category/names", this.state.category, this.props.user.token).then((response) => {
            let childCategory = null
            

            if (response.data.childCategoriesNames.length !== 0) {
                childCategory = (
                    <div className="form-group row">
                        <label htmlFor="nome" className="col-sm-3 col-form-label">Nome das categorias filhas</label>
                        {
                            response.data.childCategoriesNames.map((category, i) => {
                                return(
                                    <div key={i} className="col-sm-3">
                                        <input type="text" readOnly className="form-control" id="nome" value={category}/>
                                    </div>
                                )
                            })
                        }

                    </div>
                )
            }

            let info = (
                <form>
                    <div className="form-group row">
                        <label htmlFor="name" className="col-sm-3 col-form-label">Nome</label>
                        <div className="col-sm-9">
                            <input type="text" readOnly className="form-control" id="name" value={response.data.categoryName} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="id" className="col-sm-3 col-form-label">Id</label>
                        <div className="col-sm-9">
                            <input type="text" readOnly className="form-control" id="id" value={response.data.categoryID} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="parentName" className="col-sm-3 col-form-label">Nome da categoria pai</label>
                        <div className="col-sm-9">
                            <input type="text" readOnly className="form-control" id="parentName" value={response.data.parentCategoryName} />
                        </div>
                    </div>
                    {childCategory}
                </form>
            )
            this.setState({ categoryInfo: info })

        }).catch((error) => {
            this.setState({ categoryInfo: '' })
            Swall({
                type: 'error',
                title: 'Erro',
                text: error.response.data.errorMessage
            })
        })
    }

    render() {
        return (
            <Wrapper>
                {this.state.categoryInfo}

                <form className="needs-validation">
                    <div className="form-group">
                        <label htmlFor="category">Categoria a procurar</label>
                        <input
                            type="text"
                            className={this.state.categoryClass}
                            id="category"
                            placeholder="Introduzir o nome da categoria"
                            value={this.state.category}
                            onChange={this.handleCategoryChange} />
                        <div className="invalid-feedback">
                            Deve preencher o campo
                        </div>
                    </div>
                    <button onClick={this.searchCategory} type="submit" className="btn btn-primary">Procurar</button>
                </form>

            </Wrapper>
        )
    }
}

export default SearchCategory