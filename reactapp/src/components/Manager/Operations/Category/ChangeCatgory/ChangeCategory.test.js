import React from 'react'
import { configure , shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ChangeCategory from './ChangeCategory';

configure({adapter: new Adapter()})

describe('<ChangeCategory />', () => {
    let wrapper

    beforeEach(() => {
        wrapper = shallow(<ChangeCategory/>)
    })

    it('should not be able to change category if no name specified', () => {
        expect(wrapper.instance().changeCategory()).toBeFalsy()
    })
})  