import React, { Component } from 'react'
import Swall from 'sweetalert2'
import Axios from '../../../../../api/ManagerApi'

import Wrapper from '../../../../../hoc/Wrapper'

class ChangeCategory extends Component {

    state = {
        parentCategory: '',
        category: '',
        parentCategoryClass: 'form-control',
        categoryClass: 'form-control'
    }

    handleParentCategoryChange = (event) => {
        this.setState({ parentCategory: event.target.value, parentCategoryClass: 'form-control' })
    }

    handleCategoryChange = (event) => {
        this.setState({ category: event.target.value, categoryClass: 'form-control' })
    }

    changeCategory = (event) => {
        let data = null
        let valid = true

        if (this.state.parentCategory.length === 0) {
            this.setState({ parentCategoryClass: this.state.parentCategoryClass.concat(' is-invalid') })
            valid = false
        }

        if (this.state.category.length === 0) {
            this.setState({ categoryClass: this.state.categoryClass.concat(' is-invalid') })
            valid = false
        }

        if (!valid) {
            return
        }

        data = {
            "category": this.state.category,
            "parentCagetory": this.state.parentCategory
        }

        Axios.put("category/names", data, this.props.user.token).then((response) => {
            Swall({
                type: 'success',
                title: 'Sucesso',
                text: 'Categoria alterada com sucesso'
            })
        }).catch((error) => {
            Swall({
                type: 'error',
                title: 'Erro',
                text: error.response.data.errorMessage
            })
        })
    }

    render() {
        return (
            <Wrapper>
                <form className="needs-validation">
                    <div className="form-group">
                        <label htmlFor="category">Nome da Categoria</label>
                        <input
                            type="text"
                            className={this.state.categoryClass}
                            id="category"
                            placeholder="Introduzir o nome da categoria"
                            value={this.state.category}
                            onChange={this.handleCategoryChange} />
                        <div className="invalid-feedback">
                            Deve preencher o campo
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="parentCategory">Nome da Categoria Pai</label>
                        <input
                            type="text"
                            className={this.state.parentCategoryClass}
                            id="parentCategory"
                            placeholder="Introduzir o nome da categoria"
                            value={this.state.parentCategory}
                            onChange={this.handleParentCategoryChange} />
                        <div className="invalid-feedback">
                            Deve preencher o campo
                        </div>
                    </div>
                    <button onClick={this.changeCategory} type="submit" className="btn btn-primary">Alterar</button>
                </form>
            </Wrapper>
        )
    }
}

export default ChangeCategory