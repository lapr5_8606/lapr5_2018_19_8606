import React, { Component } from "react";
import Swall from "sweetalert2";

import ManagerApi from "../../../../../api/ManagerApi";

class NewCategory extends Component {
  state = {
    hasParent: false,
    parentCategory: "",
    category: "",
    parentCategoryClass: "form-control",
    categoryClass: "form-control"
  };

  handleCheckEvent = () => {
    this.setState({ hasParent: !this.state.hasParent });
  };

  handleParentCategoryChange = event => {
    this.setState({
      parentCategory: event.target.value,
      parentCategoryClass: "form-control"
    });
  };

  handleCategoryChange = event => {
    this.setState({
      category: event.target.value,
      categoryClass: "form-control"
    });
  };

  submitCategory = event => {
    event.preventDefault();

    let data = null;
    let valid = true;

    if (this.state.hasParent) {
      if (this.state.parentCategory.length === 0) {
        this.setState({
          parentCategoryClass: this.state.parentCategoryClass.concat(
            " is-invalid"
          )
        });
        valid = false;
      }

      data = {
        CategoryName: this.state.category,
        ParentCategoryName: this.state.parentCategory
      };
    } else {
      data = {
        CategoryName: this.state.category
      };
    }

    if (this.state.category.length === 0) {
      this.setState({
        categoryClass: this.state.categoryClass.concat(" is-invalid")
      });
      valid = false;
    }

    if (!valid) {
      return;
    }

    ManagerApi.post("category", data, this.props.user.token)
      .then(response => {
        Swall({
          type: "success",
          title: "Sucesso",
          text: "Categoria criada com sucesso"
        });
      })
      .catch(error => {
        Swall({
          type: "error",
          title: "Erro",
          text: error.response.data.errorMessage
        });
      });
  };

  render() {
    let parentCategory = null;

    if (this.state.hasParent) {
      parentCategory = (
        <div className="form-group">
          <label htmlFor="parentCategory">Nome da Categoria Pai</label>
          <input
            type="text"
            className={this.state.parentCategoryClass}
            id="parentCategory"
            placeholder="Introduzir o nome da categoria"
            value={this.state.parentCategory}
            onChange={this.handleParentCategoryChange}
          />
          <div className="invalid-feedback">Deve preencher o campo</div>
        </div>
      );
    }

    return (
      <form className="needs-validation">
        <div className="form-group">
          <label htmlFor="category">Nome da Categoria</label>
          <input
            type="text"
            className={this.state.categoryClass}
            id="category"
            placeholder="Introduzir o nome da categoria"
            value={this.state.category}
            onChange={this.handleCategoryChange}
          />
          <div className="invalid-feedback">Deve preencher o campo</div>
        </div>
        {parentCategory}
        <div className="form-group">
          <div className="form-check">
            <input
              onChange={this.handleCheckEvent}
              className="form-check-input"
              type="checkbox"
              id="hasParent"
            />
            <label className="form-check-label" htmlFor="hasParent">
              Tem categoria pai
            </label>
          </div>
        </div>
        <button
          onClick={this.submitCategory}
          type="submit"
          className="btn btn-primary"
        >
          Submeter
        </button>
      </form>
    );
  }
}

export default NewCategory;
