import React, { Component } from 'react'
import Swall from 'sweetalert2'
import Axios from '../../../../../api/ManagerApi'

class ListAllCategories extends Component {

    state = {
        categories: []
    }
 

    componentDidMount() {
        Axios.get("category/all", "", this.props.user.token).then((response) => {
            this.setState({ categories: response.data.categories })
        }).catch((error) => {
            console.log(JSON.stringify(error))
            Swall({
                type: 'error',
                title: 'Erro',
                text: error
            })
        })
    }

    render() {
        return (
            <form>
                <label>Lista De Categorias</label>
                {
                    this.state.categories.map((categoria, i) => {
                        return (
                            <div className="form-group row" key={i}>
                                <label htmlFor="name" className="col-sm-2 col-form-label">Nome</label>
                                <div className="col-sm-10">
                                    <input type="text" readonly className="form-control" disabled={true} id="name"
                                        value={categoria.categoryName} />
                                    
                                    <label htmlFor="name" className="col-sm-3 col-form-label">Sub-Categorias:</label>{

                                            categoria.childCategoriesNames.map((subCatName, j) => {
                                            return (
                                                <input type="text" readonly className="form-control" disabled={true} id="subCatName"
                                                    value={subCatName} />
                                            )
                                        })

                                    }
                                    <hr></hr>
                                </div>
                            </div>
                        )
                    })
                }
            </form>
        )
    }
}

export default ListAllCategories