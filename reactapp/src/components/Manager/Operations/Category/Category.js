import React, { Component } from 'react'

import Wrapper from '../../../../hoc/Wrapper'
import '../../../Layout/Layout.css'

import NewCategory from './NewCategory/NewCategory'
import ChangeCategory from './ChangeCatgory/ChangeCategory'
import SearchCategory from './SearchCategory/SearchCategory'
import ListAllCategories from './ListAllCategories/ListAllCategories'
class Category extends Component {

    state = {
        operations: ['Adicionar', 'Alterar', 'Procurar', 'Listar todos'],
        selected: 0,
        pages: [<NewCategory user={this.props.user}/>, <ChangeCategory user={this.props.user}/>, <SearchCategory user={this.props.user}/>,<ListAllCategories user={this.props.user}/>],
        page: <NewCategory user={this.props.user} />
    }

    changeOperation = (index) => {
        this.setState({ selected: index, page: this.state.pages[index] })
    }

    render() {
        return (
            <Wrapper>
                <ul className="nav nav-tabs justify-content-center">
                    {
                        this.state.operations.map((operation, i) => {
                            return (
                                <li className="nav-item" key={i}>
                                    { /* eslint-disable-next-line */}
                                    <a
                                        onClick={() => this.changeOperation(i)}
                                        className={"nav-link" + (i === this.state.selected ? " active" : "")}
                                        href="#">
                                        {operation}
                                    </a>
                                </li>
                            )
                        })
                    }
                </ul>
                <div className="subContainer">
                    {this.state.page}
                </div>

            </Wrapper>
        )
    }
}

export default Category