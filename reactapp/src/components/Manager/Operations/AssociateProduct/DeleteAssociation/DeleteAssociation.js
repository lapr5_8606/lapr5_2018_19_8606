import React, { Component } from 'react'
import Swall from 'sweetalert2'
import Axios from '../../../../../api/ManagerApi'

class DeleteAssociation extends Component {

    state = {
        parentProductName: '',
        parentProductNameClass: 'form-control',
        childProductName: '',
        childProductNameClass: 'form-control'
    }

    handleParentProductNameChange = (event) => {
        this.setState({parentProductName: event.target.value, parentProductNameClass: 'form-control'})
    }

    handleChildProductNameChange = (event) => {
        this.setState({childProductName: event.target.value, childProductNameClass: 'form-control'})
    }

    handleDelete = (event) => {
        event.preventDefault()

        let valid = true

        let pC = this.state.parentProductNameClass
        let cC = this.state.childProductNameClass
       
        if(this.state.parentProductName.length === 0) {
            pC = pC.concat(" is-invalid")
            valid = false
        }

        if(this.state.childProductName.length === 0) {
            cC = cC.concat(" is-invalid")
            valid = false
        }

        this.setState({parentProductNameClass: pC, childProductNameClass: cC})

        if(!valid)
            return

        let data = {
            "ParentName" : this.state.parentProductName,
            "ChildName" : this.state.childProductName
        }       
        Axios.deleteWithBody("product/names/associate", data, this.props.user.token).then(() => {
            Swall({
                type: 'success',
                title: 'Sucesso',
                text: 'Associação apagado com sucesso'
            })
        }).catch((error) => {
            Swall({
                type: 'error',
                title: 'Erro',
                text: error.response.data.errorMessage
            })
        })

    }  

    render () {
        return (
            <form className="needs-validation">
                <div className="form-group">
                    <label htmlFor="parentProduct">Nome do produto pai</label>
                    <input 
                        id="parentProduct"
                        type="text"
                        placeholder="Introduzir o nome do produto"
                        className={this.state.parentProductNameClass}
                        value={this.state.parentProductName}
                        onChange={this.handleParentProductNameChange}/>
                    <div className="invalid-feedback">
                        Deve preencher este campo
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="childProduct">Nome do produto filho</label>
                    <input 
                        id="childProduct"
                        type="text"
                        placeholder="Introduzir o nome do produto"
                        className={this.state.childProductNameClass}
                        value={this.state.childProductName}
                        onChange={this.handleChildProductNameChange}/>
                    <div className="invalid-feedback">
                        Deve preencher este campo
                    </div>
                </div>
                <div className="form-group">
                    <button 
                        onClick={this.handleDelete}
                        className="btn btn-danger">Apagar</button>
                </div>
            </form>
        )
    }
}

export default DeleteAssociation