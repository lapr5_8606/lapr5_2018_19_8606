import React, { Component } from 'react'

import Wrapper from '../../../../hoc/Wrapper'
import NewAssociation from './NewAssociation/NewAssociantion'
import DeleteAssocation from './DeleteAssociation/DeleteAssociation'
import ChangeAssociation from './ChangeAssociation/ChangeAssociation';

class AsssociateProduct extends Component {

    state = {
        operations: ['Criar', 'Apagar', 'Alterar'],
        selected: 0,
        pages: [<NewAssociation user={this.props.user}/>, <DeleteAssocation user={this.props.user}/>, <ChangeAssociation user={this.props.user}/>],
        page: <NewAssociation user={this.props.user}/>
    }

    changeOperation = (index) => {
        this.setState({ selected: index, page: this.state.pages[index] })
    }

    render() {
        return (
            <Wrapper>
                <ul className="nav nav-tabs justify-content-center">
                    {
                        this.state.operations.map((operation, i) => {
                            return (
                                <li className="nav-item" key={i}>
                                    { /* eslint-disable-next-line */}
                                    <a
                                        onClick={() => this.changeOperation(i)}
                                        className={"nav-link" + (i === this.state.selected ? " active" : "")}
                                        href="#">
                                        {operation}
                                    </a>
                                </li>
                            )
                        })
                    }
                </ul>
                <div className="subContainer">
                    {this.state.page}
                </div>
            </Wrapper>
        )
    }
}

export default AsssociateProduct