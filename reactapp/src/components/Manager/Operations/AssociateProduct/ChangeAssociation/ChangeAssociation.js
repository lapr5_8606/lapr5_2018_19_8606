import React, { Component } from 'react'

import Axios from '../../../../../api/ManagerApi'
import Swall from 'sweetalert2'

import Wrapper from '../../../../../hoc/Wrapper'


class ChangeAssociation extends Component {
    state = {
        parentName: '',
        parentNameClass: 'form-control',
        childName: '',
        childNameClass: 'form-control',
        isMandatory: false,
        isMaterial: false,
        isPercentage: false,
        minWidthPercentage: '',
        minWidthPercentageClass: 'form-control',
        maxWidhtPercentage: '',
        maxWidhtPercentageClass: 'form-control',
        minHeightPercentage: '',
        minHeightPercentageClass: 'form-control',
        maxHeightPercentage: '',
        maxHeightPercentageClass: 'form-control',
        minDepthPercentage: '',
        minDepthPercentageClass: 'form-control',
        maxDepthPercentage: '',
        maxDepthPercentageClass: 'form-control'
    }

    handleParentNameChange = (event) => {
        this.setState({ parentName: event.target.value, parentNameClass: 'form-control' })
    }

    handleChildNameChange = (event) => {
        this.setState({ childName: event.target.value, childNameClass: 'form-control' })
    }

    handleMandatoryCheckEvent = () => {
        this.setState({ isMandatory: !this.state.isMandatory })
    }

    handleMaterialCheckEvent = () => {
        this.setState({ isMandatory: !this.state.isMaterial })
    }

    handlePercentageCheckEvent = () => {
        this.setState({ isPercentage: !this.state.isPercentage })
    }

    handleMinWidthPercentage = (event) => {
        this.setState({ minWidthPercentage: (event.target.validity.valid) ? event.target.value : this.state.minWidthPercentage, minWidthPercentageClass: 'form-control' })
    }

    handleMaxWidthPercentage = (event) => {
        this.setState({ maxWidhtPercentage: (event.target.validity.valid) ? event.target.value : this.state.maxWidhtPercentage, maxWidhtPercentageClass: 'form-control' })
    }

    handleMinHeighPercentage = (event) => {
        this.setState({ minHeightPercentage: (event.target.validity.valid) ? event.target.value : this.state.minHeightPercentage, minHeightPercentageClass: 'form-control' })
    }

    handleMaxHeighPercentage = (event) => {
        this.setState({ maxHeightPercentage: (event.target.validity.valid) ? event.target.value : this.state.maxHeightPercentage, maxHeightPercentageClass: 'form-control' })
    }

    handleMinDepthercentage = (event) => {
        this.setState({ minDepthPercentage: (event.target.validity.valid) ? event.target.value : this.state.minDepthPercentage, minDepthPercentageClass: 'form-control' })
    }

    handleMaxDepthPercentage = (event) => {
        this.setState({ maxDepthPercentage: (event.target.validity.valid) ? event.target.value : this.state.maxDepthPercentage, maxDepthPercentageClass: 'form-control' })
    }

    createMandatoryComponent = () => {
        return (
            <div className="form-group">
                <div className="form-check">
                    <input onChange={this.handleMandatoryCheckEvent} className="form-check-input" type="checkbox" id="isMandatory" />
                    <label className="form-check-label" htmlFor="isMandatory">
                        É obrigatorio
                    </label>
                </div>
            </div>
        )
    }

    createMaterialComponent = () => {
        return (
            <div className="form-group">
                <div className="form-check">
                    <input onChange={this.handleMaterialCheckEvent} className="form-check-input" type="checkbox" id="isMaterial" />
                    <label className="form-check-label" htmlFor="isMaterial">
                        Restricção de materiais
                        </label>
                </div>
            </div>
        )
    }

    createPercentageComponent = () => {

        let config = null

        if (this.state.isPercentage) {
            config = (
                <Wrapper>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Largura</label>
                        <div className="form-group col-sm-5">
                            <label>Valor Minimo</label>
                            <input
                                type="text"
                                pattern="[0-9]*?.?[0-9]*"
                                placeholder="Introduzir o valor minimo"
                                value={this.state.minWidthPercentage}
                                onChange={this.handleMinWidthPercentage}
                                className={this.state.minWidthPercentageClass} />
                            <div className="invalid-feedback">
                                Deve preencher este campo
                        </div>
                        </div>
                        <div className="form-group col-sm-5">
                            <label>Valor Máximo</label>
                            <input
                                type="text"
                                pattern="[0-9]*?.?[0-9]*"
                                placeholder="Introduzir o valor maximo"
                                value={this.state.maxWidhtPercentage}
                                onChange={this.handleMaxWidthPercentage}
                                className={this.state.maxWidhtPercentageClass} />
                            <div className="invalid-feedback">
                                Deve preencher este campo
                        </div>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Altura</label>
                        <div className="form-group col-sm-5">
                            <label>Valor Minimo</label>
                            <input
                                type="text"
                                pattern="[0-9]*?.?[0-9]*"
                                placeholder="Introduzir o valior minimo"
                                value={this.state.minHeightPercentage}
                                onChange={this.handleMinHeighPercentage}
                                className={this.state.minHeightPercentageClass} />
                            <div className="invalid-feedback">
                                Deve preencher este campo
                        </div>
                        </div>
                        <div className="form-group col-sm-5">
                            <label>Valor Máximo</label>
                            <input
                                type="text"
                                pattern="[0-9]*?.?[0-9]*"
                                placeholder="Introduzir o valor maximo"
                                value={this.state.maxHeightPercentage}
                                onChange={this.handleMaxHeighPercentage}
                                className={this.state.maxHeightPercentageClass} />
                            <div className="invalid-feedback">
                                Deve preencher este campo
                        </div>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Produndidade</label>
                        <div className="form-group col-sm-5">
                            <label>Valor Minimo</label>
                            <input
                                type="text"
                                pattern="[0-9]*?.?[0-9]*"
                                placeholder="Introduzir o valior minimo"
                                value={this.state.minDepthPercentage}
                                onChange={this.handleMinDepthercentage}
                                className={this.state.minDepthPercentageClass} />
                            <div className="invalid-feedback">
                                Deve preencher este campo
                        </div>
                        </div>
                        <div className="form-group col-sm-5">
                            <label>Valor Máximo</label>
                            <input
                                type="text"
                                pattern="[0-9]*?.?[0-9]*"
                                placeholder="Introduzir o valor maximo"
                                value={this.state.maxDepthPercentage}
                                onChange={this.handleMaxDepthPercentage}
                                className={this.state.maxDepthPercentageClass} />
                            <div className="invalid-feedback">
                                Deve preencher este campo
                        </div>
                        </div>
                    </div>
                </Wrapper>
            )
        }

        return (
            <Wrapper>
                <div className="form-group">
                    <div className="form-check">
                        <input onChange={this.handlePercentageCheckEvent} className="form-check-input" type="checkbox" id="isPercentage" />
                        <label className="form-check-label" htmlFor="isPercentage">
                            Restricção de percentagem
                        </label>
                    </div>
                </div>

                {config}
            </Wrapper>
        )
    }

    handleChangeAssociation = (event) => {
        event.preventDefault()

        let valid = true
        
        if(this.state.childName.length === 0) {
            valid = false
            this.setState({childNameClass: this.state.childNameClass.concat(" is-invalid")})
        }

        if(this.state.parentName.length === 0) {
            valid = false
            this.setState({parentNameClass: this.state.parentNameClass.concat(" is-invalid")})
        }

        if(!valid) {
            return 
        }

        let data = {
            "Parent": this.state.parentName,
            "Child": this.state.childName
        }

        if(this.state.isMandatory) {
            data["IsMandatory"] = true
        } else {
            data["IsMandatory"] = false
        }

        if(this.state.isMaterial) {
            data["RestrictMaterials"] = true
        } else {
            data["RestrictMaterials"] = false
        }

        if(this.state.isPercentage) {

            if(this.state.minDepthPercentage === 0 || this.state.minDepthPercentage.length === 0) {
                valid = false
                this.setState({minDepthPercentageClass: this.state.minDepthPercentageClass.concat(" is-invalid")})
            }

            if(this.state.maxDepthPercentage === 0 || this.state.maxDepthPercentage.length === 0) {
                valid = false
                this.setState({maxDepthPercentageClass: this.state.maxDepthPercentageClass.concat(" is-invalid")})
            }

            if(this.state.minWidthPercentage === 0 || this.state.minWidthPercentage.length === 0) {
                valid = false
                this.setState({minWidthPercentageClass: this.state.minWidthPercentageClass.concat(" is-invalid")})
            }

            if(this.state.maxWidhtPercentage === 0 || this.state.maxWidhtPercentage.length === 0) {
                valid = false
                this.setState({maxWidhtPercentageClass: this.state.maxWidhtPercentageClass.concat(" is-invalid")})
            }

            if(this.state.minHeightPercentage === 0 || this.state.minHeightPercentage.length === 0) {
                valid = false
                this.setState({minHeightPercentageClass: this.state.minHeightPercentageClass.concat(" is-invalid")})
            }

            if(this.state.maxHeightPercentage === 0 || this.state.maxHeightPercentage.length === 0) {
                valid = false
                this.setState({maxHeightPercentageClass: this.state.maxHeightPercentageClass.concat(" is-invalid")})
            }

            if(!valid) {
                return
            }

            data["PercentageRestrictions"] = [this.state.minHeightPercentage, this.state.maxHeightPercentage,
                this.state.minWidthPercentage, this.state.maxWidhtPercentage, this.state.minDepthPercentage, this.state.maxDepthPercentage]
        } else {
            data["PercentageRestrictions"] = []
        }
        
        Axios.put("product/names/associate", data, this.props.user.token).then(() => {
            Swall({
                type: 'success',
                title: 'Sucesso',
                text: 'Associação alterada com sucesso'
            })
        }).catch((error) => {
            Swall({
                type: 'error',
                title: 'Erro',
                text: error.response.data.errorMessage
            })
        })
        
    }
    render() {
        return (
            <form>
                <div className="form-group">
                    <label htmlFor="parentName">Nome do produto pai</label>
                    <input
                        type="text"
                        id="parentName"
                        placeholder="Introduzir o nome do produto"
                        value={this.state.parentName}
                        onChange={this.handleParentNameChange}
                        className={this.state.parentNameClass} />
                    <div className="invalid-feedback">
                        Deve preencher este campo
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="childName">Nome do produto filho</label>
                    <input
                        type="text"
                        id="childName"
                        placeholder="Introduzir o nome do produto"
                        value={this.state.childName}
                        onChange={this.handleChildNameChange}
                        className={this.state.childNameClass} />
                    <div className="invalid-feedback">
                        Deve preencher este campo
                    </div>
                </div>

                {this.createMandatoryComponent()}

                {this.createMaterialComponent()}

                {this.createPercentageComponent()}

                <button onClick={this.handleChangeAssociation} type="submit" className="btn btn-primary">Alterar</button>
            </form>
        )
    }
}

export default ChangeAssociation