import React, { Component } from 'react'

import Wrapper from '../../../../hoc/Wrapper'


import CreateMaterial from './CreateMaterial/CreateMaterial'
import DeleteMaterial from './DeleteMaterial/DeleteMaterial'
import UpdateMaterial from './UpdateMaterial/UpdateMaterial'
import SearchMaterial from './SearchMaterial/SearchMaterial';
import ListAllMaterials from './ListAllMaterials/ListAllMaterials';

class Material extends Component {

    state = {
        operations: ['Criar', 'Apagar', 'Alterar', 'Procurar','Listar todos'],
        selected: 0,
        pages: [<CreateMaterial user={this.props.user}/>, <DeleteMaterial user={this.props.user}/>, <UpdateMaterial user={this.props.user}/>, <SearchMaterial user={this.props.user}/>,<ListAllMaterials  user={this.props.user}/>],
        page: <CreateMaterial user={this.props.user}/>
    }

    changeOperation = (index) => {
        this.setState({ selected: index, page: this.state.pages[index] })
    }

    render() {
        return (
            <Wrapper>
                <ul className="nav nav-tabs justify-content-center">
                    {
                        this.state.operations.map((operation, i) => {
                            return (
                                <li className="nav-item" key={i}>
                                    { /* eslint-disable-next-line */}
                                    <a
                                        onClick={() => this.changeOperation(i)}
                                        className={"nav-link" + (i === this.state.selected ? " active" : "")}
                                        href="#">
                                        {operation}
                                    </a>
                                </li>
                            )
                        })
                    }
                </ul>
                <div className="subContainer">
                    {this.state.page}
                </div>
            </Wrapper>
        )
    }
}

export default Material