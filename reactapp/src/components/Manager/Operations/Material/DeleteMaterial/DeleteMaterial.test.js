import React from 'react'
import { configure , shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import DeleteMaterial from './DeleteMaterial'

configure({adapter: new Adapter()})

describe('<DeleteMaterial />', () => {
    let wrapper

    beforeEach(() => {
        wrapper = shallow(<DeleteMaterial />)
    })

    it('should have a default empty text box', () => {
        let input = wrapper.find('input')
        expect(input.props().value).toEqual('')
    })

    it('should not allow to delete if there is no input', () => {
        expect(wrapper.instance().deleteMaterial()).toBeFalsy()
        expect(wrapper.state("materialNameClass")).toEqual("form-control is-invalid")
    })
})