import React , { Component } from 'react'

import Swall from 'sweetalert2'
import Axios from '../../../../../api/ManagerApi'

class DeleteMaterial extends Component {

    state = {
        materialName: '',
        materialNameClass: 'form-control'
    }

    handleMaterialNameChange = (event) => {
        this.setState({materialName: event.target.value, materialNameClass: 'form-control'})
    }

    deleteMaterial = (event) => {

        if(this.state.materialName.length === 0) {
            this.setState({materialNameClass: this.state.materialNameClass.concat(" is-invalid")})
            return false
        }

        Axios.delete("material/names", this.state.materialName, this.props.user.token).then(() => {
            Swall({
                type: 'success',
                title: 'Sucesso',
                text: 'Material apagado com sucesso'
            })
        }).catch((error) => {
            Swall({
                type: 'error',
                title: 'Erro',
                text: error.response.data.errorMessage
            })
        })

    }
    render() {
        return (
            <form className="needs-validation">
                <div className="form-group">
                    <label htmlFor="material">Nome do material</label>
                    <input 
                        type="text"
                        id="material"
                        className={this.state.materialNameClass}
                        value={this.state.materialName}
                        placeholder="Introduzir o nome do material"
                        onChange={this.handleMaterialNameChange}/>
                        <div className="invalid-feedback">
                            Deve preencher o campo
                        </div>
                </div>
                <button onClick={this.deleteMaterial} type="submit" className="btn btn-danger">Apagar</button>
            </form>
        )
    }
}

export default DeleteMaterial