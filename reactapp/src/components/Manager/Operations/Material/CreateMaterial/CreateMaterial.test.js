import React from 'react'
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import CreateMaterial from './CreateMaterial';

configure({ adapter: new Adapter() })

describe('<CreateMaterial />', () => {
    let wrapper

    beforeEach(() => {
        wrapper = shallow(<CreateMaterial />)
    })

    it('should by default have empty name', () => {
        expect(wrapper.state("materialName")).toEqual('')
    })
    
    it('should by default only have a finish', () => {
        expect(wrapper.state("finishName")).toHaveLength(1)
    })

    it('should be able to add another finish if previous are filled', () => {
        wrapper.setState({
            materialName: 'mat',
            materialNameClass: 'form-control',
            finishName: ['mat'],
            finishNameClass: ['form-control']
        })

        wrapper.instance().handleFinishClick()

        expect(wrapper.state("finishName")).toHaveLength(2)
    })

    it('should have the correct amount of inputs', () => {
        expect(wrapper.find('input')).toHaveLength(2)
    })

    it('should not be able to add nother finish if previous are not filled', () => {
        expect(wrapper.instance().handleFinishClick()).toBeFalsy()
    })

    it('should not be able to delete a finish if it has just one', () => {
        expect(wrapper.instance().handleDeleteMaterialClick(0)).toBeFalsy()
    })

    it('should be able to delete if there are more than one finish', () => {
        wrapper.setState({
            materialName: 'mat',
            materialNameClass: 'form-control',
            finishName: ['mat', 'mat2'],
            finishNameClass: ['form-control', 'form-control']
        })

        wrapper.instance().handleDeleteMaterialClick(1)

        expect(wrapper.state("finishName")).toHaveLength(1)
    })
})