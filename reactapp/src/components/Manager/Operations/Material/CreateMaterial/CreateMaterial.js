import React, {Component} from "react";
import Swall from "sweetalert2";
import Axios from "../../../../../api/ManagerApi";

class CreateMaterial extends Component {
    state = {
        materialName: "",
        materialPrice: 0,
        materialNameClass: "form-control",
        prices: [],
        finishName: [""],
        finishNameClass: ["form-control"]
    };

    handleMaterialNameChange = event => {
        this.setState({
            materialName: event.target.value,
            materialNameClass: "form-control"
        });
    }

    handleMaterialPriceChange = event => {
        this.setState({
            materialPrice: event.target.value
        });
    }

    handleFinishNameChange = (event, index) => {
        let f = [...this.state.finishName];
        let fc = [...this.state.finishNameClass];
        f[index] = event.target.value;
        fc[index] = "form-control";
        this.setState({finishName: f, finishNameClass: fc});
    };

    handleFinishPriceChange = (event, index) => {
        let prices = [...this.state.prices];
        prices[index] = event.target.value;

        this.setState({prices: prices});
    }

    handleDeleteMaterialClick = index => {
        if (this.state.finishName.length === 1) {
            return false;
        }

        let f = [...this.state.finishName];
        let fc = [...this.state.finishNameClass];

        f.splice(index, 1);
        fc.splice(index, 1);

        this.setState({finishName: f, finishNameClass: fc});
    };

    handleFinishClick = event => {
        let valid = true;
        for (let i = 0; i < this.state.finishName.length; i++) {
            if (this.state.finishName[i].length === 0) {
                valid = false;
                let newClass = [...this.state.finishNameClass];
                newClass[i] = newClass[i].concat(" is-invalid");
                this.setState({finishNameClass: newClass});
            }
        }

        if (!valid) {
            return false;
        }

        let f = [...this.state.finishName];
        f.push("");
        let fc = [...this.state.finishNameClass];
        fc.push("form-control");
        let prices = [...this.state.prices]
        prices.push(-1);
        this.setState({finishName: f, finishNameClass: fc});
    };

    createFinishComponent = () => {
        return (
            <div className="form-group">
                <label>Acabamentos</label>
                {this.state.finishName.map((finish, index) => {
                    return (
                        <div className="input-group mb-3" key={index}>
                            <input
                                className={this.state.finishNameClass[index]}
                                placeholder="Introduzir o nome do acabamento"
                                value={this.state.finishName[index]}
                                onChange={event => this.handleFinishNameChange(event, index)}
                            />
                            <input
                                type="number"
                                placeholder="Incremento Acabamento"
                                value={this.state.prices[index]}
                                onChange={event => this.handleFinishPriceChange(event, index)}
                            />
                            <div className="input-group-append">
                                <button
                                    onClick={event =>
                                        this.handleDeleteMaterialClick(event, index)
                                    }
                                    type="button"
                                    className="btn btn-danger"
                                >
                                    Apagar
                                </button>
                            </div>
                        </div>
                    );
                })}
                <button
                    onClick={this.handleFinishClick}
                    type="button"
                    className="btn btn-success"
                >
                    Adicionar mais acabamentos
                </button>
            </div>
        );
    };

    submitMaterial = event => {
        event.preventDefault();
        let valid = true;
        let materialNameClass = this.state.materialNameClass;
        let finishClasses = [...this.state.finishNameClass];
        if (this.state.materialName.length === 0) {
            materialNameClass = materialNameClass.concat(" is-invalid");
        }

        let data = {
            MaterialName: this.state.materialName,
            MaterialPrice: this.state.materialPrice
        };

        let finishes = [];
        for (let i = 0; i < this.state.finishNameClass.length; i++) {
            if (this.state.finishName[i].length === 0) {
                valid = false;
                finishClasses[i] = finishClasses.concat(" is-invalid");
            } else {
                finishes.push({
                    Name: this.state.finishName[i],
                    PriceIncrement: this.state.prices[i]
                });
            }
        }

        this.setState({
            materialNameClass: materialNameClass,
            finishClasses: finishClasses
        });
        if (!valid) return;

        data["Finishes"] = finishes;

        console.log(data);

        console.log(this.props.user.token);

        Axios.post("material", data, this.props.user.token)
            .then(() => {
                Swall({
                    type: "success",
                    title: "Sucesso",
                    text: "Material criado com sucesso"
                });
            })
            .catch(error => {
                Swall({
                    type: "error",
                    title: "Erro",
                    text: error.response.data.errorMessage
                });
            });
    };

    render() {
        return (
            <form className="needs-validation">
                <div className="form-group">
                    <label htmlFor="material">Nome do Material</label>
                    <input
                        type="text"
                        className={this.state.materialNameClass}
                        id="material"
                        placeholder="Introduzir o nome do material"
                        value={this.state.materialName}
                        onChange={this.handleMaterialNameChange}
                    />
                    <div className="invalid-feedback">Deve preencher o campo</div>
                    <label htmlFor="materialPrice">Preço do Material</label>
                    <input
                        type="number"
                        min="0.0"
                        className="form-control"
                        id="materialPrice"
                        placeholder="Preço por m2"
                        value={this.state.materialPrice}
                        onChange={this.handleMaterialPriceChange}>

                    </input>
                </div>
                {this.createFinishComponent()}
                <button
                    onClick={this.submitMaterial}
                    type="submit"
                    className="btn btn-success"
                >
                    Criar
                </button>
            </form>
        );
    }
}

export default CreateMaterial;
