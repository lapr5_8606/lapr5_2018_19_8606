import React from 'react'
import { configure , shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import SearchMaterial from './SearchMaterial';

configure({adapter: new Adapter()})

describe('<SearchMaterial />', () => {
    let wrapper

    beforeEach(() => {
        wrapper = shallow(<SearchMaterial />)
    })

    it('should have a default empty text box', () => {
        let input = wrapper.find('input')
        expect(input.props().value).toEqual('')
    })

    it('should not allow to search if there is no input', () => {
        expect(wrapper.instance().searchMaterial()).toBeFalsy()
        expect(wrapper.state("materialNameClass")).toEqual("form-control is-invalid")
    })
})