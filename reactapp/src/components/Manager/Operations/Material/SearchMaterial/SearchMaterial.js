import React, {Component} from 'react'

import Swall from 'sweetalert2'
import Axios from '../../../../../api/ManagerApi'

class SearchMaterial extends Component {

    state = {
        materialName: '',
        materialNameClass: 'form-control',
        materialPrice: -1,
        priceEntries: [],
        finishPrices: [],
        finishName: [],
        finishNameClass: []
    }

    handleMaterialNameChange = (event) => {
        this.setState({materialName: event.target.value, materialNameClass: 'form-control'})
    }

    searchMaterial = (event) => {
        if (this.state.materialName.length === 0) {
            this.setState({materialNameClass: this.state.materialNameClass.concat(" is-invalid")})
            return false
        }

        Axios.get("material/names", this.state.materialName, this.props.user.token).then((response) => {
            let f = []
            let fc = []
            let finishPrices = [];
            for (let i = 0; i < response.data.availableFinishes.length; i++) {
                f.push(response.data.availableFinishes[i].name)
                fc.push('form-control')
                finishPrices.push(response.data.availableFinishes[i].priceIncrement);
            }

            let entries = [];
            for (let i = 0; i < response.data.priceHistory.length; i++) {
                entries.push(response.data.priceHistory[i]);
            }

            this.setState(
                {
                    finishName: f,
                    finishNameClass: fc,
                    materialPrice: response.data.price,
                    priceEntries: entries
                }
            )
        }).catch((error) => {
            Swall({
                type: 'error',
                title: 'Erro',
                text: error.response.data.errorMessage
            })
        })
    }

    createFinishComponent = () => {
        if (this.state.finishName.length >= 1)
            return (
                <div className="form-group">
                    <label>Acabamentos</label>
                    {
                        this.state.finishName.map((finish, index) => {
                            return (
                                <div className="input-group mb-3" key={index}>
                                    <input
                                        disabled={true}
                                        className={this.state.finishNameClass[index]}
                                        placeholder="Introduzir o nome do acabamento"
                                        value={this.state.finishName[index]}
                                        onChange={(event) => this.handleFinishNameChange(event, index)}/>
                                    <label>Increment: {this.state.finishPrices[index]}</label>
                                </div>
                            )
                        })
                    }
                </div>
            )
    }

    createPriceHistoryComponent() {
        if (this.state.priceEntries.length != 0) {
            return (
                <table className="table form-group">
                    <thead>
                    <tr>
                        <th scope="col">Price</th>
                        <th scope="col">Date Modified</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.priceEntries.map((entry) => {
                        return (
                            <tr>
                                <td>{entry.value}</td>
                                <td>{entry.changedOn}</td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>);
        }
    }

    render() {
        return (
            <form className="needs-validation">
                <div className="form-group">
                    <label htmlFor="material">Nome do Material</label>
                    <input
                        type="text"
                        className={this.state.materialNameClass}
                        id="material"
                        placeholder="Introduzir o nome do material"
                        value={this.state.materialName}
                        onChange={this.handleMaterialNameChange}/>
                    <div className="invalid-feedback">
                        Deve preencher o campo
                    </div>
                </div>
                {this.createFinishComponent()}
                {this.createPriceHistoryComponent()}
                <button onClick={this.searchMaterial} type="submit" className="btn btn-primary">Procurar</button>
            </form>
        )
    }
}

export default SearchMaterial