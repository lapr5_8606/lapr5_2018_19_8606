import React from 'react'
import { configure , shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import Material from './Material'
import CreateMaterial from './CreateMaterial/CreateMaterial';
import DeleteMaterial from './DeleteMaterial/DeleteMaterial';

configure({adapter: new Adapter()})

describe('<Material />', () => {
    let wrapper

    beforeEach(() => {
        wrapper = shallow(<Material />)
    })

    it('should display <CreateMaterial /> by default', () => {
        expect(wrapper.find(CreateMaterial)).toHaveLength(1)   
    })

    it('Should display <DeleteMaterial /> if state set to product', () => {
        wrapper.setState({page: <DeleteMaterial />})
        expect(wrapper.find(DeleteMaterial)).toHaveLength(1)
    })

    it('Should change selected state when ChangeOperation is called', () => {
        wrapper.instance().changeOperation(1)
        expect(wrapper.state('selected')).toEqual(1)
        expect(wrapper.find(DeleteMaterial)).toHaveLength(1)
    })
})