import React, { Component } from "react";
import Swall from "sweetalert2";
import Axios from "../../../../../api/ManagerApi";

class ListAllMaterials extends Component {
  state = {
    materials: []
  };

  componentDidMount() {
    Axios.get("material/all", "", this.props.user.token)
      .then(response => {
        this.setState({ materials: response.data.materiais });
      })
      .catch(error => {
        console.log(JSON.stringify(error))
        Swall({
            type: 'error',
            title: 'Erro',
            text: error
        })
      });
  }

  render() {
    return (
      <form>
        <label>Lista De Materiais</label>
        {this.state.materials.map((material, i) => {
          return (
            <div className="form-group row" key={i}>
              <label htmlFor="name" className="col-sm-2 col-form-label">
                Nome
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readonly
                  className="form-control"
                  disabled={true}
                  id="name"
                  value={this.state.materials[i].name}
                />
                  <label>Price: {this.state.materials[i].price}</label>
                <label htmlFor="name" className="col-sm-2 col-form-label">
                  Acabamentos:
                </label>
                {this.state.materials[i].availableFinishes.map((finish, j) => {
                  return (
                      <div>
                          <input
                              type="text"
                              readOnly
                              className="form-control"
                              disabled={true}
                              id="finishName"
                              value={finish.name}
                          />
                          <label className="form-control">Increment: {finish.priceIncrement}</label>
                      </div>
                  );
                })}
                <hr />
              </div>
            </div>
          );
        })}
      </form>
    );
  }
}

export default ListAllMaterials;
