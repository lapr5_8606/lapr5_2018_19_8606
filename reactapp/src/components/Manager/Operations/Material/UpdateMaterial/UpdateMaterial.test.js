import React from 'react'
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import UpdateMaterial from './UpdateMaterial';

configure({ adapter: new Adapter() })

describe('<UpdateMaterial />', () => {

    let wrapper
    beforeEach(() => {
        wrapper = shallow(<UpdateMaterial />)
    })
    
    it('should update the finishName value on index', () => {
        wrapper.instance().handleFinishNameChange({
            target : {
                value: "teste"
            }
        }, 0)

        expect(wrapper.state("finishName")).toEqual(["teste"])
    })

    it('should not be able to delete a material if there is only one', () => {
        expect(wrapper.instance().handleDeleteMaterialClick(0)).toBeFalsy()
    })

    it('should be able to delete a material if there is more than one', () => {
        wrapper.setState({
            finishName: ['teste', 'teste2'],
            finishNameClass: ['form-control', 'form-control']
        })

        wrapper.instance().handleDeleteMaterialClick(0)

        expect(wrapper.state("finishName")).toEqual(["teste2"])
    })
})