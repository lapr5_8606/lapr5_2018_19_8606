import React, {Component} from 'react'

import Swall from 'sweetalert2'
import Axios from '../../../../../api/ManagerApi'

class UpdateMaterial extends Component {

    state = {
        materialName: '',
        materialNameClass: 'form-control',
        finishName: [],
        finishNameClass: [],
        price: -1,
        finishPrices: [],
        hasSearched: false
    }


    handleMaterialNameChange = (event) => {
        this.setState({materialName: event.target.value, materialNameClass: 'form-control'})
    }

    handleFinishNameChange = (event, index) => {
        let f = [...this.state.finishName]
        let fc = [...this.state.finishNameClass]
        f[index] = event.target.value
        fc[index] = 'form-control'
        this.setState({finishName: f, finishNameClass: fc})
    }

    handleDeleteMaterialClick = (index) => {
        if (this.state.finishName.length === 1) {
            return false
        }

        let f = [...this.state.finishName]
        let fc = [...this.state.finishNameClass]

        f.splice(index, 1)
        fc.splice(index, 1)

        this.setState({finishName: f, finishNameClass: fc})
    }

    handleFinishClick = (event) => {
        event.preventDefault()

        let valid = true
        for (let i = 0; i < this.state.finishName.length; i++) {
            if (this.state.finishName[i].length === 0) {
                valid = false
                let newClass = [...this.state.finishNameClass]
                newClass[i] = newClass[i].concat(' is-invalid')
                this.setState({finishNameClass: newClass})
            }
        }

        if (!valid) {
            return
        }

        let f = [...this.state.finishName]
        f.push('')
        let fc = [...this.state.finishNameClass]
        fc.push('form-control')
        this.setState({finishName: f, finishNameClass: fc})
    }

    handleMaterialPriceChange = event => {
        this.setState({
            price: event.target.value
        });
    }

    handleFinishPriceChangeClick = (event, index) => {
        let prices = [...this.state.finishPrices];
        prices[index].push(event.target.value);
        this.setState({
            finishPrices: prices
        });
    }

    searchMaterial = (event) => {
        event.preventDefault()

        if (this.state.materialName.length === 0) {
            this.setState({materialNameClass: this.state.materialNameClass.concat(" is-invalid")})
            return
        }
        Axios.get("material/names", this.state.materialName, this.props.user.token).then((response) => {
            let f = [...this.state.finishName]
            let fc = [...this.state.finishNameClass]
            let price = response.data.price;
            for (let i = 0; i < response.data.availableFinishes.length; i++) {
                f.push(response.data.availableFinishes[i].name)
                fc.push('form-control')
            }
            this.setState(
                {
                    hasSearched: !this.state.hasSearched,
                    finishName: f,
                    finishNameClass: fc,
                    price: price
                })
        }).catch((error) => {
            Swall({
                type: 'error',
                title: 'Erro',
                text: error.response.data.errorMessage
            })
        })
    }

    changeMaterial = (event) => {
        let valid = true
        let materialNameClass = this.state.materialNameClass
        let finishNameClass = [...this.state.finishNameClass]
        if (this.state.materialName.length === 0) {
            materialNameClass = materialNameClass.concat(" is-invalid")
        }

        let data = {
            "Name": this.state.materialName
        }

        let finishes = []
        for (let i = 0; i < this.state.finishNameClass.length; i++) {

            if (this.state.finishName[i].length === 0) {
                valid = false
                finishNameClass[i] = finishNameClass[i].concat(" is-invalid")
            } else {
                let finish = {
                    Name: this.state.finishName[i],
                    PriceIncrement: this.state.finishPrices[i]
                }
                finishes.push(finish);
            }
        }

        this.setState({
            materialNameClass: materialNameClass,
            finishNameClass: finishNameClass
        })

        if (!valid)
            return;

        data["NewFinishes"] = finishes;
        data["NewName"] = this.state.materialName;
        data["NewPrice"] = this.state.price;

        console.log(data);

        Axios.put("material/names", data, this.props.user.token).then(() => {
            Swall({
                type: 'success',
                title: 'Sucesso',
                text: 'Material alterado com sucesso'
            })
        }).catch((error) => {
            console.log(JSON.stringify(error))
            Swall({
                type: 'error',
                title: 'Erro',
                text: error.response.data.errorMessage
            })
        })
    }
    createFinishComponent = () => {
        return (
            <div className="form-group">
                <label>Acabamentos</label>
                {
                    this.state.finishName.map((finish, index) => {
                        return (
                            <div className="input-group mb-3" key={index}>
                                <input
                                    className={this.state.finishNameClass[index]}
                                    placeholder="Introduzir o nome do acabamento"
                                    value={this.state.finishName[index]}
                                    onChange={(event) => this.handleFinishNameChange(event, index)}/>
                                <input
                                    type="number"
                                    placeholder="Incremento Acabamento"
                                    value={this.state.finishPrices[index]}
                                    onChange={event => this.handleFinishPriceChangeClick(event, index)}
                                />
                                <div className="input-group-append">
                                    <button onClick={(event) => this.handleDeleteMaterialClick(event, index)}
                                            type="button" className="btn btn-danger">Apagar
                                    </button>
                                </div>
                            </div>
                        )
                    })
                }
                <button
                    onClick={this.handleFinishClick}
                    type="button"
                    className="btn btn-success">Adicionar mais acabamentos
                </button>
            </div>
        )
    }

    render() {
        if (!this.state.hasSearched) {
            return (
                <form className="needs-validation">
                    <div className="form-group">
                        <label htmlFor="material">Nome do Material</label>
                        <input
                            type="text"
                            className={this.state.materialNameClass}
                            id="material"
                            placeholder="Introduzir o nome do material"
                            value={this.state.materialName}
                            onChange={this.handleMaterialNameChange}/>
                        <div className="invalid-feedback">
                            Deve preencher o campo
                        </div>
                    </div>
                    <button onClick={this.searchMaterial} type="submit" className="btn btn-primary">Procurar</button>
                </form>
            )
        } else {
            return (
                <form className="needs-validation">
                    <div className="form-group">
                        <label htmlFor="material">Nome do Material</label>
                        <input
                            type="text"
                            className={this.state.materialNameClass}
                            id="material"
                            placeholder="Introduzir o nome do material"
                            value={this.state.materialName}
                            onChange={this.handleMaterialNameChange}/>
                        <div className="invalid-feedback">
                            Deve preencher o campo
                        </div>
                        <label htmlFor="materialPrice">Preço do Material</label>
                        <input
                            type="number"
                            className="form-control"
                            id="materialPrice"
                            placeholder="Preço por m2"
                            value={this.state.pricerice}
                            onChange={this.handleMaterialPriceChange}>
                        </input>
                    </div>
                    {this.createFinishComponent()}
                    <button onClick={this.changeMaterial} type="submit" className="btn btn-success">Alterar</button>
                </form>
            )
        }


    }
}

export default UpdateMaterial