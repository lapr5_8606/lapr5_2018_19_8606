import React, { Component } from "react";
import Swall from "sweetalert2";
import Axios from "../../../../../api/ManagerApi";

class ListAllCatalogs extends Component {
  state = {
    catalogs: []
  };

  componentDidMount() {
    Axios.get("catalog/all", "",this.props.user.token)
      .then(response => {
        this.setState({ catalogs: response.data.catalogs });
        console.log("LISTAR CATALOGOS", response.data.catalogs);
      })
      .catch(error => {
        console.log(JSON.stringify(error));
        Swall({
          type: "error",
          title: "Erro", 
          text: error
        });
      });
  }

  render() {
    return (
      <form>
        <label>Lista De Catálogos</label>
        {this.state.catalogs.map((catalog, i) => {
          return (
            <div className="form-group row" key={i}>
              <label htmlFor="name" className="col-sm-2 col-form-label">
                Nome
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readonly
                  className="form-control"
                  disabled={true}
                  id="name"
                  value={this.state.catalogs[i].catalogName}
                />
              </div>
            </div>
          );
        })}
      </form>
    );
  }
}

export default ListAllCatalogs;
