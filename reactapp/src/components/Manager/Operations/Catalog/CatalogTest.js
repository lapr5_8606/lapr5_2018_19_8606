import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import Catalog from "./Catalog";
import CreateCatalog from "./CreateCatalog/CreateCatalog";
import DeleteCatalog from "./DeleteCatalog/DeleteCatalog";
import SearchCatalog from "./SearchCatalog/SearchCatalog";
import ListAllCatalogs from "./ListAllCatalogs/ListAllCatalogs";

configure({ adapter: new Adapter() });

describe("<Catalog />", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Catalog />);
  });

  it("should display <CreateCatalog /> by default", () => {
    expect(wrapper.find(CreateCatalog)).toHaveLength(1);
  });

  it("Should display <DeleteCatalog /> if state set to delete", () => {
    wrapper.setState({ page: <DeleteCatalog /> });
    expect(wrapper.find(DeleteCatalog)).toHaveLength(1);
  });

  it("Should display <ListAllCatalogs/> if state set to list all", () => {
    wrapper.setState({ page: <ListAllCatalogs /> });
    expect(wrapper.find(ListAllCatalogs)).toHaveLength(1);
  });

  it("Should display <SearchCatalog/> if state set to search", () => {
    wrapper.setState({ page: <SearchCatalog /> });
    expect(wrapper.find(SearchCatalog)).toHaveLength(1);
  });

  it("Should change selected state when ChangeOperation is called", () => {
    wrapper.instance().changeOperation(1);
    expect(wrapper.state("selected")).toEqual(1);
    expect(wrapper.find(DeleteCatalog)).toHaveLength(1);
  });
});
