import React, { Component } from "react";

import Swall from "sweetalert2";
import Axios from "../../../../../api/ManagerApi";

class UpdateCatalog extends Component {
  state = {
    catalogName: "",
    catalogNameClass: "form-control",
    productName: [],
    productNameClass: [],
    hasSearched: false
  };

  handleCatalogNameChange = event => {
    this.setState({
      catalogName: event.target.value,
      catalogNameClass: "form-control"
    });
  };

  handleProductNameChange = (event, index) => {
    let f = [...this.state.productName];
    let fc = [...this.state.productNameClass];
    f[index] = event.target.value;
    fc[index] = "form-control";
    this.setState({ productName: f, productNameClass: fc });
  };

  handleDeleteMaterialClick = index => {
    if (this.state.productName.length === 1) {
      return false;
    }

    let f = [...this.state.productName];
    let fc = [...this.state.productNameClass];

    f.splice(index, 1);
    fc.splice(index, 1);

    this.setState({ productName: f, productNameClass: fc });
  };

  handleProductClick = event => {
    event.preventDefault();

    let valid = true;
    for (let i = 0; i < this.state.productName.length; i++) {
      if (this.state.productName[i].length === 0) {
        valid = false;
        let newClass = [...this.state.productNameClass];
        newClass[i] = newClass[i].concat(" is-invalid");
        this.setState({ productNameClass: newClass });
      }
    }

    if (!valid) {
      return;
    }

    let f = [...this.state.productName];
    f.push("");
    let fc = [...this.state.productNameClass];
    fc.push("form-control");
    this.setState({ productName: f, productNameClass: fc });
  };

  searchMaterial = event => {
    event.preventDefault();

    if (this.state.catalogName.length === 0) {
      this.setState({
        catalogNameClass: this.state.catalogNameClass.concat(" is-invalid")
      });
      return;
    }
    Axios.get("catalog/names", this.state.catalogName, this.props.user.token)
      .then(response => {
        let f = [...this.state.productName];
        let fc = [...this.state.productNameClass];
        for (let i = 0; i < response.data.Products.length; i++) {
          f.push(response.data.Products[i].name);
          fc.push("form-control");
        }
        this.setState({
          hasSearched: !this.state.hasSearched,
          productName: f,
          productNameClass: fc
        });
      })
      .catch(error => {
        Swall({
          type: "error",
          title: "Erro",
          text: error.response.data.errorMessage
        });
      });
  };

  changeCatalog = event => {
    let valid = true;
    let catalogNameClass = this.state.catalogNameClass;
    let productNameClass = [...this.state.productNameClass];
    if (this.state.catalogName.length === 0) {
      catalogNameClass = catalogNameClass.concat(" is-invalid");
    }

    let data = {
      Name: this.state.catalogName
    };

    let products = [];
    for (let i = 0; i < this.state.productNameClass.length; i++) {
      if (this.state.productName[i].length === 0) {
        valid = false;
        productNameClass[i] = productNameClass[i].concat(" is-invalid");
      } else {
        products.push(this.state.productName[i]);
      }
    }

    this.setState({
      catalogNameClass: catalogNameClass,
      productNameClass: productNameClass
    });

    if (!valid) return;

    data["Newproducts"] = products;
    data["NewName"] = this.state.catalogName;

    console.log(data);

    Axios.put("catalog/names", data, this.props.user.token)
      .then(() => {
        Swall({
          type: "success",
          title: "Sucesso",
          text: "Catálogo alterado com sucesso"
        });
      })
      .catch(error => {
        console.log(JSON.stringify(error));
        Swall({
          type: "error",
          title: "Erro",
          text: error.response.data.errorMessage
        });
      });
  };
  createFinishComponent = () => {
    return (
      <div className="form-group">
        <label>Produtos</label>
        {this.state.productName.map((finish, index) => {
          return (
            <div className="input-group mb-3" key={index}>
              <input
                className={this.state.productNameClass[index]}
                placeholder="Introduzir o nome do acabamento"
                value={this.state.productName[index]}
                onChange={event => this.handleproductNameChange(event, index)}
              />
              <div className="input-group-append">
                <button
                  onClick={event =>
                    this.handleDeleteMaterialClick(event, index)
                  }
                  type="button"
                  className="btn btn-danger"
                >
                  Apagar
                </button>
              </div>
            </div>
          );
        })}
        <button
          onClick={this.handleProductClick}
          type="button"
          className="btn btn-success"
        >
          Adicionar mais produtos
        </button>
      </div>
    );
  };

  render() {
    if (!this.state.hasSearched) {
      return (
        <form className="needs-validation">
          <div className="form-group">
            <label htmlFor="material">Nome do Catálogo</label>
            <input
              type="text"
              className={this.state.catalogNameClass}
              id="material"
              placeholder="Introduzir o nome do catálogo"
              value={this.state.catalogName}
              onChange={this.handlecatalogNameChange}
            />
            <div className="invalid-feedback">Deve preencher o campo</div>
          </div>
          <button
            onClick={this.searchMaterial}
            type="submit"
            className="btn btn-primary"
          >
            Procurar
          </button>
        </form>
      );
    } else {
      return (
        <form className="needs-validation">
          <div className="form-group">
            <label htmlFor="material">Nome do Catálogo</label>
            <input
              type="text"
              className={this.state.catalogNameClass}
              id="material"
              placeholder="Introduzir o nome do catálogo"
              value={this.state.catalogName}
              onChange={this.handlecatalogNameChange}
            />
            <div className="invalid-feedback">Deve preencher o campo</div>
          </div>
          {this.createFinishComponent()}
          <button
            onClick={this.changeCatalog}
            type="submit"
            className="btn btn-success"
          >
            Alterar
          </button>
        </form>
      );
    }
  }
}

export default UpdateCatalog;
