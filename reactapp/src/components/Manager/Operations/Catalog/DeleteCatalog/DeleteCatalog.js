import React, { Component } from "react";
import Swall from "sweetalert2";
import ManagerAPI from "../../../../../api/ManagerApi";
import FilteredMultiSelect from "react-filtered-multiselect";

const FILTEREDMULTISELECT_CLASSES = {
  filter: "form-control",
  select: "form-control",
  button: "btn btn btn-block btn-default",
  buttonActive: "btn btn-danger"
};

class DeleteCatalog extends Component {
  state = {
    catalogName: "",
    catalogNameClass: "form-control",
    catalogs: [],
    selectedCatalogs: []
  };

  handleFirstListSelectionChange = selectedCatalogs => {
    selectedCatalogs.sort((a, b) => a.id - b.id);

    let valid = true;

    this.state.catalogName = selectedCatalogs[0].name;

    if (this.state.catalogName.length === 0) {
      valid = false;
      this.setState({
        catalogNameClass: this.state.catalogNameClass.concat(" is-invalid")
      });
    }

    if (!valid) {
      return;
    }

    ManagerAPI.delete(
      "catalog/names",
      this.state.catalogName,
      this.props.user.token
    )
      .then(() => {
        let catalogs = [];
        let x = 0;

        for (var i = 0; i < this.state.catalogs.length; i++) {
          if (this.state.catalogs[i].name === this.state.catalogName) {
          } else {
            catalogs[x] = this.state.catalogs[i];
            x++;
          }
        }

        this.setState({ catalogs });

        Swall({
          type: "success",
          title: "Sucesso",
          text: "Catálogo apagado com sucesso"
        });
      })
      .catch(error => {
        Swall({
          type: "error",
          title: "Erro",
          text: error.response.data.errorMessage
        });
      });
  };

  componentDidMount() {
    ManagerAPI.get("catalog/all", "", this.props.user.token)
      .then(response => {
        let optionsNew = [];

        let i;
        for (i = 0; i < response.data.catalogs.length; i++) {
          optionsNew[i] = {
            id: response.data.catalogs[i].catalogID,
            name: response.data.catalogs[i].catalogName
          };
        }

        this.setState({
          catalogs: optionsNew
        });

        console.log(response.data);
        console.log(response.data.catalogs);
      })
      .catch(erro => {});
  }

  render() {
    return (
      <div className="form-group">
        <label htmlFor="catalog">Nome do catálogo</label>
        <div className="col-md-5">
          <FilteredMultiSelect
            buttonText="Delete"
            classNames={FILTEREDMULTISELECT_CLASSES}
            onChange={this.handleFirstListSelectionChange}
            options={this.state.catalogs}
            selectedOptions={this.state.selectedCatalogs}
            textProp="name"
            valueProp="id"
          />
        </div>
      </div>
    );
  }
}

export default DeleteCatalog;
