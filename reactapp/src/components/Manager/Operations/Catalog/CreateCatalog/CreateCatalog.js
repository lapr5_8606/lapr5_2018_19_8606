import React, { Component } from "react";
import Swall from "sweetalert2";
import ManagerAPI from "../../../../../api/ManagerApi";
import FilteredMultiSelect from "react-filtered-multiselect";

const FILTEREDMULTISELECT_CLASSES = {
  filter: "form-control",
  select: "form-control",
  button: "btn btn btn-block btn-default",
  buttonActive: "btn btn btn-block btn-primary"
};

class CreateCatalog extends Component {
  state = {
    catalogName: "",
    catalogNameClass: "form-control",
    productName: [""],
    productNameList: [],
    selectedProductsNames: []
  };

  componentDidMount() {
    ManagerAPI.get("product/all", "", this.props.user.token)
      .then(response => {
        let optionsNew = [];

        let i;
        for (i = 0; i < response.data.produtos.length; i++) {
          optionsNew[i] = {
            id: response.data.produtos[i].id,
            name: response.data.produtos[i].name
          };
        }

        this.setState({
          productNameList: optionsNew
        });

        // console.log(optionsNew);
        //console.log(response.data);
        //console.log(response.data.produtos[0].id);
        //console.log(response.data.produtos[0].name);
      })
      .catch(erro => {});
  }

  handleCatalogNameChange = event => {
    this.setState({
      catalogName: event.target.value,
      catalogNameClass: "form-control"
    });
  };

  handleSecondListSelectionChange = deselectedOptions => {
    var selectedProductsNames = this.state.selectedProductsNames.slice();
    deselectedOptions.forEach(option => {
      selectedProductsNames.splice(selectedProductsNames.indexOf(option), 1);
    });
    this.setState({ selectedProductsNames });
  };

  handleFirstListSelectionChange = selectedProductsNames => {
    selectedProductsNames.sort((a, b) => a.id - b.id);
    this.setState({ selectedProductsNames });
  };

  submitProduct = event => {
    event.preventDefault();
    let valid = true;
    let catalogNameClass = this.state.catalogNameClass;
    if (this.state.catalogName.length === 0) {
      catalogNameClass = catalogNameClass.concat(" is-invalid");
    }

    let products = [];

    console.log(this.state.selectedProductsNames);
    console.log("Catalog", catalogNameClass);

    this.setState({
      catalogNameClass: catalogNameClass
    });

    for (let i = 0; i < this.state.selectedProductsNames.length; i++) {
      products[i] = this.state.selectedProductsNames[i].id;
    }

    console.log("Produtos", products);
    console.log("NOME", this.state.catalogName);

    if (!valid) return;

    let data = {
      CatalogName: this.state.catalogName,
      ProductIDs: products
    };
    console.log("DATA : ", data);

    ManagerAPI.post("catalog", data, this.props.user.token)
      .then(() => {
        Swall({
          type: "success",
          title: "Sucesso",
          text: "Catálogo criado com sucesso"
        });
      })
      .catch(error => {
        Swall({
          type: "error",
          title: "Erro",
          text: error.response.data
        });
      });
  };

  render() {
    return (
      <form className="needs-validation">
        <div className="form-group">
          <label htmlFor="catalogo">Nome do Catálogo</label>
          <input
            type="text"
            className={this.state.catalogNameClass}
            id="catalogo"
            placeholder="Introduzir o nome do catálogo"
            value={this.state.catalogName}
            onChange={this.handleCatalogNameChange}
          />
          <div className="invalid-feedback">Deve preencher o campo</div>
          <label htmlFor="catalogo">
            {" "}
            Selecione os produtos para o catálogo
          </label>
          <div className="row">
            <div className="col-md-5">
              <FilteredMultiSelect
                buttonText="Select"
                classNames={FILTEREDMULTISELECT_CLASSES}
                onChange={this.handleFirstListSelectionChange}
                options={this.state.productNameList}
                selectedOptions={this.state.selectedProductsNames}
                textProp="name"
                valueProp="id"
              />
            </div>
            <div className="col-md-5">
              <FilteredMultiSelect
                buttonText="Remove"
                classNames={{
                  filter: "form-control",
                  select: "form-control",
                  button: "btn btn btn-block btn-default",
                  buttonActive: "btn btn btn-block btn-danger"
                }}
                onChange={this.handleSecondListSelectionChange}
                options={this.state.selectedProductsNames}
                textProp="name"
                valueProp="id"
              />
            </div>
          </div>
        </div>
        <button
          onClick={this.submitProduct}
          type="submit"
          className="btn btn-primary"
        >
          Criar
        </button>
      </form>
    );
  }
}

export default CreateCatalog;
