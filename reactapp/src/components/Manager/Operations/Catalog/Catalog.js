import React, { Component } from "react";

import Wrapper from "../../../../hoc/Wrapper";

import CreateCatalog from "./CreateCatalog/CreateCatalog";
import DeleteCatalog from "./DeleteCatalog/DeleteCatalog";
import SearchCatalog from "./SearchCatalog/SearchCatalog";
import ListAllCatalogs from "./ListAllCatalogs/ListAllCatalogs";

class Catalog extends Component {
  state = {
    operations: [
      "Criar",
      "Apagar",
      //"Alterar",
      "Procurar",
      "Listar todos"
    ],
    selected: 0,
    pages: [
      <CreateCatalog user={this.props.user} />,
      <DeleteCatalog user={this.props.user}/>,
      //<UpdateCatalog />,
      <SearchCatalog user={this.props.user}/>,
      <ListAllCatalogs user={this.props.user}/>
    ],
    page: <CreateCatalog user={this.props.user}/>
  };

  changeOperation = index => {
    this.setState({ selected: index, page: this.state.pages[index] });
  };

  render() {
    return (
      <Wrapper>
        <ul className="nav nav-tabs justify-content-center">
          {this.state.operations.map((operation, i) => {
            return (
              <li className="nav-item" key={i}>
                {/* eslint-disable-next-line */}
                <a
                  onClick={() => this.changeOperation(i)}
                  className={
                    "nav-link" + (i === this.state.selected ? " active" : "")
                  }
                  href="#"
                >
                  {operation}
                </a>
              </li>
            );
          })}
        </ul>
        <div className="subContainer">{this.state.page}</div>
      </Wrapper>
    );
  }
}

export default Catalog;
