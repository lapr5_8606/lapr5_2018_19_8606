import React, { Component } from "react";
import Swall from "sweetalert2";

import ManagerApi from "../../../../../api/ManagerApi";

import Wrapper from "../../../../../hoc/Wrapper";

class SearchCatalog extends Component {
  state = {
    catalogName: "",
    catalogNameClass: "form-control",
    catalogInfo: ""
  };

  handleCatalogNameChange = event => {
    this.setState({ catalogName: event.target.value });
    console.log(this.state.catalogName);
  };

  createProdutoComponent = catalogs => {
    return (
      <div>
        <div className="form-group row">
          <label className="col-sm-3 col-form-label">Produtos:</label>
        </div>
        {catalogs.map((produto, i) => {
          console.log(produto);
          return (
            <div className="form-group row" key={i}>
              <label htmlFor="name" className="col-sm-3 col-form-label">
                Nome
              </label>
              <div className="col-sm-9">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="name"
                  value={produto}
                />
              </div>
            </div>
          );
        })}
      </div>
    );
  };

  handleSearchClick = event => {
    event.preventDefault();

    if (this.state.catalogName.length === 0) {
      this.setState({
        catalogNameClass: this.state.catalogNameClass.concat(" is-invalid")
      });
      return;
    }

    ManagerApi.get(
      "catalog",
      "withnames/" + this.state.catalogName,
      this.props.user.token
    )
      .then(response => {
        console.log("CATALOG ID", response.data.catalogID);
        console.log("NOME CATALOG", response.data.catalogName);
        let catalogInfor = (
          <form>
            <div className="form-group row">
              <label htmlFor="id" className="col-sm-3 col-form-label">
                Id
              </label>
              <div className="col-sm-9">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="id"
                  value={response.data.catalogID}
                />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-3 col-form-label">
                Nome
              </label>
              <div className="col-sm-9">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="name"
                  value={response.data.catalogName}
                />
              </div>
            </div>
            {this.createProdutoComponent(response.data.productNames)}
          </form>
        );

        this.setState({ catalogInfo: catalogInfor });
      })
      .catch(error => {
        Swall({
          type: "error",
          title: "Erro",
          text: error.response.data.errorMessage
        });
      });
  };

  render() {
    return (
      <Wrapper>
        {this.state.catalogInfo}

        <form className="needs-validation">
          <div className="form-group">
            <label htmlFor="catalog">Catálogo a procurar</label>
            <input
              placeholder="Introduzir o nome do catálogo"
              type="text"
              id="catalog"
              className={this.state.catalogNameClass}
              value={this.state.catalogName}
              onChange={this.handleCatalogNameChange}
            />
            <div className="invalid-feedback">Deve preencher o campo</div>
          </div>
          <button
            onClick={this.handleSearchClick}
            type="submit"
            className="btn btn-success"
          >
            Procurar
          </button>
        </form>
      </Wrapper>
    );
  }
}

export default SearchCatalog;
