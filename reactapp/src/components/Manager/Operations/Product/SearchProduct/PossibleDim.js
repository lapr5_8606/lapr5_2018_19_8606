import React, { Component } from 'react'

class PossibleDim extends Component {
    render() {
        if (!this.props.info.isDiscrete) {
            return (
                <div>
                    <div className="form-group row">
                        <label className="col-sm-3 col-form-label">Dimensão continua</label>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="minValue" className="col-sm-3 col-form-label">Valor Minimo</label>
                        <div className="col-sm-9">
                            <input type="text" readOnly className="form-control" id="minValue"
                                value={this.props.info.values[0]} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="maxValue" className="col-sm-3 col-form-label">Valor Maximo</label>
                        <div className="col-sm-9">
                            <input type="text" readOnly className="form-control" id="maxValue"
                                value={this.props.info.values[1]} />
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div>
                    <div className="form-group row">
                        <label className="col-sm-3 col-form-label">Dimensão discreta</label>
                    </div>
                    <div className="form-group row">
                        <label  className="col-sm-3 col-form-label">Valores</label>
                        {
                            this.props.info.values.map((value, i) => {
                                return (
                                    <div key={i} className="col-sm-3">
                                        <input type="text" readOnly className="form-control" id="nome" value={value} />
                                    </div>
                                )
                            })
                        }
                    </div>

                </div>)
        }
    }
}

export default PossibleDim