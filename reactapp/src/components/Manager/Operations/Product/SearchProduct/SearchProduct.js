import React, { Component } from "react";
import Swall from "sweetalert2";

import Axios from "../../../../../api/ManagerApi";

import Wrapper from "../../../../../hoc/Wrapper";
import PossibleDim from "./PossibleDim";

class SearchProduct extends Component {
  state = {
    productName: "",
    productNameClass: "form-control",
    productInformation: ""
  };

  handleProductNameChange = event => {
    this.setState({ productName: event.target.value });
  };

  sliceWord = word => {
    let final = word.replace("PossibleValues", "");
    return final;
  };

  createMaterialComponent = materials => {
    return (
      <div>
        <div className="form-group row">
          <label className="col-sm-3 col-form-label">Materiais:</label>
        </div>
        {materials.map((material, i) => {
          return (
            <div className="form-group row" key={i}>
              <label htmlFor="name" className="col-sm-3 col-form-label">
                Nome
              </label>
              <div className="col-sm-9">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="name"
                  value={material}
                />
              </div>
            </div>
          );
        })}
      </div>
    );
  };

  handleSearchClick = event => {
    event.preventDefault();

    if (this.state.productName.length === 0) {
      this.setState({
        productNameClass: this.state.productNameClass.concat(" is-invalid")
      });
      return;
    }

    Axios.get(
      "product",
      "?name=" + this.state.productName
    )
      .then(response => {
        let productInfo = (
          <form>
            <div className="form-group row">
              <label htmlFor="name" className="col-sm-3 col-form-label">
                Nome
              </label>
              <div className="col-sm-9">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="name"
                  value={response.data.name}
                />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="id" className="col-sm-3 col-form-label">
                Id
              </label>
              <div className="col-sm-9">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="id"
                  value={response.data.id}
                />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="price" className="col-sm-3 col-form-label">
                Preço
              </label>
              <div className="col-sm-9">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="price"
                  value={response.data.price}
                />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="category" className="col-sm-3 col-form-label">
                Categoria
              </label>
              <div className="col-sm-9">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="category"
                  value={response.data.category}
                />
              </div>
            </div>

            {this.createMaterialComponent(response.data.materialsAndFinishes)}

            <div className="form-group row">
              <label className="col-sm-3 col-form-label">Largura:</label>
            </div>

            {<PossibleDim info={response.data.widthPossibleValues} />}

            <div className="form-group row">
              <label className="col-sm-3 col-form-label">Comprimento:</label>
            </div>

            {<PossibleDim info={response.data.heightPossibleValues} />}

            <div className="form-group row">
              <label className="col-sm-3 col-form-label">Profunidade:</label>
            </div>

            {<PossibleDim info={response.data.depthPossibleValues} />}
          </form>
        );

        this.setState({ productInformation: productInfo });
      })
      .catch(error => {
        Swall({
          type: "error",
          title: "Erro",
          text: error.response.data.errorMessage
        });
      });
  };

  render() {
    return (
      <Wrapper>
        {this.state.productInformation}

        <form className="needs-validation">
          <div className="form-group">
            <label htmlFor="product">Produto a procurar</label>
            <input
              placeholder="Introduzir o nome do produto"
              type="text"
              id="product"
              className={this.state.productNameClass}
              value={this.state.productName}
              onChange={this.handleProductNameChange}
            />
            <div className="invalid-feedback">Deve preencher o campo</div>
          </div>
          <button
            onClick={this.handleSearchClick}
            type="submit"
            className="btn btn-success"
          >
            Procurar
          </button>
        </form>
      </Wrapper>
    );
  }
}

export default SearchProduct;
