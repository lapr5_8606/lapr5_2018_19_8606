import React, { Component } from 'react'

import Wrapper from '../../../../hoc/Wrapper'
import '../../../Layout/Layout.css'

import NewProduct from './NewProduct/NewProduct'
import DeleteProduct from './DeleteProduct/DeleteProduct'
import SearchProduct from './SearchProduct/SearchProduct'
import ChageProduct from './ChangeProduct/ChangeProduct'
import ListAllProducts from './ListAllProducts/ListAllProducts';

class Product extends Component {

    state = {
        operations: ['Criar', 'Apagar', 'Procurar', 'Alterar', 'Listar todos'],
        selected: 0,
        pages: [<NewProduct user={this.props.user}/>, <DeleteProduct user={this.props.user}/>, <SearchProduct  user={this.props.user}/>, <ChageProduct user={this.props.user}/>, <ListAllProducts  user={this.props.user}/>],
        page: <NewProduct user={this.props.user}/>
    }

    changeOperation = (index) => {
        this.setState({ selected: index, page: this.state.pages[index] })
    }

    render() {
        return (
            <Wrapper>
                <ul className="nav nav-tabs justify-content-center">
                    {
                        this.state.operations.map((operation, i) => {
                            return (
                                <li className="nav-item" key={i}>
                                    { /* eslint-disable-next-line */}
                                    <a
                                        onClick={() => this.changeOperation(i)}
                                        className={"nav-link" + (i === this.state.selected ? " active" : "")}
                                        href="#">
                                        {operation}
                                    </a>
                                </li>
                            )
                        })
                    }
                </ul>

                <div className="subContainer">
                    {this.state.page}
                </div>
            </Wrapper>
        )
    }
}

export default Product