import React, { Component } from "react";
import Swall from "sweetalert2";
import Axios from "../../../../../api/ManagerApi";

class DeleteProduct extends Component {
  state = {
    productName: "",
    productClass: "form-control"
  };

  handleChangeName = event => {
    this.setState({
      productName: event.target.value,
      productClass: "form-control"
    });
  };

  deleteProduct = event => {
    event.preventDefault();

    let valid = true;

    if (this.state.productName.length === 0) {
      valid = false;
      this.setState({
        productClass: this.state.productClass.concat(" is-invalid")
      });
    }

    if (!valid) {
      return;
    }

    Axios.delete("product/names", this.state.productName, this.props.user.token)
      .then(() => {
        Swall({
          type: "success",
          title: "Sucesso",
          text: "Produto apagado com sucesso"
        });
      })
      .catch(error => {
        Swall({
          type: "error",
          title: "Erro",
          text: error.response.data.errorMessage
        });
      });
  };

  render() {
    return (
      <form className="needs-validation">
        <div className="form-group">
          <label htmlFor="product">Nome do produto a apagar</label>
          <input
            type="text"
            id="product"
            className={this.state.productClass}
            value={this.state.productName}
            placeholder="Introduzir o nome do produto"
            onChange={this.handleChangeName}
          />
          <div className="invalid-feedback">Deve preencher o campo</div>
        </div>
        <button
          onClick={this.deleteProduct}
          type="submit"
          className="btn btn-danger"
        >
          Apagar
        </button>
      </form>
    );
  }
}

export default DeleteProduct;
