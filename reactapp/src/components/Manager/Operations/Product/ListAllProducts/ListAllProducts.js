import React, { Component } from "react";
import Swall from "sweetalert2";
import Axios from "../../../../../api/ManagerApi";

class ListAllProducts extends Component {
  state = {
    produtos: []
  };

  componentDidMount() {
    Axios.get("product/all", "", this.props.user.token)
      .then(response => {
        this.setState({ produtos: response.data.produtos });
      })
      .catch(error => {
        console.log(JSON.stringify(error));
        Swall({
          type: "error",
          title: "Erro",
          text: error
        });
      });
  }

  render() {
    return (
      <form>
        <label>Lista De Produtos</label>
        {this.state.produtos.map((produto, i) => {
          return (
            <div className="form-group row" key={i}>
              <label htmlFor="name" className="col-sm-2 col-form-label">
                Nome
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readonly
                  className="form-control"
                  disabled={true}
                  id="name"
                  value={this.state.produtos[i].name}
                />
              </div>
            </div>
          );
        })}
      </form>
    );
  }
}

export default ListAllProducts;
