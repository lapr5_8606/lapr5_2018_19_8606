import React, { Component } from 'react'
import Swall from 'sweetalert2'
import Axios from '../../../../../api/ManagerApi'

class ChangeProduct extends Component {

    state = {
        newProductName: '',
        productName: '',
        price: 0,
        category: '',
        materials: [''],
        materialClasses: ['form-control'],
        productNameClass: 'form-control',
        widthDiscrete: false,
        widthValuesClasses: ['form-control', 'form-control'],
        widthValues: [0, 0],
        heightDiscrete: false,
        heightValues: [0, 0],
        heightValuesClasses: ['form-control', 'form-control'],
        depthDiscrete: false,
        depthValues: [0, 0],
        depthValuesClasses: ['form-control', 'form-control']
    }

    handleNameChange = (event) => {
        this.setState({ productName: event.target.value })
    }

    handlePriceChange = (event) => {
        this.setState({ price: (event.target.validity.valid) ? event.target.value : this.state.price })
    }

    handleCategoryChange = (event) => {
        this.setState({ category: event.target.value })
    }

    handleMaterialChange = (event, index) => {
        let mat = [...this.state.materials]
        let matC = [...this.state.materialClasses]
        mat[index] = event.target.value
        matC[index] = 'form-control'
        this.setState({ materials: mat, materialClasses: matC })
    }

    handleDeleteMaterialClick = (event, index) => {
        event.preventDefault()

        if (this.state.materials.length === 1) {
            return
        }

        let mat = [...this.state.materials]
        let matC = [...this.state.materialClasses]

        mat.splice(index, 1)
        matC.splice(index, 1)

        this.setState({ materials: mat, materialClasses: matC })

    }

    handlenewNameChange = (event) => {
        this.setState({ newProductName: event.target.value })
    }

    createMaterialComponent = () => {
        return (
            <div className="form-group">
                <label>Materiais</label>
                {
                    this.state.materials.map((_, i) => {
                        return (
                            <div className="input-group mb-3" key={i}>
                                <input
                                    type="text"
                                    className={this.state.materialClasses[i]}
                                    placeholder="Introduzir o nome do material"
                                    value={this.state.materials[i]}
                                    onChange={(event) => this.handleMaterialChange(event, i)} />
                                <div className="input-group-append">
                                    <button onClick={(event) => this.handleDeleteMaterialClick(event, i)} type="button" className="btn btn-danger">Apagar</button>
                                </div>
                            </div>
                        )
                    })
                }
                <button
                    onClick={this.handleCategoryClick}
                    type="button"
                    className="btn btn-success">Adicionar mais materiais
                </button>

            </div>
        )
    }

    handleCategoryClick = (event) => {
        event.preventDefault()

        let valid = true
        for (let i = 0; i < this.state.materials.length; i++) {
            if (this.state.materials[i].length === 0) {
                valid = false
                let newClass = [...this.state.materialClasses]
                newClass[i] = newClass[i].concat(' is-invalid')
                this.setState({ materialClasses: newClass })
            }
        }

        if (!valid) {
            return
        }

        let mat = [...this.state.materials]
        mat.push('')
        let matC = [...this.state.materialClasses]
        matC.push('form-control')
        this.setState({ materials: mat, materialClasses: matC })
    }

    handleWidthClick = (event) => {
        event.preventDefault()

        let valid = true
        for (let i = 0; i < this.state.widthValues.length; i++) {
            if (this.state.widthValues[i] === 0 || this.state.widthValues[i].length === 0) {
                valid = false
                let newClass = [...this.state.widthValuesClasses]
                newClass[i] = newClass[i].concat(' is-invalid')
                this.setState({ widthValuesClasses: newClass })
            }
        }

        if (!valid) {
            return
        }

        let w = [...this.state.widthValues]
        w.push(0)
        let wC = [...this.state.widthValuesClasses]
        wC.push('form-control')
        this.setState({ widthValues: w, widthValuesClasses: wC })
    }

    handleHeightClick = (event) => {
        event.preventDefault()

        let valid = true
        for (let i = 0; i < this.state.heightValues.length; i++) {
            if (this.state.heightValues[i] === 0 || this.state.heightValues[i].length === 0) {
                valid = false
                let newClass = [...this.state.heightValuesClasses]
                newClass[i] = newClass[i].concat(' is-invalid')
                this.setState({ heightValuesClasses: newClass })
            }
        }

        if (!valid) {
            return
        }

        let h = [...this.state.heightValues]
        h.push(0)
        let hC = [...this.state.heightValuesClasses]
        hC.push('form-control')
        this.setState({ heightValues: h, heightValuesClasses: hC })
    }

    handleDepthClick = (event) => {
        event.preventDefault()

        let valid = true
        for (let i = 0; i < this.state.depthValues.length; i++) {
            if (this.state.depthValues[i] === 0 || this.state.depthValues[i].length === 0) {
                valid = false
                let newClass = [...this.state.depthValuesClasses]
                newClass[i] = newClass[i].concat(' is-invalid')
                this.setState({ depthValuesClasses: newClass })
            }
        }

        if (!valid) {
            return
        }

        let d = [...this.state.depthValues]
        d.push(0)
        let dC = [...this.state.depthValuesClasses]
        dC.push('form-control')
        this.setState({ depthValues: d, depthValuesClasses: dC })
    }

    handleWidhtTypeChange = () => {
        if (this.state.widthDiscrete) {
            this.setState({ widthDiscrete: false, widthValues: [0, 0], widthValuesClasses: ['form-control', 'form-control'] })
        } else {
            this.setState({ widthDiscrete: true, widthValues: [0], widthValuesClasses: ['form-control'] })
        }
    }

    handleHeightTypeChange = () => {
        if (this.state.heightDiscrete) {
            this.setState({ heightDiscrete: false, heightValues: [0, 0], heightValuesClasses: ['form-control', 'form-control'] })
        } else {
            this.setState({ heightDiscrete: true, heightValues: [0], heightValuesClasses: ['form-control'] })
        }
    }

    handleDepthTypeChange = () => {
        if (this.state.depthDiscrete) {
            this.setState({ depthDiscrete: false, depthValues: [0, 0], depthValuesClasses: ['form-control', 'form-control'] })
        } else {
            this.setState({ depthDiscrete: true, depthValues: [0], depthValuesClasses: ['form-control'] })
        }
    }


    handleWithValuesChange = (event, index) => {
        let w = [...this.state.widthValues]
        let wC = [...this.state.widthValuesClasses]

        w[index] = (event.target.validity.valid) ? event.target.value : this.state.widthValues[index]
        wC[index] = 'form-control'

        this.setState({ widthValues: w, widthValuesClasses: wC })
    }

    handleHeightValuesChange = (event, index) => {
        let h = [...this.state.heightValues]
        let hC = [...this.state.heightValuesClasses]

        h[index] = (event.target.validity.valid) ? event.target.value : this.state.heightValues[index]
        hC[index] = 'form-control'

        this.setState({ heightValues: h, heightValuesClasses: hC })
    }

    handleDepthValuesChange = (event, index) => {
        let d = [...this.state.depthValues]
        let dC = [...this.state.depthValuesClasses]

        d[index] = (event.target.validity.valid) ? event.target.value : this.state.depthValues[index]
        dC[index] = 'form-control'

        this.setState({ depthValues: d, depthValuesClasses: dC })
    }

    handleDeleteWidthClick = (event, index) => {
        event.preventDefault()

        if (this.state.widthValues.length === 1) {
            return
        }

        let w = [...this.state.widthValues]
        let wC = [...this.state.widthValuesClasses]

        w.splice(index, 1)
        wC.splice(index, 1)

        this.setState({ widthValues: w, widthValuesClasses: wC })
    }

    handleDeleteHeightClick = (event, index) => {
        event.preventDefault()

        if (this.state.heightValues.length === 1) {
            return
        }

        let h = [...this.state.heightValues]
        let hC = [...this.state.heightValuesClasses]

        h.splice(index, 1)
        hC.splice(index, 1)

        this.setState({ heightValues: h, heightValuesClasses: hC })
    }

    handleDeleteDepthClick = (event, index) => {
        event.preventDefault()

        if (this.state.depthValues.length === 1) {
            return
        }

        let d = [...this.state.depthValues]
        let dC = [...this.state.depthValuesClasses]

        d.splice(index, 1)
        dC.splice(index, 1)

        this.setState({ depthValues: d, depthValuesClasses: dC })
    }

    createWidthComponent = () => {
        let values = null

        if (this.state.widthDiscrete) {
            values = (
                <div className="form-group">
                    {
                        this.state.widthValues.map((_, i) => {
                            return (
                                <div className="input-group mb-3" key={i}>
                                    <input
                                        type="text"
                                        className={this.state.widthValuesClasses[i]}
                                        pattern="[0-9]*?.?[0-9]*"
                                        placeholder="Introduza a medida"
                                        value={this.state.widthValues[i]}
                                        onChange={(event) => this.handleWithValuesChange(event, i)} />
                                    <div className="input-group-append">
                                        <button onClick={(event) => this.handleDeleteWidthClick(event, i)} type="button" className="btn btn-danger">Apagar</button>
                                    </div>
                                </div>
                            )
                        })
                    }
                    <button
                        onClick={this.handleWidthClick}
                        type="button"
                        className="btn btn-success">Adicionar medida
                </button>
                </div>
            )
        } else {
            values = (
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label htmlFor="minWith">Valor Minimo</label>
                        <input
                            type="text"
                            id="minWidth"
                            pattern="[0-9]*?.?[0-9]*"
                            className={this.state.widthValuesClasses[0]}
                            value={this.state.widthValues[0]}
                            onChange={(event) => this.handleWithValuesChange(event, 0)} />
                        <div className="invalid-feedback">
                            Deve preencher o campo
                        </div>
                    </div>
                    <div className="form-group col-md-6">
                        <label htmlFor="maxWidth">Valor Maximo</label>
                        <input
                            type="text"
                            id="maxWidth"
                            pattern="[0-9]*?.?[0-9]*"
                            className={this.state.widthValuesClasses[1]}
                            value={this.state.widthValues[1]}
                            onChange={(event) => this.handleWithValuesChange(event, 1)} />
                        <div className="invalid-feedback">
                            Deve preencher o campo
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="form-group">
                <label>Largura</label>
                <div className="form-group form-check">
                    <input onChange={this.handleWidhtTypeChange} type="checkbox" className="form-check-input" id="widthDiscrete" />
                    <label className="form-check-label" htmlFor="widthDiscrete">Dimensão discreta</label>
                </div>

                {values}

            </div>
        )
    }

    createHeightComponent = () => {
        let values = null

        if (this.state.heightDiscrete) {
            values = (
                <div className="form-group">
                    {
                        this.state.heightValues.map((_, i) => {
                            return (
                                <div className="input-group mb-3" key={i}>
                                    <input
                                        type="text"
                                        className={this.state.heightValuesClasses[i]}
                                        pattern="[0-9]*?.?[0-9]*"
                                        placeholder="Introduza a medida"
                                        value={this.state.heightValues[i]}
                                        onChange={(event) => this.handleHeightValuesChange(event, i)} />
                                    <div className="input-group-append">
                                        <button onClick={(event) => this.handleDeleteHeightClick(event, i)} type="button" className="btn btn-danger">Apagar</button>
                                    </div>
                                </div>
                            )
                        })
                    }
                    <button
                        onClick={this.handleHeightClick}
                        type="button"
                        className="btn btn-success">Adicionar medida
                </button>
                </div>
            )
        } else {
            values = (
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label htmlFor="minHeight">Valor Minimo</label>
                        <input
                            type="text"
                            id="minHeight"
                            pattern="[0-9]*?.?[0-9]*"
                            className={this.state.heightValuesClasses[0]}
                            value={this.state.heightValues[0]}
                            onChange={(event) => this.handleHeightValuesChange(event, 0)} />
                        <div className="invalid-feedback">
                            Deve preencher o campo
                        </div>
                    </div>
                    <div className="form-group col-md-6">
                        <label htmlFor="maxHeight">Valor Maximo</label>
                        <input
                            type="text"
                            id="maxHeight"
                            pattern="[0-9]*?.?[0-9]*"
                            className={this.state.heightValuesClasses[1]}
                            value={this.state.heightValues[1]}
                            onChange={(event) => this.handleHeightValuesChange(event, 1)} />
                        <div className="invalid-feedback">
                            Deve preencher o campo
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="form-group">
                <label>Altura</label>
                <div className="form-group form-check">
                    <input onChange={this.handleHeightTypeChange} type="checkbox" className="form-check-input" id="heightDiscrete" />
                    <label className="form-check-label" htmlFor="heightDiscrete">Dimensão discreta</label>
                </div>

                {values}

            </div>
        )
    }

    createDepthComponent = () => {
        let values = null

        if (this.state.depthDiscrete) {
            values = (
                <div className="form-group">
                    {
                        this.state.depthValues.map((_, i) => {
                            return (
                                <div className="input-group mb-3" key={i}>
                                    <input
                                        type="text"
                                        className={this.state.depthValuesClasses[i]}
                                        pattern="[0-9]*?.?[0-9]*"
                                        placeholder="Introduza a medida"
                                        value={this.state.depthValues[i]}
                                        onChange={(event) => this.handleDepthValuesChange(event, i)} />
                                    <div className="input-group-append">
                                        <button onClick={(event) => this.handleDeleteDepthClick(event, i)} type="button" className="btn btn-danger">Apagar</button>
                                    </div>
                                </div>
                            )
                        })
                    }
                    <button
                        onClick={this.handleDepthClick}
                        type="button"
                        className="btn btn-success">Adicionar medida
                </button>
                </div>
            )
        } else {
            values = (
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label htmlFor="minDepth">Valor Minimo</label>
                        <input
                            type="text"
                            id="minDepth"
                            pattern="[0-9]*?.?[0-9]*"
                            className={this.state.depthValuesClasses[0]}
                            value={this.state.depthValues[0]}
                            onChange={(event) => this.handleDepthValuesChange(event, 0)} />
                        <div className="invalid-feedback">
                            Deve preencher o campo
                        </div>
                    </div>
                    <div className="form-group col-md-6">
                        <label htmlFor="maxDepth">Valor Maximo</label>
                        <input
                            type="text"
                            id="maxDepth"
                            pattern="[0-9]*?.?[0-9]*"
                            className={this.state.depthValuesClasses[1]}
                            value={this.state.depthValues[1]}
                            onChange={(event) => this.handleDepthValuesChange(event, 1)} />
                        <div className="invalid-feedback">
                            Deve preencher o campo
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="form-group">
                <label>Profundidade</label>
                <div className="form-group form-check">
                    <input onChange={this.handleDepthTypeChange} type="checkbox" className="form-check-input" id="depthDiscrete" />
                    <label className="form-check-label" htmlFor="depthDiscrete">Dimensão discreta</label>
                </div>

                {values}

            </div>
        )
    }

    handleProductChange = (event) => {
        event.preventDefault()

        if (this.state.productName.length === 0) {
            this.setState({ productNameClass: this.state.productNameClass.concat(' is-invalid') })
            return
        }

        let data = {
            "Product": this.state.productName
        }

        if (this.state.newProductName.length !== 0) {
            data["Name"] = this.state.newProductName
        }

        if (this.state.category.length !== 0) {

            data["Category"] = this.state.Category
        }

        if (this.state.price !== 0) {
            data["Price"] = this.state.price
        }

        if (this.state.materials.length !== 0) {
            let flag = true
            for (let i = 0; i < this.state.materials.length; i++) {
                if (this.state.materials[i].length === 0) {
                    flag = false
                    break
                }
            }
            if (flag)
                data["MaterialsAndFinishes"] = [...this.state.materials]
        }

        if (this.state.widthValues.length !== 0) {
            let flag = true

            for (let i = 0; i < this.state.widthValues.length; i++) {
                if (this.state.widthValues[i] === 0) {
                    flag = false
                    break
                }
            }
            if (flag) {
                let width = {
                    "IsDiscrete": this.state.widthDiscrete,
                    "Values": this.state.widthValues
                }

                data["NewWidthDimensions"] = width
            }
        }

        if (this.state.heightValues.length !== 0) {
            let flag = true

            for (let i = 0; i < this.state.heightValues.length; i++) {
                if (this.state.heightValues[i] === 0) {
                    flag = false
                    break
                }
            }
            if (flag) {
                let height = {
                    "IsDiscrete": this.state.heightDiscrete,
                    "Values": this.state.heightValues
                }

                data["NewHeightDimensions"] = height
            }
        }

        if (this.state.depthValues.length !== 0) {
            let flag = true

            for (let i = 0; i < this.state.depthValues.length; i++) {
                if (this.state.depthValues[i] === 0) {
                    flag = false
                    break
                }
            }
            if (flag) {
                let depth = {
                    "IsDiscrete": this.state.depthDiscrete,
                    "Values": this.state.depthValues
                }

                data["NewDepthDimensions"] = depth
            }
        }

        Axios.put("product/names", data, this.props.user.token).then((response) => {
            Swall({
                type: 'success',
                title: 'Sucesso',
                text: 'Produto alterada com sucesso'
            })
        }).catch((error) => {

            Swall({
                type: 'error',
                title: 'Erro',
                text: error.response.data.errorMessage
            })

        })
    }

    render() {
        return (
            <form className="needs-validation">
                <div className="form-group">
                    <label htmlFor="productName">Nome do produto a alterar</label>
                    <input
                        type="text"
                        className={this.state.productNameClass}
                        placeholder="Introduzir o nome do produto"
                        value={this.state.productName}
                        onChange={this.handleNameChange}
                        id="productName" />
                    <div className="invalid-feedback">
                        Deve introuzir o nome
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="productName">Novo nome do produto</label>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Introduzir o nome do produto"
                        value={this.state.newProductName}
                        onChange={this.handlenewNameChange}
                        id="productName" />
                </div>
                <div className="needs-validation">
                    <div className="form-group">
                        <label htmlFor="price">Novo Preço</label>
                        <input
                            type="text"
                            pattern="[0-9]*?.?[0-9]*"
                            className="form-control"
                            placeholder="Introduzir o preço do produto"
                            value={this.state.price}
                            id="price"
                            onChange={this.handlePriceChange}
                        />
                    </div>
                </div>
                <div className="needs-validation">
                    <div className="form-group">
                        <label htmlFor="category">Nome da categoria</label>
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Introduzir o nome da cateogria"
                            value={this.state.category}
                            onChange={this.handleCategoryChange}
                            id="category" />
                    </div>
                </div>

                {this.createMaterialComponent()}

                {this.createWidthComponent()}

                {this.createHeightComponent()}

                {this.createDepthComponent()}

                <div className="form-group">
                    <button
                        type="submit"
                        onClick={this.handleProductChange}
                        className="btn btn-success">Alterar
                    </button>
                </div>
            </form>
        )
    }
}

export default ChangeProduct