import React, { Component } from "react";

import "./Manager.css";

import "./Operations/Category/Category";
import Category from "./Operations/Category/Category";

import Product from "./Operations/Product/Product";
import AssociateProduct from "./Operations/AssociateProduct/AssociateProduct";
import Material from "./Operations/Material/Material";
import Catalog from "./Operations/Catalog/Catalog";
import Collection from "./Operations/Collection/Collection";

class Manager extends Component {
  state = {
    operations: [
      "Categorias",
      "Produto",
      "Associar Produtos",
      "Materiais",
      "Collections",
      "Catálogos"
    ],
    selected: 0,
    pages: [
      <Category user={this.props.user}/>,
      <Product  user={this.props.user}/>,
      <AssociateProduct user={this.props.user}/>,
      <Material user={this.props.user}/>,
      <Collection user={this.props.user}/>,
      <Catalog user={this.props.user} />
    ],
    page: <Category user={this.props.user}/>
  };

  changeOperation = index => {
    this.setState({ selected: index, page: this.state.pages[index] });
  };

  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-3 no-float">
            <ul className="list-group">
              {this.state.operations.map((operation, i) => {
                return (
                  <li
                    onClick={() => this.changeOperation(i)}
                    className={
                      "li-pointer list-group-item" +
                      (i === this.state.selected ? " active" : "")
                    }
                    key={i}
                  >
                    {operation}
                  </li>
                );
              })}
            </ul>
          </div>
          <div className="col-md-9 no-float">
            <div className="operation border rounded">{this.state.page}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Manager;
