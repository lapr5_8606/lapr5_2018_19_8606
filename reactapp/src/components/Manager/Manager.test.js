import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import Manager from "./Manager";
import Category from "./Operations/Category/Category";
import Product from "./Operations/Product/Product";
import Catalog from "./Operations/Catalog/Catalog";

configure({ adapter: new Adapter() });

describe("<Manager />", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Manager />);
  });

  it("should display <Category /> by default", () => {
    expect(wrapper.find(Category)).toHaveLength(1);
  });

  it("Should display <Product /> if state set to product", () => {
    wrapper.setState({ page: <Product /> });
    expect(wrapper.find(Product)).toHaveLength(1);
  });

  it("Should display <Catalog /> if state set to catalog", () => {
    wrapper.setState({ page: <Catalog /> });
    expect(wrapper.find(Catalog)).toHaveLength(1);
  });

  it("Should change selected state when ChangeOperation is called", () => {
    wrapper.instance().changeOperation(1);
    expect(wrapper.state("selected")).toEqual(1);
    expect(wrapper.find(Product)).toHaveLength(1);
  });
});
