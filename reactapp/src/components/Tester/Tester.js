import React, { Component } from 'react'
import GestaoRotas from './Operations/GestaoRotas/GestaoRotas'
import TodasEncomendas from './Operations/TodasEncomendas/TodasEncomendas'
class Tester extends Component {

    
    state = {
        operations: ['Encomendas','Gestão de Rotas'],
        selected: 0,
        pages: [<TodasEncomendas  user={this.props.user}/>,<GestaoRotas user={this.props.user}/>],
        page: <TodasEncomendas user={this.props.user} />
    }

    changeOperation = (index) => {
        this.setState({ selected: index, page: this.state.pages[index] })
    }
    
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-3 no-float">
                        <ul className="list-group">
                            {
                                this.state.operations.map((operation, i) => {
                                    return (
                                        <li
                                            onClick={() => this.changeOperation(i)}
                                            className={"li-pointer list-group-item" +
                                                (i === this.state.selected ? " active" : "")}
                                            key={i}>
                                            {operation}
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                    <div className="col-md-9 no-float">
                        <div className="operation border rounded">
                            {this.state.page}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Tester