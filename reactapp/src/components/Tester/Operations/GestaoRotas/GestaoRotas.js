import React, { Component } from 'react'
import Swall from 'sweetalert2'
//Should not be name ClientApi but OrdersApi
import ClientApi from '../../../../api/ClientApi'

class GestaoRotas extends Component {

    state = {
        allPathsWithDistances: []
    }


    componentDidMount() {

        ClientApi.get("order/getBestDeliveryPath", "", this.props.user.token).then((response) => {
            this.setState({ allPathsWithDistances: response.data })
        }).catch((error) => {
            console.log(JSON.stringify(error))
            Swall({
                type: 'error',
                title: 'Erro',
                text: error.response.data
            })
        })
    }

    render() {
        return (
            <form>
                <label>Rotas recomendadas:</label>
                {
                    
                    this.state.allPathsWithDistances.map((pathWithDistance, i) => {
                        return (
                            <div className="form-group row" key={i}>
                            <hr></hr>
                                <label htmlFor="name" className="col-sm-12 col-form-label"><b>Fábrica:</b></label>
                                
                                <div className="col-sm-3">
                                    <input type="text" readonly className="form-control" disabled={true} id="name"
                                        value={pathWithDistance.origem.replace(/\b\w/g, function(l){ return l.toUpperCase() })} />
                                </div>
                                <label htmlFor="name" className="col-sm-12 col-form-label">Rota:</label>
                                <div className="col-sm-12">
                                    <input type="text" readonly className="form-control" disabled={true} id="name"
                                        value={pathWithDistance.shortestPath.toString().replace(/,/g,' - ').replace(/([A-Z])/g, ' $1').trim().replace(/\b\w/g, function(l){ return l.toUpperCase() })} />
                                </div>
                                <label htmlFor="name" className="col-sm-12 col-form-label">Distância:</label>
                                <div className="col-sm-3">
                                    <input type="text" readonly className="form-control" disabled={true} id="name"
                                        value={pathWithDistance.distance+' km'} />
                                        <hr></hr>
                                </div>
                                
                            </div>
                            
                        )
                    })
                }
            </form>
        )
    }
}

export default GestaoRotas