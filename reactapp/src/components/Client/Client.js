import React, { Component } from 'react'

import Order from './Operations/Order/Order'

class Client extends Component {

    
    state = {
        operations: ['Encomenda'],
        selected: 0,
        pages: [<Order user={this.props.user}/>],
        page: <Order user={this.props.user} />
    }

    changeOperation = (index) => {
        this.setState({ selected: index, page: this.state.pages[index] })
    }
    
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-3 no-float">
                        <ul className="list-group">
                            {
                                this.state.operations.map((operation, i) => {
                                    return (
                                        <li
                                            onClick={() => this.changeOperation(i)}
                                            className={"li-pointer list-group-item" +
                                                (i === this.state.selected ? " active" : "")}
                                            key={i}>
                                            {operation}
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                    <div className="col-md-9 no-float">
                        <div className="operation border rounded">
                            {this.state.page}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Client