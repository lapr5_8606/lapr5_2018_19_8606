import React, { Component } from 'react'

import CreateOrder from './CreateOrder/CreateOrder'
import ListAllOrders from './ListAllOrders/ListAllOrders'
import Wrapper from '../../../../hoc/Wrapper'

class Order extends Component {

    state = {
        operations: ['Criar','Listar Encomendas'],
        selected: 0,
        pages: [<CreateOrder user={this.props.user}/>,<ListAllOrders user={this.props.user}/>],
        page: <CreateOrder user={this.props.user} />
    }

    changeOperation = (index) => {
        this.setState({ selected: index, page: this.state.pages[index] })
    }

    render () {
        return (
            <Wrapper>
                <ul className="nav nav-tabs justify-content-center">
                    {
                        this.state.operations.map((operation, i) => {
                            return (                              
                                <li className="nav-item" key={i}>
                                    { /* eslint-disable-next-line */}
                                    <a
                                        onClick={() => this.changeOperation(i)}
                                        className={"nav-link" + (i === this.state.selected ? " active" : "")}
                                        href="#">
                                        {operation}
                                    </a>
                                </li>
                            )
                        })
                    }
                </ul>
                <div className="subContainer">
                    {this.state.page}
                </div>
            </Wrapper>
        )
    }
}

export default Order