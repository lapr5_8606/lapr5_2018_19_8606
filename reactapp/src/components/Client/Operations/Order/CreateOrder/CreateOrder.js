import React, { Component } from 'react'
import Swall from 'sweetalert2'
import ClientApi from '../../../../../api/ClientApi'
import SgraiApi from '../../../../../api/SgraiApi';
import ManagerApi from '../../../../../api/ManagerApi'
import config from '../../../../../config/api'

class CreateOrder extends Component {

    state = {
        items: [
            {
                needsToShow: true,
                parentName: '',
                name: '',
                nameClassName: 'form-control',
                material: '',
                materialClassName: 'form-control',
                width: '',
                widthClassName: 'form-control',
                height: '',
                heightClassName: 'form-control',
                depth: '',
                depthClassName: 'form-control',
                category: ''
                
            }
        ]
    }

    handleItemNameChange = (event, index) => {

        let item = [...this.state.items]
        item[index].name = event.target.value
        item[index].nameClassName = 'form-control'

        // Andre adicionar categoria ao json

        ManagerApi.get("product", "?name=" + item[index].name).then((response) => {

            item[index].category = response.data.category

        }).catch((error) => {
            console.log("erro verificar categoria")
        })

        this.setState({ items: item })
    }

    handleMaterialNameChange = (event, index) => {
        let item = [...this.state.items]
        item[index].material = event.target.value
        item[index].materialClassName = 'form-control'
        this.setState({ items: item })
    }

    handleWidthChange = (event, index) => {
        let item = [...this.state.items]
        item[index].width = (event.target.validity.valid) ? event.target.value : this.state.items[index].width
        item[index].widthClassName = 'form-control'
        this.setState({ items: item })
    }

    handleHeightChange = (event, index) => {
        let item = [...this.state.items]
        item[index].height = (event.target.validity.valid) ? event.target.value : this.state.items[index].height
        item[index].heightClassName = 'form-control'
        this.setState({ items: item })
    }

    handleDepthChange = (event, index) => {
        let item = [...this.state.items]
        item[index].depth = (event.target.validity.valid) ? event.target.value : this.state.items[index].depth
        item[index].depthClassName = 'form-control'
        this.setState({ items: item })
    }

    handleShowitem = (event, index) => {
        event.preventDefault()
        if (this.state.items[index].name.length === 0) {
            return
        } else {
            let item = [...this.state.items]
            item[index].needsToShow = !item[index].needsToShow
            this.setState({ items: item })
        }
    }

    createNewItem = (event, parentIndex) => {
        event.preventDefault()

        let items = [...this.state.items]

        if (items[parentIndex].name.length !== 0) {
            let item = {
                needsToShow: true,
                parentName: this.state.items[parentIndex].name,
                name: '',
                nameClassName: 'form-control',
                material: '',
                materialClassName: 'form-control',
                width: '',
                widthClassName: 'form-control',
                height: '',
                heightClassName: 'form-control',
                depth: '',
                depthClassName: 'form-control'
            }
            items.splice(parentIndex + 1, 0, item)
        } else {
            items[parentIndex].nameClassName = this.state.items[parentIndex].nameClassName.concat(" is-invalid")
        }

        this.setState({ items: items })

    }

    deleteItem = (event, index) => {
        event.preventDefault()

        if (this.state.items.length === 1) {
            return
        }

        let items = [...this.state.items]

        let name = items[index].name
        items.splice(index, 1)

        for (let i = 0; i < items.length; i++) {
            if (items[i].parentName === name) {
                items.splice(i, 1)
                i = 0
            }
        }

        this.setState({ items: items })
    }

    displayItem = (index) => {
        if (!this.state.items[index].needsToShow && this.state.items[index].name.length !== 0) {
            return (
                <div className="input-group mb-3" key={index}>
                    <input
                        type="text"
                        className="form-control"
                        value={this.state.items[index].name}
                        disabled={true} />
                    <div className="input-group-append">
                        <button
                            onClick={(event) => this.handleShowitem(event, index)}
                            className="btn btn-outline-primary"
                            type="button">Mostrar</button>
                    </div>
                </div>
            )
        } else {
            let parent = null
            if (this.state.items[index].parentName.length !== 0) {
                parent = (
                    <div className="form-group">
                        <label htmlFor="parent">Nome do produto Pai</label>
                        <input
                            id="parent"
                            type="text"
                            className="form-control"
                            disabled={true}
                            value={this.state.items[index].parentName} />
                    </div>
                )
            }
            return (
                <div class-name="form" key={index}>
                    {parent}
                    <div className="form-group">
                        <label htmlFor="name">Nome do produto</label>
                        <input
                            id="name"
                            placeholder="Introduzir o nome do produto"
                            type="text"
                            className={this.state.items[index].nameClassName}
                            value={this.state.items[index].name}
                            onChange={(event) => this.handleItemNameChange(event, index)} />
                        <div className="invalid-feedback">
                            Deve preencher este campo
                    </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="material">Nome do material</label>
                        <input
                            id="material"
                            type="text"
                            placeholder="Introduzir o nome do material"
                            className={this.state.items[index].materialClassName}
                            value={this.state.items[index].material}
                            onChange={(event) => this.handleMaterialNameChange(event, index)} />
                        <div className="invalid-feedback">
                            Deve preencher este campo
                    </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="width">Largura</label>
                        <input
                            id="width"
                            type="text"
                            placeholder="Introduzir a largura"
                            pattern="[0-9]*?.?[0-9]*"
                            className={this.state.items[index].widthClassName}
                            value={this.state.items[index].width}
                            onChange={(event) => this.handleWidthChange(event, index)} />
                        <div className="invalid-feedback">
                            Deve preencher este campo
                    </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="height">Altura</label>
                        <input
                            id="height"
                            type="text"
                            placeholder="Introduzir a altura"
                            pattern="[0-9]*?.?[0-9]*"
                            className={this.state.items[index].heightClassName}
                            value={this.state.items[index].height}
                            onChange={(event) => this.handleHeightChange(event, index)} />
                        <div className="invalid-feedback">
                            Deve preencher este campo
                    </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="depth">Profundidade</label>
                        <input
                            id="depth"
                            type="text"
                            placeholder="Introduzir a profundidade"
                            pattern="[0-9]*?.?[0-9]*"
                            className={this.state.items[index].depthClassName}
                            value={this.state.items[index].depth}
                            onChange={(event) => this.handleDepthChange(event, index)} />
                        <div className="invalid-feedback">
                            Deve preencher este campo
                    </div>
                    </div>
                    <div className="form-group">
                        <div className="form-row">
                            <button
                                onClick={(event) => this.createNewItem(event, index)}
                                type="button"
                                className="btn btn-success"
                                style={{ marginRight: 8 }}>
                                Adicionar produto filho
                            </button>
                            <button
                                onClick={(event) => this.handleShowitem(event, index)}
                                type="button"
                                className="btn btn-primary"
                                style={{ marginRight: 8 }}>
                                Esconder
                            </button>
                            <button
                                onClick={(event) => this.deleteItem(event, index)}
                                className="btn btn-danger"
                                type="button">
                                Apagar
                            </button>
                        </div>
                    </div>
                </div>
            )
        }
    }
    // Mudanças Andre
    buildItem = (rawData) => {
        return ({
            "productName": rawData.name,
            "material": rawData.material,
            "finish": 1,
            "width": rawData.width,
            "height": rawData.height,
            "depth": rawData.depth,
            "category": rawData.category,
            "children": []
        })


    }

    getItemInOrder = (item, name) => {
        if (item.productName === name) {
            return item
        } else if (item.children.length !== 0) {
            let i, result = null
            for (i = 0; i < item.children.length; i++) {
                result = this.getItemInOrder(item.children[i], name)
            }
            return result
        }
        return null
    }

    addItemToOrder = (order, item) => {
        let name = item.parentName

        let parentItem = this.getItemInOrder(order.item, name)

        parentItem.children.push(this.buildItem(item))
    }

    visualizeOrder3D = (event) => {
        event.preventDefault()

        let valid = true

        //verificacoes
        let stateItems = [...this.state.items]
        for (let i = 0; i < stateItems.length; i++) {
            if (stateItems[i].name.length === 0) {
                stateItems[i].nameClassName = stateItems[i].nameClassName.concat(" is-invalid")
                valid = false
            }
            if (stateItems[i].material.length === 0) {
                stateItems[i].materialClassName = stateItems[i].materialClassName.concat(" is-invalid")
                valid = false
            }
            if (stateItems[i].width.length === 0 || stateItems[i].width === 0) {
                stateItems[i].widthClassName = stateItems[i].widthClassName.concat(" is-invalid")
                valid = false
            }
            if (stateItems[i].height.length === 0 || stateItems[i].height === 0) {
                stateItems[i].heightClassName = stateItems[i].heightClassName.concat(" is-invalid")
                valid = false
            }
            if (stateItems[i].depth.length === 0 || stateItems[i].depth === 0) {
                stateItems[i].depthClassName = stateItems[i].depthClassName.concat(" is-invalid")
                valid = false
            }

            if (!valid) {
                stateItems[i].needsToShow = true
            }
        }

        this.setState({ items: stateItems })

        if (!valid)
            return

        //Build tree
        let data = {
            "item": this.buildItem(stateItems[0])
        }

        for (let i = 1; i < stateItems.length; i++) {
            let item = stateItems[i]

            this.addItemToOrder(data, item)
        }

        SgraiApi.post("order", data).then(() => {
            Swall({
                type: 'success',
                title: 'Sucesso',
                text: 'Encomenda visualizada com sucesso'
            })
            window.open(config.renderApi + "order", '_blank');
        }).catch((error) => {
            console.log(JSON.stringify(error))
            Swall({
                type: 'error',
                title: 'Erro',
                text: error.response.data
            })
        })

    }

    submitOrder = (event) => {
        event.preventDefault()

        let valid = true

        //verificacoes
        let stateItems = [...this.state.items]
        for (let i = 0; i < stateItems.length; i++) {
            if (stateItems[i].name.length === 0) {
                stateItems[i].nameClassName = stateItems[i].nameClassName.concat(" is-invalid")
                valid = false
            }
            if (stateItems[i].material.length === 0) {
                stateItems[i].materialClassName = stateItems[i].materialClassName.concat(" is-invalid")
                valid = false
            }
            if (stateItems[i].width.length === 0 || stateItems[i].width === 0) {
                stateItems[i].widthClassName = stateItems[i].widthClassName.concat(" is-invalid")
                valid = false
            }
            if (stateItems[i].height.length === 0 || stateItems[i].height === 0) {
                stateItems[i].heightClassName = stateItems[i].heightClassName.concat(" is-invalid")
                valid = false
            }
            if (stateItems[i].depth.length === 0 || stateItems[i].depth === 0) {
                stateItems[i].depthClassName = stateItems[i].depthClassName.concat(" is-invalid")
                valid = false
            }

            if (!valid) {
                stateItems[i].needsToShow = true
            }
        }

        this.setState({ items: stateItems })

        if (!valid)
            return

        //Build tree
        let data = {
            "item": this.buildItem(stateItems[0])
        }

        for (let i = 1; i < stateItems.length; i++) {
            let item = stateItems[i]

            this.addItemToOrder(data, item)
        }
        ClientApi.post("order", data, this.props.user.token).then(() => {
            Swall({
                type: 'success',
                title: 'Sucesso',
                text: 'Encomenda criada com sucesso'
            })
        }).catch((error) => {
            console.log(JSON.stringify(error))
            Swall({
                type: 'error',
                title: 'Erro',
                text: error.response.data
            })
        })
    }


    renderOrder = () => {
        return (
            <div>
                {
                    this.state.items.map((item, i) => {
                        return (
                            this.displayItem(i)
                        )
                    })
                }
            </div>
        )
    }

    render() {
        console.log(this.props)
        return (
            <form className="needs-validation">
                <div className="form-group">
                    <label>Lista De Produtos</label>
                </div>
                {this.renderOrder()}
                <div className="form-group">
                    <button
                        onClick={this.submitOrder}
                        className="btn btn-primary">
                        Submeter
                    </button>
                    <button
                        onClick={this.visualizeOrder3D}
                        className="btn btn-primary">
                        Visualizar Encomenda 3D
                    </button>
                </div>
            </form>
        )
    }
}

export default CreateOrder