import React, { Component } from 'react'
import Swall from 'sweetalert2'
import ClientApi from '../../../../../api/ClientApi'

class ListAllOrders extends Component {

    state = {
        orders: []
    }


    componentDidMount() {
        ClientApi.get("order/", "",this.props.user.token).then((response) => {
            this.setState({ orders: response.data })
        }).catch((error) => {
            console.log(JSON.stringify(error))
            if(error.response.status === 404){
                Swall('Não tem qualquer encomenda');
            }else{
                Swall({
                    type: 'error',
                    title: 'Erro',
                    text: error
                })
            }
            
        })
    } 

    render() {
        return (
            <form>
                <label>Lista De Encomendas</label>
                {
                    this.state.orders.map((order, i) => {
                        return (
                            <div className="form-group row" key={i}>
                                <label htmlFor="name" className="col-sm-2 col-form-label">Id de encomenda:</label>
                                <div className="col-sm-10">
                                    <input type="text" readOnly className="form-control" disabled={true} id="name"
                                        value={order.OrderId} />
                                </div>
                                <label htmlFor="name" className="col-sm-2 col-form-label">Estado atual:</label>
                                <div className="col-sm-10">
                                    <input type="text" readOnly className="form-control" disabled={true} id="estado"
                                        value={order.status} />
                                </div>
                                <label htmlFor="name" className="col-sm-2 col-form-label">Id de cliente:</label>
                                <div className="col-sm-10">
                                    <input type="text" readOnly className="form-control" disabled={true} id="estado"
                                        value={order.clientId} />
                                </div>
                                <label htmlFor="name" className="col-sm-2 col-form-label">Local de entrega:</label>
                                <div className="col-sm-10">
                                    <input type="text" readOnly className="form-control" disabled={true} id="estado"
                                        value={order.placeOfDelivery} />
                                </div>
                                <label htmlFor="name" className="col-sm-2 col-form-label">Local de fabrico:</label>
                                <div className="col-sm-10">
                                    <input type="text" readOnly className="form-control" disabled={true} id="estado"
                                        value={order.placeOfManufacture} />
                                </div>
                                <label htmlFor="name" className="col-sm-2 col-form-label">Data de criação:</label>
                                <div className="col-sm-10">
                                    <input type="text" readOnly className="form-control" disabled={true} id="estado"
                                        value={order.creationDate} />
                                </div>
                            </div>
                        )
                    })
                }
            </form>
        )
    }
}

export default ListAllOrders