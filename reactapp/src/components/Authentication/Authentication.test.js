import React from 'react'
import { configure , shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import Authentication from './Authentication'
import Login from './Operations/Login/Login';
import Register from './Operations/Register/Register';
import TermsAndConditions from './Operations/TermsAndConditions/TermsAndConditions'

configure({adapter: new Adapter()})

describe('<Authentication />', () => {
    let wrapper;

    beforeEach(() => { 
        wrapper = shallow(<Authentication />)
    })

    it('should display <Login /> by default', () => {
        expect(wrapper.find(Login)).toHaveLength(1)   
    })
 
    it('Should display <Register /> if state set to 1', () => {
        wrapper.setState({selected: 1})
        expect(wrapper.find(Register)).toHaveLength(1)
    })

    it('Should display <TermsAndConditions /> if state set to 2', () => {
        wrapper.setState({selected: 2})
        expect(wrapper.find(TermsAndConditions)).toHaveLength(1)
    })

    it('Should change selected state when ChangeOperation is called', () => {
        wrapper.instance().changeOperation(1)
        expect(wrapper.state('selected')).toEqual(1)
        expect(wrapper.find(Register)).toHaveLength(1)
    })
})