import React, { Component } from "react";

import "./Authentication.css";

import Login from './Operations/Login/Login'
import Register from './Operations/Register/Register'
import TermsAndConditions from "./Operations/TermsAndConditions/TermsAndConditions";

class Authentication extends Component {

    state = {
        selected: 0
    }

    changeOperation = (index) => {
        this.setState((state) => {
            state.selected = index;
            return state
        })
    }

    render() {
        var { selected } = this.state

        const operations = ['Login', 'Register','Terms and Conditions'];
        const pages = [<Login registerClick={() => this.changeOperation(1)} />, <Register termsCond={() => this.changeOperation(2)} />,<TermsAndConditions/>];

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-3 no-float">
                        <ul className="list-group">
                            {
                                operations.map((operation, index) => {
                                    return (
                                        <li
                                            onClick={() => this.changeOperation(index)}
                                            className={"li-pointer list-group-item" +
                                                (index === selected ? " active" : "")}
                                            key={index}>
                                            {operation}
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                    <div className="col-md-9 no-float">
                        <div className="operation border rounded">
                            {pages[selected]}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Authentication;
