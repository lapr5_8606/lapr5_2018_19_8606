import React, { Component } from "react";
import AuthApi from "../../../../api/AuthApi";
import "./Register.css";
import Swall from 'sweetalert2';

class Register extends Component {
  state = {
    email: "",
    password: "",
    firstName: "",
    lastName: "",
    city: "",
    zip: "",
    password2: "",
    agreeTerms: false
  };

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  validateZip(zip) {
    var re = /^[0-9]{4}(?:-[0-9]{3})?$/;
    return re.test(String(zip));
  }

  validatePassword(password) {
    var letter = /[a-zA-Z]/;
    var number = /[0-9]/;

    if (
      password.length < 8 ||
      !letter.test(password) ||
      !number.test(password)
    ) {
      return false;
    } else {
      return true;
    }
  }

  // [TODO]
  checkIfCityExists() {
    return true;
  }

  validateInformation() {
    if (this.state.email.length === 0) {
      return;
    }
    if (!this.validateEmail(this.state.email)) {
      return;
    }

    if (this.state.password !== this.state.password2) {
      return;
    }

    if (!this.checkIfCityExists()) {
      return;
    }

    if (this.state.zip.length === 0) {
      return;
    }
    if (!this.validateZip(this.state.zip)) {
      return;
    }

    if (this.state.password.length === 0) {
      return;
    }
    if (!this.validatePassword(this.state.password)) {
      return;
    }

    if (this.state.firstName.length === 0) {
      return;
    }

    if (this.state.lastName.length === 0) {
      return;
    }

    if (this.state.agreeTerms) {
      let userJson = {
        email: this.state.email,
        password: this.state.password,
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        postalCode: this.state.zip
      };

      AuthApi.createUser(userJson);
    } else {
      Swall({
        type: "error",
        title: "Accept terms and conditions!",
        text: "You have to accept our terms and conditions in order to create and account"
      });
    }
  }

  handleChangeFirstName = event => {
    this.setState({
      firstName: event.target.value
    });
  };

  handleChangeLastName = event => {
    this.setState({
      lastName: event.target.value
    });
  };

  handleChangeEmail = event => {
    this.setState({
      email: event.target.value
    });
  };

  handleChangePassword = event => {
    this.setState({
      password: event.target.value
    });
  };

  handleChangePassword2 = event => {
    this.setState({
      password2: event.target.value
    });
  };

  handleChangeAddress = event => {
    this.setState({
      address: event.target.value
    });
  };

  handleChangeCity = event => {
    this.setState({
      city: event.target.value
    });
  };

  handleChangeZip = event => {
    this.setState({
      zip: event.target.value
    });
  };

  handleAgreeTerms = event => {
    this.setState({
      agreeTerms: !this.state.agreeTerms
    });
  };

  render() {
    let firstNameErr = null;
    let lastNameErr = null;
    let zipErr = null;
    let passwordErr = null;
    let emailErr = null;
    let passwordsDontMatch = null;

    if (this.state.zip.length === 0) {
      zipErr = (
        <div className="alert alert-light" role="alert">
          * Mandatory field
        </div>
      );
    } else if (!this.validateZip(this.state.zip)) {
      zipErr = (
        <div className="alert alert-light" role="alert">
          Zip code example 4050-232
        </div>
      );
    } else if (!this.checkIfCityExists()) {
      zipErr = (
        <div className="alert alert-danger" role="alert">
          The field is not a valid zip code!
        </div>
      );
    }

    if (this.state.password.length === 0) {
      passwordErr = (
        <div className="alert alert-light" role="alert">
          * Mandatory field
        </div>
      );
    } else if (!this.validatePassword(this.state.password)) {
      passwordErr = (
        <div className="alert alert-light" role="alert">
          * Password must have at least 8 characters and 1 number!
        </div>
      );
    } else if (this.state.password !== this.state.password2) {
      passwordErr = (
        <div className="alert alert-danger" role="alert">
          * Passwords don't match!
        </div>
      );
    }

    if (this.state.firstName.length === 0) {
      firstNameErr = (
        <div className="alert alert-light" role="alert">
          * Mandatory field
        </div>
      );
    }

    if (this.state.lastName.length === 0) {
      lastNameErr = (
        <div className="alert alert-light" role="alert">
          * Mandatory field
        </div>
      );
    }

    if (this.state.email.length === 0) {
      emailErr = (
        <div className="alert alert-light" role="alert">
          * Mandatory field
        </div>
      );
    } else if (!this.validateEmail(this.state.email)) {
      emailErr = (
        <div className="alert alert-danger" role="alert">
          The field is not a valid email!
        </div>
      );
    }

    return (
      <div className="Login">
        <div className="form-row">
          <div className="form-group col-md-6">
            <label htmlFor="inputEmail4">First Name</label>
            <input
              type="text"
              className="form-control"
              placeholder="First name"
              value={this.state.firstName}
              onChange={this.handleChangeFirstName}
            />
            {firstNameErr}
          </div>
          <div className="form-group col-md-6">
            <label htmlFor="inputPassword4">Last Name</label>
            <input
              type="text"
              className="form-control"
              placeholder="Last name"
              value={this.state.lastName}
              onChange={this.handleChangeLastName}
            />
            {lastNameErr}
          </div>
        </div>

        <div className="form-row">
          <div className="form-group col-md-6">
            <label htmlFor="inputEmail4">Email</label>
            <input
              type="email"
              className="form-control"
              id="inputEmail4"
              value={this.state.email}
              onChange={this.handleChangeEmail}
              placeholder="Email"
            />
            {emailErr}
          </div>
          <div className="form-group col-md-6">
            <label htmlFor="inputZip">Zip</label>
            <input
              type="text"
              className="form-control"
              id="inputZip"
              value={this.state.zip}
              placeholder="Zip Code"
              onChange={this.handleChangeZip}
            />
            {zipErr}
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-6">
            <label htmlFor="inputPassword4">Password</label>
            <input
              type="password"
              className="form-control"
              value={this.state.password}
              onChange={this.handleChangePassword}
              id="inputPassword4"
              placeholder="Password"
            />
            {passwordErr}
          </div>

          <div className="form-group col-md-6">
            <label htmlFor="inputPassword4">Repeat Password</label>
            <input
              type="password"
              className="form-control"
              value={this.state.password2}
              onChange={this.handleChangePassword2}
              id="inputPassword5"
              placeholder="Password"
            />
            {passwordsDontMatch}
          </div>
        </div>

        <div className="form-group">
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              checked={this.state.agreeTerms}
              onChange={this.handleAgreeTerms}
              id="gridCheck"
            />
            <label className="form-check-label" htmlFor="gridCheck">
              Agree to{" "}
              <a href="#" onClick={this.props.termsCond}>
                Terms and Conditions
              </a>
            </label>
          </div>
        </div>
        <button
          type="submit"
          className="btn btn-primary"
          onClick={() => this.validateInformation()}
        >
          Sign up
        </button>
      </div>
    );
  }
}

export default Register;
