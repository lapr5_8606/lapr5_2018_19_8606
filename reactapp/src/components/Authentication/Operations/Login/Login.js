import React, { Component } from "react";
import { Button } from "react-bootstrap";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Swal from 'sweetalert2';

import "./Login.css";
import AuthApi from "../../../../api/AuthApi";
import UserInfoApi from "../../../../api/UserInfoApi";

class Login extends Component {
  state = {
    email: "",
    password: "",
    open: false,
    loginWarn: true,
    doubleAuthComp: {},
    userJson: {},
    code: "",
    trueCode: ""
  };

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  handleChangeEmail = event => {
    this.setState({
      email: event.target.value
    });
  };

  handleChangeCode = event => {
    this.setState({
      code: event.target.value
    });
  };

  handleChangePassword = event => {
    this.setState({
      password: event.target.value
    });
  };

  validateInformation() {

    if (!this.validateEmail(this.state.email)) {
      return;
    } else {

      this.state.userJson = {
        email: this.state.email,
        password: this.state.password
      };

      UserInfoApi.sendVerificationCode(this.state.email).then(response => {
        this.setState(state => {
          state.trueCode = response.data.code;
          console.log(this.state.trueCode);
          return state;
        })
      });

      

      this.handleClickOpen();
    }
  }

  handleClickOpen = () => { 
    this.setState({ open: true });

  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleCloseCancel = () => {
    this.setState({ open: false });
  };

  handleCloseSubmit = () => {
    this.setState({ open: false });

    //console.log(this.state.code);
    //console.log(this.state.trueCode);

    if (this.state.trueCode == this.state.code) {
      Swal(
        'Verified!',
        'Thank you!',
        'success'
      )
      AuthApi.loginUser(this.state.userJson);
    } else {
      Swal(
        'Wrong Code!',
        'Please re-enter your code!',
        'error'
      )
    }
  };


  render() {
    let err = null;
    let emailErr = null;
    let doubleAuthComp = null;


    if (!this.state.loginWarn) {
      err = (
        <div className="alert alert-danger" role="alert">
          Wrong email/password!
        </div>
      );
    }

    if (!this.validateEmail(this.state.email) && this.state.email.length > 0) {
      emailErr = (
        <div className="alert alert-danger" role="alert">
          The field is not a valid email!
        </div>
      );
    }

    if (this.state.open == true) {
      doubleAuthComp = (
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">2-Factor Authentication</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Verify this is really you, by entering the verification code we sent to your email address below!
          </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Verification Code"
              value={this.state.code}
              onChange={this.handleChangeCode}
              type="email"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCloseCancel} color="primary">
              Cancel
          </Button>
            <Button onClick={this.handleCloseSubmit} color="primary">
              Submit
          </Button>
          </DialogActions>
        </Dialog>)
    }

    return (
      <div className="Login">
        <div className="form-group col-md-4">
          <label htmlFor="exampleInputEmail1">Email address</label>
          <input
            type="email"
            className="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            value={this.state.email}
            onChange={this.handleChangeEmail}
            placeholder="Enter email"
          />
          {emailErr}
          <small id="emailHelp" className="form-text text-muted">
            We'll never share your email with anyone else.
          </small>
        </div>

        <div className="form-group col-md-4">
          <label htmlFor="exampleInputPassword1">Password</label>
          <input
            type="password"
            className="form-control"
            value={this.state.password}
            onChange={this.handleChangePassword}
            id="exampleInputPassword1"
            placeholder="Password"
          />

          <button
            type="submit"
            className="btn btn-light"
            onClick={() => this.validateInformation()}
          >
            Login
          </button>
          {err}
          {doubleAuthComp}
        </div>
        <div className="form-group">
          <a href="#" className="badge badge-light" onClick={this.props.registerClick}>
            Don't have an account? Create one!
          </a>
        </div>
      </div>
    );
  }
}

export default Login;
