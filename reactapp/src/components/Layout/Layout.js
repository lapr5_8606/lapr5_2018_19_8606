import React, { Component } from "react";

import "./Layout.css";

import Wrapper from "../../hoc/Wrapper";
import NavBar from "../Navigation/NavBar";

import Manager from "../Manager/Manager";
import Client from "../Client/Client";
import Tester from "../Tester/Tester";
import Login from "../Authentication/Authentication";


class Layout extends Component {
  constructor(props) {
    super(props);
  }

  render = () => {
    let page = null;
    let { user, onUserLogout } = this.props;

    if (user.token) {
      if (user.roles.indexOf("manager") ===0) {
        page = <Manager user={user} />;
      } else if(user.roles.indexOf("client") === 0){
        page = <Client user={user} />;
      }else if(user.roles.indexOf("tester") === 0){
        page = <Tester user={user}/>;
      }
    } else {
      page = <Login />;
    }

    return (
      <Wrapper>
        <NavBar user={user} logoutClick={onUserLogout} />
        <div className="mainContent">{page}</div>
      </Wrapper>
    );
  };
}

export default Layout;
