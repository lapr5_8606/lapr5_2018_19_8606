import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import App from './App';
import Layout from './components/Layout/Layout'

const AppRouter = () => (
    <Router>
        <div>
            <Route path="/" exact component={App} />
            <Route path="/app" component={null} />
            <Route path="/client" component={App} />
            <Route path="/manager" component={App} />
        </div>
    </Router>
);

export default AppRouter;