import axios from 'axios'
import config from '../config/api'

let instance = axios.create({
    baseURL: config.renderApi,
    headers: {
        'Accept': 'application/json'
    }
})

let SgraiApi = {
    post: (url, data) => {
        return instance.post(url, data)
    }
}

export default SgraiApi    