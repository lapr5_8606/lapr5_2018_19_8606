import axios from 'axios'
import config from '../config/api'

let instance = axios.create({
    baseURL: config.ordersApi,
    headers: {
        'Accept': 'application/json'
    }
})

let ClientApi = {
    post: (url, data , clientToken) => {
        return instance.post(url, data, {headers:{'Authorization':clientToken}});
    },
    get: (url, data , clientToken) => {
        return instance.get(url + data,{headers:{'Authorization':clientToken}})
    },
}

export default ClientApi    