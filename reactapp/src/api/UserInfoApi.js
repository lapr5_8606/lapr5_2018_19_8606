import axios from 'axios'
import config from '../config/api'
class UserInfoApi {

    static instance = axios.create({
        baseURL: config.authorizationApi
    })

    static getUserRoles = (clientToken) => {
        console.log(config)
        return UserInfoApi.instance.get('/auth/roles',
            {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': clientToken
                }
            });
    }
    static getUserInfo = (clientToken) => {
        return UserInfoApi.instance.get('/auth/info',
            {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': clientToken
                }
            });
    }
    static sendVerificationCode = (email) => {
        return UserInfoApi.instance.post('/twofactor', {'email': email}, 
        {
            headers: {
                'Accept': 'application/json'
            }
        });
    }
}

export default UserInfoApi    