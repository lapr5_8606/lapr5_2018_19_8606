import fire from "../config/Fire";
import Swall from 'sweetalert2'
const CLIENT = 'client';
const MANAGER = 'manager';

export default class AuthApi {

    static createUser = (user) => {
        console.log(user);
        fire.auth().createUserWithEmailAndPassword(user.email, user.password)
            .then((userCredential) => {
                //token
                console.log(userCredential);
                console.log(userCredential.user.getIdToken());
                fire
                    .firestore()
                    .collection('users')
                    .doc(userCredential.user.uid)
                    .set({
                        //por defeito, utilizadores registados são clientes
                        roles: [CLIENT],
                        postalCode: user.postalCode,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        uid : userCredential.user.uid
                    }).then(function () {
                        console.log('Utilizador criado com sucesso!')
                        Swall({
                            ype: 'success',
                            title: 'Success',
                            text: 'User Created'
                        })
                    })
                // TODO: modificar aviso de criação

            })
            .catch((error) => {
                // Handle Errors here.
                console.log('Erro ao criar utilizador.')
                console.log(error)
                Swall({
                    type: 'error',
                    title: 'Error',
                    text: error.message
                })
            });
    }

    static loginUser = (user) => {
        fire.auth().signInWithEmailAndPassword(user.email, user.password)
            .then((user) => {
                console.log(user);
                console.log('Login efetuado com sucesso!');
                Swall({
                    ype: 'success',
                    title: 'Success',
                    text: 'Login Successful'
                })
            })
            .catch((error) => {
                console.log(error);
                console.log(error)
                Swall({
                    type: 'error',
                    title: 'Error',
                    text: error.message
                })
            });

    }
}


