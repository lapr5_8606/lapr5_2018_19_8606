import axios from "axios";
import config from '../config/api'

let instance = axios.create({
  baseURL: config.catalogApi,
  headers: {
    Accept: "application/json"
  }
});

let ManagerApi = {
  post: (url, data, clientToken) => {
    return instance.post(url, data, { headers: { Authorization: clientToken } });
  },
  put: (url, data, clientToken) => {
    return instance.put(url, data, { headers: { Authorization: clientToken } });
  },
  get: (url, data, clientToken) => {
    return instance.get(url + "/" + data, {
      headers: { Authorization: clientToken }
    });
  },
  delete: (url, data, clientToken) => {
    return instance.delete(url + "/" + data, {
      headers: { Authorization: clientToken }
    });
  },
  deleteWithBody: (url, data, clientToken) => {
    return instance.delete(
      url,
      { data: data },
      { headers: { Authorization: clientToken } }
    );
  }
};

export default ManagerApi;
