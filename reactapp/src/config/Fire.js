import firebase from 'firebase'
import config from './api'

let apiKey = process.env.REACT_APP_FIREBASE_API_KEY;
let configWithKey = Object.assign(config.firebase, { apiKey });

const fire = firebase.initializeApp(configWithKey);
console.log(configWithKey)
export default fire;