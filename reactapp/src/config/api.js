const configBase = {
    ordersApi: "http://localhost:8082/",
    catalogApi: "http://localhost:5000/api/",
    renderApi: "http://localhost:8079/",
    authorizationApi: "http://localhost:1337/",
    prologApi: "http://localhost:8090/",
    firebase: {
        authDomain: "lapr5-8606.firebaseapp.com",
        databaseURL: "https://lapr5-8606.firebaseio.com",
        projectId: "lapr5-8606",
        storageBucket: "lapr5-8606.appspot.com",
        messagingSenderId: "452499393078"
    }
};

const configDev = {};
const configProd = {};

const config = (process.env.NODE_ENV == "production") ?
    Object.assign(configBase, configProd) :
    Object.assign(configBase, configDev);

export default config;