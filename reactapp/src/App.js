import React, { Component } from "react";
import Layout from "./components/Layout/Layout";
import fire from "./config/Fire";
import UserInfoApi from "./api/UserInfoApi";

class App extends Component {
  state = {
    userToken: null,
    roles: []
  };

  componentDidMount = () => {
    fire.auth().onAuthStateChanged(user => {
      if (user) {
        user.getIdToken().then(token => {
          this.setState(state => {
            state.userToken = token;
            return state;
          });

          UserInfoApi.getUserRoles(token).then(response => {
            this.setState(state => {
              state.roles = response.data;
              return state;
            });
          });
        });
      } else {
        this.setState(state => {
          state.userToken = null;
          state.roles = [];
          return state;
        });
      }
    });
  };

  logout = () => {
    fire
      .auth()
      .signOut()
      .then(() => {
        console.log("logout success");
      })
      .catch(error => {
        console.log("logout error", error);
      });
  };

  render() {
    return (
      <Layout
        user={{ token: this.state.userToken, roles: this.state.roles }}
        onUserLogout={this.logout}
      />
    );
  }
}

export default App;
