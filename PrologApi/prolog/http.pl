% ------------------------------------------------------Bibliotecas---------------------------------------------
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_client)).
:- use_module(library(http/http_parameters)).
:- use_module(factories).
:- use_module(postals).
:- consult('bestPath.pl').
:- use_module(library(http/json)). % provides support for the core JSON object serialization.
:- use_module(library(http/json_convert)). % converts between the primary representation of JSON terms in Prolog and more application oriented Prolog terms. E.g. point(X,Y) vs. object([x=X,y=Y]).
:- use_module(library(http/http_json)). % hooks the conversion libraries into the HTTP client and server libraries.

% ------------------------------------------------------Criação de Objetos---------------------------------------------
%Usados para enviar respostas json 

:- json_object
        city(name:text,latitude:number, longitude:number).% + [type=city].

:- json_object
        factory(factoryPlace:text,clientePlace:text) .%+ [type=factory].
		
:- json_object
        path(origem:text,shortestPath:list ,distance:number) .%+ [type=factory].

% Exitem vários métodos sobre json como:
% json_to_prolog -> Transformar json em prolog ex. json_to_prolog(city(porto,1,1,true),X).		
% prolog_to_json -> Transformar prolog em json ex. prolog_to_json(city(porto,1,1,true),X).
% http_read_json and http_read_json_dict para ler json
% reply_json para responder um json 

% ------------------------------------------------------Relação entre pedidos HTTP e predicados que os processam---------------------------------------------		
:- http_handler(root(.),http_redirect(moved_temporary,home), []).
		
:- http_handler(root('nearestFactory'),nearestFactoryHttp,[]). % Método que dará a fábrica mais próxima da morada de encomenda do cliente
:- http_handler(root('nearestFactoryTest'),nearestFactoryTest,[]). %Método que testa o algoritmo da fábrica mais próxima
:- http_handler(root('bestPath'),bestPath,[]). % Método que dará o menor circuito
:- http_handler(root('city'),determineCodePostal,[]). % Método que retornará a cidade através do codigoPostal
:- http_handler(root('factories'), allFactories, []).
:- http_handler(root('cities'), allCities, []).	
:- http_handler(root('lapr5'), lapr5, []).	
:- http_handler(root('home'),	home,	  []).
:- http_handler(root('send_file_post'), send_file_post, []).

% ------------------------------------------------------Criação de servidor HTTP no porto 'Port', Neste Caso 8090---------------------------------------------						
server(Port) :-						
        http_server(http_dispatch, [port(Port)]).
		
:-http_server(http_dispatch, [port(8090)]).		
		
% ------------------------------------------------------Apresentação LAPR5---------------------------------------------		
% Tratamento de 'http://localhost:8090/lapr5'
lapr5(_Request) :-				
        format('Content-type: text/plain~n~n'),
        format('LAPR5 - 2018/2019 ! ~n ').
		
% ------------------------------------------------------Enviar Ficheiro---------------------------------------------
% MÉTODO POST enviando um ficheiro de texto
% http_client:http_post('http://localhost:8090/send_file_post', form_data([file=file('./teste.txt')]), Reply, []).

send_file_post(Request) :-
	http_parameters(Request,[ file(X,[])]),
    format('Content-type: text/plain~n~n'),
	format('Received: ~w~n',[X]).

% ------------------------------------------------------Atribuir Fábrica Mais Próxima---------------------------------------------
% Método que dá a fábrica mais próxima	
% MÉTODO GET: Tratamento de http_client:http_get('http://localhost:8090/nearestFactory?city=\'4575\'',X,[]).
%Testar  http_client:http_get('http://localhost:8090/nearestFactory?city=4575',X,[]).

nearestFactoryHttp(Request) :-
    http_parameters(Request,
                   [ city(Postal, [])]),
				   postalCity(City,Postal),
				   nearestFactory(City,Factory,_),	
	prolog_to_json(factory(Factory,City),X),	reply_json(X); prolog_to_json(factory(null,null),X),reply_json(X).
	
% ------------------------------------------------------Atribuir Fábrica Mais Próxima Teste---------------------------------------------

% Método que testa  algoritmo da fábrica mais próxima	
% MÉTODO GET: Tratamento de http_client:http_get('http://localhost:8090/nearestFactoryTest?city=\'4575\'',X,[]).
%Testar  http_client:http_get('http://localhost:8090/nearestFactoryTest?city=4575',X,[]).

nearestFactoryTest(Request) :-
    http_parameters(Request,
                   [ city(Postal, [])]),
				   postalCity(City,Postal),
				   findall(
	    (Name),
	    (
		factory(F), dist_cities(City,F,Dist),metersToKms(Dist,Distance),atom_concat('Factory: ',F,F1),atom_concat(F1,' Distance Between Factory and Client Place: ',N1),atom_concat(N1,Distance,N2),atom_concat(N2, ' kms', Name)
	    ),
	    Factories),
	prolog_to_json(Factories,X),	reply_json(X,[]); prolog_to_json(factory(null,null),X),reply_json(X).
	
	
% ------------------------------------------------------Listar Todas as Fábricas---------------------------------------------	
% Método que dá todas as fábricas	
% MÉTODO GET: Tratamento de http_client:http_get('http://localhost:8090/factories',X,[]).
%Testar  http_client:http_get('http://localhost:8090/factories',X,[]).

%:- ensure_loaded(factories).

allFactories(Request) :-
    	http_parameters(Request,
			[match(Match, [optional(true), default('.*')])]),

	findall(
	    factory(Name,Place),
	    (
		factory(Name), Place=null,
		re_match(Match/i, Name)
	    ),
	    Factories),

	prolog_to_json(Factories, S),
	reply_json(S, []).

% ------------------------------------------------------Listar Todas as Cidades---------------------------------------------	
% Método que dá todas as cidades	
% MÉTODO GET: Tratamento de http_client:http_get('http://localhost:8090/cities',X,[]).
%Testar  http_client:http_get('http://localhost:8090/cities',X,[]).


allCities(Request) :-
    	http_parameters(Request,
			[match(Match, [optional(true), default('.*')])]),

	findall(
	    city(Name,Latitude,Longitude),
	    (
		city(Name,Latitude,Longitude),
		re_match(Match/i, Name)
	    ),
	    Cities),

	prolog_to_json(Cities, S),
	reply_json(S, []).

% ------------------------------------------------------Determinar Cidade pelo Código Postal---------------------------------------------

% Método que determina o distrito pelo código postal	
% MÉTODO GET: Tratamento de http_client:http_get('http://localhost:8090/city?codPostal=\'4575-583\'',X,[]).
%Testar  http_client:http_get('http://localhost:8090/city?codPostal=4575',X,[]).

% MÉTODO POST
% http_client:http_post('http://localhost:8090/nearestFactory', form_data([city='porto']), Reply, []).
%Testar http_client:http_post('http://localhost:8090/nearestFactory', form_data([city='porto']), Reply, []).

determineCodePostal(Request) :-
    http_parameters(Request,
                   [codPostal(Cp, [])]),
				   postalCity(City,Cp),
				   city(City,Latitude,Longitude),
				   prolog_to_json(city(City,Latitude,Longitude),X),reply_json(X);
				  
				   prolog_to_json(city(null,0,0),X),reply_json(X).
				   
				   
% ------------------------------------------------------Determinar Menor Caminho ---------------------------------------------

% MÉTODO POST			   
bestPath(Request) :-
   http_read_json(Request,json([factoryName=Factory|[client=Cities|_]])),
   tsp3(Factory,Cities,Path,Distance),
   prolog_to_json(path(Factory,Path,Distance),X),
   reply_json(X).
   
   			   
	