:- module(factories,[nearestFactory/3,cityExist/2,allFactories/3,city/3,factory/1,testNearestFactory/4,dist_cities/3,distance/5,metersToKms/2,degrees2radians/2]).

:-dynamic city/3.
% -----------------------------------------------------------------------
% Factos de Citys com localizacao baseada em
% latitude e longitude e predicado auxiliar para calcular a Distance
% entre quaisquer duas destas Citys.
% ------------------------------------------------------------------------

% Dados do site http://dateandtime.info/pt/

%city(name,latitude,longitude)
city(aveiro,40.6442700,-8.6455400).
city(beja,38.0150600, -7.8632300).
city(braga,41.545448, -8.426507).
city(braganca,41.8058200,-6.7571900).
city(casteloBranco,39.8221900,-7.4908700).
city(coimbra,40.2056400,-8.4195500).
city(evora,38.5666700,-7.9000000).
city(faro,37.019356, -7.930440).
city(guarda,40.5373300,-7.2657500).
city(leiria,39.7436200,-8.8070500).
city(lisboa,38.736946, -9.142685).
city(portalegre,39.2937900,-7.4312200).
city(porto,41.1496100,-8.6109900).
city(santarem,39.2333300,-8.6833300).
city(setubal,38.5244000,-8.8882000).
city(vianaDoCastelo,41.6932300, -8.8328700).
city(vilaReal,41.3006200,-7.7441300).
city(viseu,40.6610100,-7.9097100).
city(acores,37.7412,-25.6756).
city(madeira,32.7607,-16.9594).


factory(porto).
factory(lisboa).
factory(faro).
factory(leiria).

% Calcula Distance entre duas Citys
%  dist_cities(brussels,prague,D).
%  D = 716837.
dist_cities(C1,C2,Dist):-
    city(C1,Lat1,Lon1),
    city(C2,Lat2,Lon2),
    distance(Lat1,Lon1,Lat2,Lon2,Dist).

degrees2radians(Deg,Rad):-
	Rad is Deg*0.0174532925.

% distance(latitude_first_point,longitude_first_point,latitude_second_point,longitude_second_point,distance
% in meters)
distance(Lat1, Lon1, Lat2, Lon2, Dis2):-
	degrees2radians(Lat1,Psi1),
	degrees2radians(Lat2,Psi2),
	DifLat is Lat2-Lat1,
	DifLon is Lon2-Lon1,
	degrees2radians(DifLat,DeltaPsi),
	degrees2radians(DifLon,DeltaLambda),
	A is sin(DeltaPsi/2)*sin(DeltaPsi/2)+ cos(Psi1)*cos(Psi2)*sin(DeltaLambda/2)*sin(DeltaLambda/2),
	C is 2*atan2(sqrt(A),sqrt(1-A)),
	Dis1 is 6371000*C,
	Dis2 is round(Dis1).

	
/*Methods that determines number of cities and factories.*/
contCities(Number):-findall(X,(city(X,_,_)),L),length(L,Number).
contFactories(Number):-findall(X,factory(X),L),length(L,Number).

/*Method Main*/
nearestFactory(City,Factory,Distance):-allFactories(City,Factory,Distance).

/*check if the factory exists*/
cityExist(City,City2):-city(City,_,_), City2=City,!.
cityExist(City,_):-(\+city(City,_,_)),write('Inserted city does not exist'),nl,!,fail.

/*if the city is a factory, then returns the city in the factory variable. Else find the nearest factory */
allFactories(City,Factory,0):-factory(City),Factory=City,!.

allFactories(City,Factory,Distance):-(\+factory(City)),
    findall((Dist:Fac1),(factory(Fac1), dist_cities(City,Fac1,Dist)),ListDistances),
	sort(ListDistances,ListDistanceOrd),
    ListDistanceOrd=[(DistanceAux:Factory)|_],
	metersToKms(DistanceAux,Distance).
 


testNearestFactory(City,Factory,0,_):-factory(City),Factory=City,!.
testNearestFactory(City,_,Distance , ListDistanceOrd):-(\+factory(City)),
    findall((Distance:Fac1),(factory(Fac1), dist_cities(City,Fac1,Dist),metersToKms(Dist,Distance)),ListDistances),
	sort(ListDistances,ListDistanceOrd).
	
/*Meters to Kms*/	
metersToKms(Distance1,Distance):-DistanceAux is Distance1*0.001,Distance is integer(DistanceAux).
	