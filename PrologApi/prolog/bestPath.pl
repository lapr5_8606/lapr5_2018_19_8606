
:-use_module(factories).
:-use_module(library(random)).
:-dynamic cityToDelivery/3.

:-set_prolog_flag(answer_write_options,
                   [ quoted(true),
                     portray(true),
                     spacing(next_argument)
                   ]).



/* --------------- Iteracao 2 --------------*/
tsp2(Orig,Dest,Cam):-
    tsp2(Orig,Dest,[Orig],Cam,1).
     /*condicao final: nodo actual = destino*/
tsp2(_,Dest,LA,Cam,N):-
    count_cities(Count),
    N==Count,
     /*caminho actual esta invertido*/
    reverse([Dest|LA],Cam).

tsp2(Orig,Dest,LA,Cam,N):-
    /*testar ligacao entre ponto
    actual e um qualquer X*/
    /*connect(Act,X),*/
    findall((Dist,X),(cityToDelivery(X,_,_),\+member(X,LA),dist_cities(Orig,X,Dist)),List),
    sort(List,OrderedDistances),
    OrderedDistances = [(_,NearestCity)|_],
    /*testar nao circularidade para nao
    visitar nodos ja visitados*/
    \+ member(NearestCity,LA),
    N1 is N +1,
    /*chamada recursiva*/
    tsp2(NearestCity,Dest,[NearestCity|LA],Cam,N1). 
/* --------------- Iteracao 2  FIM --------------*/

createCitiesToDelivery([]).

createCitiesToDelivery([City|Cities]):-
	city(City,Latitude,Longitude),
	assertz(cityToDelivery(City,Latitude,Longitude)),
	createCitiesToDelivery(Cities).

	

/* --------------- Iteracao 3  ----------------*/


tsp3(Origem,Cities,Result,Distance):-
	createCitiesToDelivery(Cities),
    tsp2(Origem,Origem,Caminho),
    lista_segmento(Caminho,Segmentos),
    tsp3rec(Origem,Segmentos,ResultSegmentos),
	segmento_lista(ResultSegmentos,Result1),
	append(Result1,[Origem],Result),
	distanciaCaminho(Result,Distance1),
	metersToKms(Distance1,Distance),
	retractall(cityToDelivery(_,_,_)).

/*Distancia Caminho*/
distanciaCaminho([],0).
distanciaCaminho([_],0).
distanciaCaminho([Cidade1,Cidade2|T],Dist):-dist_cities(Cidade1,Cidade2,D1),distanciaCaminho([Cidade2|T],DC), Dist is D1+DC.

tsp3rec(Origem,Segmentos,Result):-
    (
        remove_cruzamento(Segmentos,ResultAux),  
        (
            rGraph(Origem,ResultAux,Regraphed),
            tsp3rec(Origem,Regraphed,Result)
        );
        (
            Result = Segmentos
        )
    ).

remove_cruzamento(Segmentos,Result):-
    member([P1,Q1],Segmentos),
    member([P2,Q2],Segmentos),
    [P1,Q1] \== [P2,Q2],
    P2 \== Q1,
    P1 \== Q2,
    interseta(P1,Q1,P2,Q2),
    troca_segmentos(([P1,Q1])|([P2,Q2]),Segmentos,SegmentosAtualizados),
    Result = SegmentosAtualizados.


troca_segmentos([P1,Q1]|[P2,Q2],Segmentos,SegmentosAtualizados):-
    (
        member([P1,P2],Segmentos),   
        (
            NovaLigacao1 = [([P2,Q1])|[]],
            NovaLigacao = [([P1,Q2])|NovaLigacao1]
        );
        (
            NovaLigacao1 = [([P1,P2])|[]],
            NovaLigacao = [([Q1,Q2])|NovaLigacao1]
        )

    ),
    delete(Segmentos,[P1,Q1],SegmentosAtualizados1),
    delete(SegmentosAtualizados1,[P2,Q2],SegmentosAtualizados2),
    append(NovaLigacao,SegmentosAtualizados2,SegmentosAtualizados).



lista_segmento(Lista,Result):-
    lista_segmento2(Lista,[],Result).

lista_segmento2([_],ListaSegmentos,Result):-
    reverse(ListaSegmentos,Result).

lista_segmento2([H1|[H2|T]],ListaSegmentos,Result):-
    lista_segmento2([H2|T],[([H1,H2])|ListaSegmentos],Result).

	
segmento_lista(Segmentos,Result):-
	segmento_lista2(Segmentos,[],Result).
	
segmento_lista2([],Aux,Result):-
	reverse(Aux,Result).
	
segmento_lista2([[City|_]|Segmentos],Aux,Result):-
	segmento_lista2(Segmentos,[City|Aux],Result).

/*predicados auxiliares*/
count_cities(Count) :-
        findall(1,cityToDelivery(_,_,_),L),
        length(L,Count).


coordenadas_plano(Cidade,X,Y):-
    city(Cidade,LatDeg,LonDeg),
    degrees2radians(LatDeg,LatRad),
    degrees2radians(LonDeg,LonRad),
    X is 6371 * cos(LatRad) * cos(LonRad),
    Y is 6371 * cos(LatRad) * sin(LonRad).

interseta(Cidade1Seg1,Cidade2Seg1,Cidade1Seg2,Cidade2Seg2):-
    coordenadas_plano(Cidade1Seg1,X1,Y1),
    coordenadas_plano(Cidade2Seg1,X2,Y2),
    coordenadas_plano(Cidade1Seg2,X3,Y3),
    coordenadas_plano(Cidade2Seg2,X4,Y4),
    doIntersect((X1,Y1),(X2,Y2),(X3,Y3),(X4,Y4)).



avalia(Seq,V):-
	avalia(Seq,0,V).

avalia([_],_,0).
avalia([C1|[C2|Resto]],DistAcc,V):-
    dist_cities(C1,C2,Distance),
	DistTotal is DistAcc+Distance,
	avalia([C2|Resto],DistTotal,VResto),
	VT is DistTotal,
	V is VT+VResto.



% Given three colinear points p, q, r, the function checks if
% point q lies on line segment 'pr'
%onSegment(P, Q, R)
onSegment((PX,PY), (QX,QY), (RX,RY)):-
    QX =< max(PX,RX),
    QX >= min(PX,RX),
    QY =< max(PY,RY),
    QY >= min(PY,RY).

 
% To find orientation of ordered triplet (p, q, r).
% The function returns following values
% 0 --> p, q and r are colinear
% 1 --> Clockwise
% 2 --> Counterclockwise

orientation((PX,PY), (QX,QY), (RX,RY), Orientation):-
	Val is (QY - PY) * (RX - QX) - (QX - PX) * (RY - QY),
	
	(
		Val == 0, !, Orientation is 0;
		Val >0, !, Orientation is 1;
		Orientation is 2
	).
 
orientation4cases(P1,Q1,P2,Q2,O1,O2,O3,O4):-
    orientation(P1, Q1, P2,O1),
    orientation(P1, Q1, Q2,O2),
    orientation(P2, Q2, P1,O3),
    orientation(P2, Q2, Q1,O4).
 
 
 
% The main function that returns true if line segment 'p1q1'
% and 'p2q2' intersect.
doIntersect(P1,Q1,P2,Q2):-
    % Find the four orientations needed for general and
    % special cases
	orientation4cases(P1,Q1,P2,Q2,O1,O2,O3,O4),
	
	(	
    % General case
    O1 \== O2 , O3 \== O4,!;

    % Special Cases
    % p1, q1 and p2 are colinear and p2 lies on segment p1q1
    O1 == 0, onSegment(P1, P2, Q1),!;
 
    % p1, q1 and p2 are colinear and q2 lies on segment p1q1
    O2 == 0, onSegment(P1, Q2, Q1),!;
 
    % p2, q2 and p1 are colinear and p1 lies on segment p2q2
    O3 == 0, onSegment(P2, P1, Q2),!;
 
     % p2, q2 and q1 are colinear and q1 lies on segment p2q2
    O4 == 0, onSegment(P2, Q1, Q2),!
    ).    
    


%----------------------------------------------------------------------------------------------------
% rGraph(Origin,UnorderedListOfEdges,OrderedListOfEdges)
%
% Examples:
% ---------
% ?- rGraph(a,[[a,b],[b,c],[c,d],[e,f],[d,f],[e,a]],R).
%
% ?- rGraph(brussels,[[vienna, sarajevo], [sarajevo, tirana],[tirana,sofia], [sofia, minsk], [andorra,brussels],[brussels,minsk],[vienna,andorra]],R).
%
%
rGraph(Orig,[(Orig,Z)|R],R2):-!,
	reorderGraph([(Orig,Z)|R],R2).
rGraph(Orig,R,R3):-
	member([Orig,X],R),!,
	delete(R,[Orig,X],R2),
	reorderGraph([[Orig,X]|R2],R3).
rGraph(Orig,R,R3):-
	member([X,Orig],R),
	delete(R,[X,Orig],R2),
	reorderGraph([[Orig,X]|R2],R3).


reorderGraph([],[]).

reorderGraph([[X,Y],[Y,Z]|R],[[X,Y]|R1]):-
	reorderGraph([[Y,Z]|R],R1).

reorderGraph([[X,Y],[Z,W]|R],[[X,Y]|R2]):-
	Y\=Z,
	reorderGraph2(Y,[[Z,W]|R],R2).

reorderGraph2(_,[],[]).
reorderGraph2(Y,R1,[[Y,Z]|R2]):-
	member([Y,Z],R1),!,
	delete(R1,[Y,Z],R11),
	reorderGraph2(Z,R11,R2).
reorderGraph2(Y,R1,[[Y,Z]|R2]):-
	member([Z,Y],R1),
	delete(R1,[Z,Y],R11),
	reorderGraph2(Z,R11,R2).
