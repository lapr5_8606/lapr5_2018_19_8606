﻿

# Testes End-To-End

## Criar produto

1. Abrir o browser
2. Introduzir o endereço da aplicação
3. Carregar no botão na barra de navegação, alternado assim para a página do gestor
4. Na barra lateral carregar na opção produto
5. Por defeito vai ser aberta a tab de criação de produto
6. Introduzir o nome do produto
7. Introduzir o preço do produto
8. Introduzir uma categoria valida
9. Introduzir um material valido
  1. Para introduzir mais, carregar no botão destinado para esse fim. Só é permitido adicionar outro material caso todos os campos para a adição de material estejam devidamente preenchidos.
  2. Para apagar um material, carregar no botão destinado para esse fim. Só é permitido apagar um material caso haja dois ou mais materiais.
10. Introduzir os valores da largura:
  1. Se pretender valores contínuos, apenas preencher o valor mínimo e máximo.
  2. Se pretender valores discretos, carregar na checkbox apropriada. Proceder da mesma forma previamente descrita na introdução dos materiais.
11. Introduzir os valores da altura:
  1.  Se pretender valores contínuos, apenas preencher o valor mínimo e máximo.
  2. Se pretender valores discretos, carregar na checkbox apropriada. Proceder da mesma forma previamente descrita na introdução dos materiais.
12. Introduzir os valores da profundidade:
  1. Se pretender valores contínuos, apenas preencher o valor mínimo e máximo.
  2. Se pretender valores discretos, carregar na checkbox apropriada. Proceder da mesma forma previamente descrita na introdução dos materiais.
13. Submeter o produto carregando no botão submeter.
14. Navegar para a tab de procurar produto
15. Pesquisar produto pelo nome previamente criado
16. Se tudo estiver a funcionar corretamente, devem aparecer os dados introduzidos

## Apagar produto

1. Abrir o browser
2. Introduzir o endereço da aplicação
3. Carregar no botão na barra de navegação, alternado assim para a página do gestor
4. Na barra lateral carregar na opção produto
5. Carregar na tab apagar
6. Introduzir um nome valido para o produto a apagar
7. Carregar no botão apagar
8. Navegar para a tab de procurar produto
9. Pesquisar produto pelo nome previamente apagado
10. Se tudo estiver a funcionar corretamente, deverá aparecer uma mensagem de erro

## Alterar produto

1. Abrir o browser
2. Introduzir o endereço da aplicação
3. Carregar no botão na barra de navegação, alternado assim para a página do gestor
4. Na barra lateral carregar na opção produto
5. Carregar na tab alterar
6. Introduzir o nome do produto a alterar
7. Se pretender, introduza um novo nome do produto
8. Se pretender, introduza um novo preço para o produto
9. Se pretender, introduza uma nova categoria
10. Se pretender, introduza a lista uma nova lista de materiais
  1. Para introduzir mais, carregar no botão destinado para esse fim. Só é permitido adicionar outro material caso todos os campos para a adição de material estejam devidamente preenchidos.
  2. Para apagar um material, carregar no botão destinado para esse fim. Só é permitido apagar um material caso haja dois ou mais materiais.
11. Se pretender, introduza os valores da largura:
  1. Se pretender valores contínuos, apenas preencher o valor mínimo e máximo.
  2. Se pretender valores discretos, carregar na checkbox apropriada. Proceder da mesma forma previamente descrita na introdução dos materiais.
12. Se pretender, introduza os valores da altura:
  1.  Se pretender valores contínuos, apenas preencher o valor mínimo e máximo.
  2. Se pretender valores discretos, carregar na checkbox apropriada. Proceder da mesma forma previamente descrita na introdução dos materiais.
13. Se pretender, introduza os valores da profundidade:
  1. Se pretender valores contínuos, apenas preencher o valor mínimo e máximo.
  2. Se pretender valores discretos, carregar na checkbox apropriada. Proceder da mesma forma previamente descrita na introdução dos materiais.
14. Submeter o produto carregando no botão alterar.
15. Navegar para a tab de procurar produto
16. Pesquisar produto pelo nome previamente alterado
17. Se tudo estiver a funcionar corretamente, devem aparecer os dados alterados.

## Criar nova categoria

1. Abrir o browser.
2. Introduzir o endereço da aplicação.
3. Carregar no botão na barra de navegação, alternado assim para a página do gestor.
4. Na barra lateral carregar na opção categoria.
5. Por defeito vai ser aberta a tab de criação de categoria.
6. Introduzir o nome da categoria.
7. Se pretender que essa categoria tenha um pai, carregar na checbox apropriada.
8. Introduzir um nome para a categoria pai.
9. Carregar no botão submeter.
10. Navegar para a tab de procurar uma categoria
11. Pesquisar a categoria pelo nome e verificar se todos os dados estão certos

## Alterar categoria

1. Abrir o browser.
2. Introduzir o endereço da aplicação.
3. Carregar no botão na barra de navegação, alternado assim para a página do gestor.
4. Na barra lateral carregar na opção categoria.
5. Carregar na tab alterar
6. Introduzir o nome da categoria a alterar.
7. Introduzir um novo pai para essa categoria
8. Carregar no botão alterar.
9. Navegar para a tab de procurar
10. Procurar pela categoria previamente criada
11. Verificar se todos os dados estão corretos.

## Criar material

1. Abrir o browser.
2. Introduzir o endereço da aplicação.
3. Carregar no botão na barra de navegação, alternado assim para a página do gestor.
4. Na barra lateral carregar na opção material.
5. Por defeito é aberta a tab de criação do material
6. Introduzir um nome para o material
7. Introduzir a lista de acabamentos desse material.
8. Carregar no botão criar.
9. Navegar para a tab procurar
10. Pesquisar o pelo material previamente criado.
11. Se tudo tiver sucesso, deverá aparecer todos os detalhes anteriormente introduzidos.

## Apagar material

1. Abrir o browser.
2. Introduzir o endereço da aplicação.
3. Carregar no botão na barra de navegação, alternado assim para a página do gestor.
4. Na barra lateral carregar na opção material.
5. Navegar para a tab de apagar o material.
6. Introduzir o nome do material a apagar.
7. Navegar para a tab procurar
8. Deverá ser lançado um erro caso tente procurar pelo material previamente apagado.

## Editar material

1. Abrir o browser.
2. Introduzir o endereço da aplicação.
3. Carregar no botão na barra de navegação, alternado assim para a página do gestor.
4. Na barra lateral carregar na opção material.
5. Navegar para a tab alterar.
6. Introduzir o nome do material a alterar.
7. Carregar no botão procurar.
8. A página será alterada para mostrar os detalhes desse material.
9. Alterar os dados pretendidos.
10. Carregar no botão alterar.
11. Navegar para a a tab procurar.
12. Introduzir o nome do material alterado.
13. Verificar se todos os dados estão de acordo com os previamente introduzidos.

## Associar produtos

1. Abrir o browser.
2. Introduzir o endereço da aplicação.
3. Carregar no botão na barra de navegação, alternado assim para a página do gestor.
4. Na barra lateral carregar na opção associar produtos.
5. Por defeito será aberta a tab de criação de uma associação
6. Introduzir o nome do produto pai
7. Introduzir o nome do produto filho
8. Se pretender que seja uma associação obrigatória carregar na checkbox apropriada.
9. Se pretender que ambos produtos tenham o mesmo material carregar na checkbox apropriada.
10. Se pretender que as dimensões do produto filho seja condicionadas pelas do pai, carregar na checkbox apropriada.
  1. Preencher todos os campos
11. Carregar no botão submeter
12. Navegar para a página de criar uma encomenda.
13. Efetuar uma encomenda com os produtos da associação previamente criada.
14. Se todos os dados tiverem em coerência a encomenda deve ser efetuada com sucesso.

## Apagar associação

1. Abrir o browser.
2. Introduzir o endereço da aplicação.
3. Carregar no botão na barra de navegação, alternado assim para a página do gestor.
4. Na barra lateral carregar na opção associar produtos.
5. Navegar para a tab de apagar
6. Introduzir o nome do produto pai
7. Introduzir o nome do produto filho
8. Carregar no botão apagar
9. Navegar para a página de criar uma encomenda.
10. Efetuar uma encomenda com os produtos da associação previamente apagada.
11. Se a associação tiver sido apagada, deverá ser lançado um erro.

## Criar coleção

1. Abrir o browser.
2. Introduzir o endereço da aplicação.
3. Fazer login como gestor
4. Inserir o código de autenticação de 2 fatores.
5. Na barra lateral carregar na opção collection.
6. Por defeito é aberta a tab de criação de collection.
7. Introduzir um nome para a coleção.
8. Introduzir a linha estética que a coleção vai seguir.
9. Escolher os produtos que vão fazer parte da coleção.
10. Clicar no botão "Create"
11. Navegar para a tab Search.
12. Pesquisar o pela coleção previamente criada.
13. Se tiver sido criada a coleção, deverá aparecer na lista de coleções a coleção criada.
14. Se tudo tiver corrido com sucesso, ao selecionar a coleção deverão aparecer todos os detalhes da coleção.

## Apagar coleção

1. Abrir o browser.
2. Introduzir o endereço da aplicação.
3. Fazer login como gestor
4. Inserir o código de autenticação de 2 fatores.
5. Na barra lateral carregar na opção collection.
6. Navegar para a tab Delete.
6. Selecionar a coleção que pretende apagar.
7. Clicar no botão Delete.
8. Se tudo correr como previsto, ao navegar para a tab Search, não estará lá a coleção previamente apagada.

## Criar catálogo

1. Abrir o browser.
2. Introduzir o endereço da aplicação.
3. Fazer login como gestor
4. Inserir o código de autenticação de 2 fatores.
5. Na barra lateral carregar na opção catálogo.
6. Por defeito é aberta a tab de criação do catálogo.
7. Introduzir um nome para o catálogo.
8. Escolher os produtos que vão fazer parte do catálogo.
9. Clicar no botão "Create"
10. Navegar para a tab Listar.
11. Pesquisar o pelo catálogo previamente criado.
12. Se tiver sido criado o catálogo, deverá aparecer na lista de catálogos o novo catálogo.
13. Se tudo tiver corrido com sucesso, ao selecionar a tab procurar e meter o nome do catálogo, deverão aparecer todos os seus detalhes.

## Apagar catálogo

1. Abrir o browser.
2. Introduzir o endereço da aplicação.
3. Fazer login como gestor
4. Inserir o código de autenticação de 2 fatores.
5. Na barra lateral carregar na opção catálogo.
6. Navegar para a tab Apagar.
6. Selecionar o catálogo que pretende apagar.
7. Clicar no botão Apagar.
8. Se tudo correr como previsto, ao navegar para a tab Procurar, não estará lá o catálogo previamente apagado.





