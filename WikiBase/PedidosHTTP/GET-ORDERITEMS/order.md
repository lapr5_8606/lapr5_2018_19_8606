# HTTP POST .../order/:id/items

## SD Diagram
![Sequence Diagram](item.jpg)

## Rest Documentation

### Title
```
    Pesquisar todos os itens de uma encomenda
```

### Url
```
    /order/:id/items
```

### Method:
```
    GET
```
### Url Params:
```json
    {
        "id" : string
    }
```
#### Example
```json
    {
        "id" : "asdalksdasidhj2798798c"
    }
}
```
### Success Response
```
    Code: 200
```

### Testes
[testes](testes.json)