# HTTP GET .../api/restriction/{id}

## SD Diagram
(idêntico ao do pedido GET de uma categoria por ID)

## Rest Documentation

### Title: 
```
    Pesquisar uma restrição por ID
```
### Url: 
```
    /api/restriction/{id}
```
### Method:
```
    Get
```
### URL Params:
``` 
    id
```
#### Example
```
    .../api/restriction/1
```
### Success Response
```
    Code: 200
```
### Error Response
```json
    Code: 404 NotFound
    {
        "errorMessage" : "No restriction found with the specified ID."
    }
```

## Testes
[Ficheiro de testes](GET_restriction_by_id_tests.postman_collection.json)
