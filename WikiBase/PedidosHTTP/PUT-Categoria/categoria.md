# HTTP PUT .../api/category

## SD Diagram
![Sequence Diagram](categoria.jpg)

## Rest Documentation

### Title
```
    Alterar uma o pai de uma categoria
```

### Url
```
    /api/category
```

### Method:
```
    PUT
```
### Url Params:
```json
    {
        "CategoryId" : [int],
        "ParentCategoryId": [id]
    }
```
#### Example
```json
    {
        "CategoryId" : 10,
        "ParentCategoryId" : 1,
    }
```

### Success Response
```
    Code: 200
```
### Error Response
``` json
    Code: 404 NotFound
    Content:
    {
        "errorMessage" : "Category with the specified id doesn't exist."
    }
```
``` json
    Code: 404 NotFound
    Content:
    {
        "errorMessage" : "Parent category doesn't exist."
    }
```

### Testes
[testes](testes.json)