# HTTP GET .../city?codPostal=4575

## Documentation

### Title
```
   Determine the city by postal code
```

### Url
```
    /city?codPostal=
```

### Method:
```
    GET
```

#### Example
```
    /city?codPostal=4575
```
### Success Response
```
    Code: 200
```
### Error Response
``` json
    Code: 404 NotFound
    
```

### Explanation
``` 
	It starts by saving the value of codPostal in a variable. Next, it is verified whether this postal code exists in the knowledge base. 
	If it exists, you return the city and its coordinates (latitude and longitude). If it does not exist, we return null and 0,0 coordinates.
```

