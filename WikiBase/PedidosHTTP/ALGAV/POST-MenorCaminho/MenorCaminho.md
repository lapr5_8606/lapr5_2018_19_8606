# HTTP POST .../bestPath

## Documentation

### Title
```
    Determine the shortest path to go through the cities once
```

### Url
```
    /bestPath
```

### Method:
```
    POST
```
### Url Params:
```json
    {
        "factoryPlace" : Text
		"cliente" : List
        
    }
```
#### Example
```json
    {
		"factoryPlace" : "porto"
		"cliente" : ["porto","lisboa","braga","aveiro"]
}
```
### Success Response
```
    Code: 201
```

### Explanation
```
	It starts by reading the values that are present in the json that is received (factoryName-Initial City, Client-List of Cities where we have to go through). 
	Next, we use the tsp3 method that determines the shortest path, which path has no crosses once they are removed.
	In the end, we return a json object of type path (Factory, Path, Distance), where Factory represents the origin of the path (equal to factoryName), 
	Path that holds the smallest path and Distance that has the distance of that path.
```