# HTTP GET .../nearestFactory?city=3000

## Documentation

### Title
```
  Determines the factory closest to a particular city. The city is determined by the postal code
```

### Url
```
    /nearestFactory?city=3000
```

### Method:
```
    GET
```
### Url Params:
```json
    {
        "city" : [int]
    }
```
#### Example
```
    /nearestFactory?city=3000
```
### Success Response
```
    Code: 200
```
### Error Response
``` json
    Code: 404 NotFound
   
```

### Explanation
```
	It starts by fetching the value of the city parameter. 
	This value will be an integer representing the zip code.
	Next, you'll find out which city has this zip code. 
	If it exists, we save the name of the city in clientPlace, and then we do a findall, where we will save the factory and its distance from the city stored in clientPlace. 
	Next, we sort of descending order, store the first city in factoryPlace, and return a json object of factory type (factoryPlace, clientPlace).
	If the postal code does not exist in the knowledge base (if it does not belong to continental Portugal) we return a json object of type factory (factoryPlace, clientPlace) with the two values to null.
```