# HTTP GET .../cities


## Documentation

### Title
```
   List all cities
```

### Url
```
    /cities
```

### Method:
```
    GET
```

#### Example
```
    /cities
```
### Success Response
```
    Code: 200
```
### Error Response
``` json
    Code: 404 NotFound
    
```

### Explanation
``` 
	Start by doing a findall where you find all the facts of the city/3 type.
	It saves them in a list and transforms this list into a json that will be returned at the end.
```

