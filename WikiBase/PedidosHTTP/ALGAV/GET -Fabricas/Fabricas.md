# HTTP GET .../factories

## Documentation

### Title
```
   List all factories
```

### Url
```
    /factories
```

### Method:
```
    GET
```

#### Example
```
    /factories
```
### Success Response
```
    Code: 200
```
### Error Response
``` json
    Code: 404 NotFound
```

### Explanation
```
	Start by doing a findall where you find all the facts of the factory/1 type.
	It saves them in a list and transforms this list into a json that will be returned at the end.
```
