# HTTP GET .../userInfo

## Documentation

### Title
```
   Determine the roles for given user
```

### Url
```
    URL/userInfo
```

### Method:
```
    GET
```

#### Example
```
    /userInfo
```
### Success Response
```
    Code: 200
	body content : array with all information stored at firestore of given user, that is: first name, last name, postal code, roles and his unique identifier
```
### Error Response
``` json
    Code: 404 NotFound
    
```

### Explanation
``` 
	This request must be filled with the following header configuration "Authorization" = USER-TOKEN otherwise it won't work.
```

