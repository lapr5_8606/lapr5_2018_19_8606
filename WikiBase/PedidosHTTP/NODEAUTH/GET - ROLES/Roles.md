# HTTP GET .../roles

## Documentation

### Title
```
   Determine the roles for given user
```

### Url
```
    URL/roles
```

### Method:
```
    GET
```

#### Example
```
    /roles
```
### Success Response
```
    Code: 200
	body content : array with all the roles that user has
```
### Error Response
``` json
    Code: 404 NotFound
    
```

### Explanation
``` 
	This request must be filled with the following header configuration "Authorization" = USER-TOKEN otherwise it won't work.
```

