# HTTP POST .../api/material

## SD Diagram
![Sequence Diagram](material.jpg)

## Rest Documentation

### Title
```
    Criar um novo material e o seu acabamento
```

### Url
```
    /api/material
```

### Method:
```
    POST
```
### Url Params:
```json
    {
        "MaterialName" : [string],
        "Finishes" : [Finish]
    }
```
#### Example
```json
    {
        "MaterialName": "Material 1",
        "Finishes": [
            {
                "Name": "Finish 1"
            },
            {
                "Name": "Finish 2"
            }
        ]
    }
```
### Success Response
```
    Code: 201
```
### Error Response
``` json
    Code: 409 Conflict
    Content:
    {
        "errorMessage" : "Material already exists."
    }
```

### Testes
[testes](testes.json)