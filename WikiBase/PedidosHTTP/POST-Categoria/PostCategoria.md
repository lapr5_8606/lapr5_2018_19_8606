# HTTP POST .../api/category

## SD Diagram
![Sequence Diagram](NovaCategoria.jpg)

## Rest Documentation

### Title: 
```
    Criar uma nova categoria
```
### Url: 
```
    /api/category
```
### Method:
```
    Post
```
### Data Params:
``` json
    {
        "CategoryName" : [string],
        "ParentCategoryName" : [string]
    }
```
#### Example
```json
    {
        "CategoryName" : "Category1",
        "ParentCategoryName" : "ParentCategory"
    }
```
### Success Response
```
    Code: 201
```
### Error Response
``` json
    Code: 409 Conflict
    Content:
    {
        "errorMessage" : "Category already exists."
    }
```
```json
    Code: 404 NotFound
    {
        "errorMessage" : "Parent category doesn't exist."
    }
```

## Testes
[Ficheiro de testes](testes.json)