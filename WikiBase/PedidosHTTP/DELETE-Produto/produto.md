# HTTP DELETE .../api/produto/{id}

## SD Diagram
Identico ao delete do material por id

## Rest Documentation

### Title
```
   Apagar um produto por id
```

### Url
```
    /api/produto/{id}
```

### Method:
```
    DELETE
```
### Url Params:
```json
    {
        "id" : [int]
    }
```
#### Example
```
    /api/produto/15
```
### Success Response
```
    Code: 200
```
### Error Response
``` json
    Code: 404 NotFound
    Content:
    {
        "errorMessage" : "Product with the specified id doesn't exist."
    }
```