# HTTP GET .../api/product/{id}/parts

## SD Diagram
(idêntico ao do pedido GET all)

## Rest Documentation

### Title: 
```
    Pesquisar todas as categorias e respetivas subcategorias
```
### Url: 
```
    /api/category/all/
```
### Method:
```
    Get
```
### URL Params:
``` 
    none
```
#### Example
```
    .../api/category/all/
```
### Success Response
```
    Code: 200
```

## Testes
TODO
