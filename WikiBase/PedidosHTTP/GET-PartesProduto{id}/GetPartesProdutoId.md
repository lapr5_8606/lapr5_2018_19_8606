# HTTP GET .../api/product/{id}/parts

## SD Diagram
(idêntico ao do pedido GET de uma categoria por ID)

## Rest Documentation

### Title: 
```
    Pesquisar as partes de um produto especificado por ID
```
### Url: 
```
    /api/product/{id}/parts
```
### Method:
```
    Get
```
### URL Params:
``` 
    id
```
#### Example
```
    .../api/product/1/parts
```
### Success Response
```
    Code: 200
```

## Testes
[Ficheiro de testes](GET_parts_of_product_tests.postman_collection.json)
