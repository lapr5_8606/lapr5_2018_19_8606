# HTTP Get .../order/:id/items/:id

## SD Diagram
Semelhante ao pedido Get de item de uma encomenda

## Rest Documentation

### Title
```
    Pesquisar itens te item de encomenda
```

### Url
```
    /order/:id/items/:id
```

### Method:
```
    GET
```
### Url Params:
```json
    {
        "id" : String,
        "id" : String
    }
```
#### Example
```json
    {
        "id" : "asdhgjasdsh8273ccbasd",
        "id" : "aisdhkajsdh1231236821"
    }
}
```
### Success Response
```
    Code: 200
```

### Testes
[testes](testes.json)