# HTTP Get .../order/:id/items/:id

## SD Diagram
![Sequence Diagram](item.jpg)

## Rest Documentation

### Title
```
    Pesquisar items por id
```

### Url
```
    /item/:id
```

### Method:
```
    GET
```
### Url Params:
```json
    {
        "id" : String
    }
```
#### Example
```json
    {
        "id" : "asdhgjasdsh8273ccbasd"
    }
}
```
### Success Response
```
    Code: 200
```

### Testes
[testes](testes.json)