# HTTP GET .../api/product/{id}/restrictions

## SD Diagram
(idêntico ao do pedido GET de uma categoria por ID)

## Rest Documentation

### Title: 
```
    Pesquisar as restrições associadas a um produto especificado por ID
```
### Url: 
```
    /api/product/{id}/restrictions
```
### Method:
```
    Get
```
### URL Params:
``` 
    id
```
#### Example
```
    .../api/product/1/restrictions
```
### Success Response
```
    Code: 200
```

## Testes
[Ficheiro de testes](GET_restrictions_of_product_tests.postman_collection.json)
