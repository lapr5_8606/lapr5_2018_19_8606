# HTTP GET .../api/product/{id}

## SD Diagram
(idêntico ao do pedido GET de uma categoria por ID)

## Rest Documentation

### Title: 
```
    Pesquisar um produto por ID
```
### Url: 
```
    /api/product/{id}
```
### Method:
```
    Get
```
### URL Params:
``` 
    id
```
#### Example
```
    .../api/product/1
```
### Success Response
```
    Code: 200
```
### Error Response
```json
    Code: 404 NotFound
    {
        "errorMessage" : "Product with specified ID not found."
    }
```

## Testes
[Ficheiro de testes](GET_product_by_id_tests.postman_collection.json)
