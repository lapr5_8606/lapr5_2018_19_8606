# HTTP PUT .../api/material
## SD Diagram
![Sequence Diagram](material.jpg)

## Rest Documentation

### Title
```
    Alterar um material e acabamento por id
```

### Url
```
    /api/material
```

### Method:
```
    PUT
```
### Url Params:
```json
    {
        "ID" : [int],
        "Name": [string],
        "AlreadyExistentFinishes": [Finish],
        "NewFinishes": [Finish]
    }
```
#### Example
```json
    {
        "ID": 1,
        "Name": "Material 1.2",
        "AlreadyExistentFinishes": [
            {
                "ID": 2,
                "Name": "Finish 2.2"
            }
        ],
        "NewFinishes": [
            {
                "Name": "Finish 3"
            },
            {
                "Name": "Finish 4"
            },
            {
                "Name": "Finish 5"
            }
        ]
    }
```

### Success Response
```
    Code: 200
```
### Error Response
```json
    Code: 404 NotFound
    Content:
    {
        "errorMessage" : "Material doesn't exist."
    }
```
```json
    Code: 404 NotFound
    {
        "errorMessage" : "Finish with the following ID not found: <ID>"
    }
```
```json
    Code: 404 NotFound
    {
        "errorMessage" : "The material doesn't have a finish with the following ID: <ID>"
    }
```
### Testes
[teste](teste.json)