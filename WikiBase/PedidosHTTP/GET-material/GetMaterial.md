# HTTP GET .../api/materialfinish/{id}

## SD Diagram
![Sequence Diagram](material.jpg)

## Rest Documentation

### Title
```
    Pesquisar um material e acabamento por id
```

### Url
```
    /api/materialfinish/{id}
```

### Method:
```
    GET
```
### Url Params:
```json
    {
        "id" : [int]
    }
```
#### Example
```
    /api/materialfinish/15
```
### Success Response
```
    Code: 200
```
### Error Response
``` json
    Code: 404 NotFound
    Content:
    {
        "errorMessage" : "Material with the specified id doesn't exist."
    }
```

### Testes
[Test File](testes.json)