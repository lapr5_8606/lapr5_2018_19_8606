# HTTP GET .../api/material/all

## SD Diagram
(idêntico ao do pedido GET all)

## Rest Documentation

### Title: 
```
    Pesquisar todos os materiais e respetivos acabamentos
```
### Url: 
```
    /api/material/all/
```
### Method:
```
    Get
```
### URL Params:
``` 
    none
```
#### Example
```
    .../api/material/all/
```
### Success Response
```
    Code: 200
```

## Testes
TODO
