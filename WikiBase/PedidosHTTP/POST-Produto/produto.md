# HTTP POST .../api/product

## SD Diagram
Identico ao pedido post da categoria

## Rest Documentation

### Title
```
    Criar um produto
```

### Url
```
    /api/product
```

### Method:
```
    POST
```
### Url Params:
```json
    {
        "ProductName" : [string],
        "Price" : [float],
        "CategoryId" : [int],
        "NewHeightDimensions" : {
            "IsDiscrete" : [bool],
            "Values" : [float]
        },
        "NewWidthDimensions" : {
            "IsDiscrete" : [bool],
            "Values" : [float]
        },
        "NewDepthDimensions" : {
            "IsDiscrete" : [bool],
            "Values" : [float]
        },
        "Materials" : [int]
    }
```
#### Example
```json
    {
        "ProductName": "Product 1",
        "Price": 50.5,
        "CategoryId": 1,
        "NewHeightDimensions":
            {
                "IsDiscrete": true,
                "Values": [1.1, 2, 3, 4]
            },
        "NewWidthDimensions":
            {
                "IsDiscrete": false,
                "Values": [1, 2]
            },
        "NewDepthDimensions":
            {
                "IsDiscrete": false,
                "Values": [1, 2]
            },
        "Materials": [1]
    }
```
### Success Response
```
    Code: 201
```

### Error Response
```json
    Code: 409 Conflict
    {
        "errorMessage" : "Product already exists."
    }
```

```json
    Code: 404 NotFound
    {
        "errorMessage" : "Category doesn't exist."
    }
```

```json
    Code: 400 BadRequest
    {
        "errorMessage" : "Wrong dimensions specified."
    }
```

### Testes
[testes](testes.json)
