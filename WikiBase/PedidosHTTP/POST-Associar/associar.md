# HTTP POST .../api/product/associate

## SD Diagram
(idêntico ao do pedido POST de uma produto)

## Rest Documentation

### Title: 
```
    Associar dois produtos
``` 
### Url: 
```
    /api/product/associate
```
### Method:
```
    POST
```
### BODY Params:
``` json
    {
        "ParentId" :[int],
        "ChildId" : [int],
        "IsMandatory" : [bool],
        "RestrictMaterials" : [bool],
        "PercentageRestrictions" : [float]
    }
```
#### Example
```json
{
    "ParentId": 1,
	"ChildId": 2,
	"IsMandatory": true,
	"RestrictMaterials": true,
	"PercentageRestrictions": [0.5, 0.7, 0.7, 0.9, 0, 100]
}
    
```
### Success Response
```
    Code: 201
```
### Error Response
```json
    Code: 404 NotFound
    {
        "errorMessage" : "Parent product doesn't exist."
    }
```

```json
    Code: 404 NotFound
    {
        "errorMessage" : "Child product doesn't exist."
    }
```

```json
    Code: 409 Conflict
    {
        "errorMessage" : "Product relationship already exists."
    }
```

```json
    Code: 400 BadRequest
    {
        "errorMessage" : "Product does not fit the parent product"
    }
```

```json
    Code: 400 BadRequest
    {
        "errorMessage" : "The child product doesn't have any of the materials of the parent product."
    }
```

```json
    Code: 400 BadRequest
    {
        "errorMessage" : "Invalid number of percentage restrictions. There should have been a minimum and maximum percentage restriction for each dimension."
    }
```

```json
    Code: 400 BadRequest
    {
        "errorMessage" : "The child product wouldn't fit the parent product with the specified percentage restrictions."
    }
```
### Testes
[testes](teste.json)
