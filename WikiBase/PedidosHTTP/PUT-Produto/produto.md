# HTTP PUT .../api/product

## SD Diagram
identico ao put do material

## Rest Documentation

### Title
```
    Alterar um produto por id
```

### Url
```
    /api/product
```

### Method:
```
    PUT
```
### Url Params:
```json
    {
        "ProductId" : [int],
        "Name": [string],
        "Price": [float],
        "CategoryId": [int],
        "NewHeightDimensions" : {
            "IsDiscrete" : [bool],
            "Values" : [float]
        },
        "NewWidthDimensions" : {
            "IsDiscrete" : [bool],
            "Values" : [float]
        },
        "NewDepthDimensions" : {
            "IsDiscrete" : [bool],
            "Values" : [float]
        },
        "MaterialsAndFinishes" : [int]
    }
```
#### Example
```json
    {
       	"ProductId": 1,
        "Name": "Product 1.1",
        "Price": 29.99,
        "CategoryId": 2,
        "NewHeightDimensions":
            {
                "IsDiscrete": false,
                "Values": [2, 4]
            },
        "NewWidthDimensions":
            {
                "IsDiscrete": false,
                "Values": [1, 2]
            },
        "NewDepthDimensions":
            {
                "IsDiscrete": true,
                "Values": [1, 3]
            },
        "MaterialsAndFinishes": [1, 2]
    }
```
### Success Response
```
    Code: 200
```
### Error Response
```json
    Code: 404 NotFound
    {
        "errorMessage" : "Product with specified ID not found."
    }
```
```json
    Code: 404 NotFound
    {
        "errorMessage" : "Category doesn't exist."
    }
```
```json
    Code: 400 BadRequest
    {
        "errorMessage" : "Wrong dimensions specified."
    }
```
