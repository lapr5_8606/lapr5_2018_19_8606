# HTTP GET .../api/product?name={name}

## SD Diagram
(idêntico ao do pedido GET de uma categoria por ID)

## Rest Documentation

### Title: 
```
    Pesquisar um produto por nome
```
### Url: 
```
    /api/product?name={name}
```
### Method:
```
    Get
```
### URL Params:
``` 
    name
```
#### Example
```
    .../api/product?name=Produto1
```
### Success Response
```
    Code: 200
```
### Error Response
```json
    Code: 404 NotFound
    {
        "errorMessage" : "Product with specified name not found."
    }
```

## Testes
[Ficheiro de testes](GET_product_by_name_tests.postman_collection.json)
