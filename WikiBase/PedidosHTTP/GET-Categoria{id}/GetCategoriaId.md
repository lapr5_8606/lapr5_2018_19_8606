# HTTP GET .../api/category/{id}

## SD Diagram
![SEquence Diagram](get_categoria_por_id-sequence_diagram.jpg)

## Rest Documentation

### Title: 
```
    Pesquisar uma categoria por ID
```
### Url: 
```
    /api/category/{id}
```
### Method:
```
    Get
```
### URL Params:
``` 
    id
```
#### Example
```
    .../api/category/1
```
### Success Response
```
    Code: 200
```
### Error Response
```json
    Code: 404 NotFound
    {
        "errorMessage" : "Category with specified ID not found."
    }
```

## Testes
[Ficheiro de testes](GET_category_by_id.postman_collection.json)
