# HTTP POST .../order

## SD Diagram
![Sequence Diagram](encomenda.jpg)

## Rest Documentation

### Title
```
    Criar uma nova encomenda
```

### Url
```
    /order
```

### Method:
```
    POST
```
### Url Params:
```json
    {
        "item" : {
            "productId" : Number,
            "material" : Number,
            "finish" : Number,
            "width" : Number,
            "height" : Number,
            "depth" : Number,
            "children" : [Number]
        }
    }
```
#### Example
```json
    {
    "item" : {
        "productId" : 3,
        "material" : 1,
        "finish" : 1,
        "width" : 200,
        "height" : 200,
        "depth" : 200,
        "children" : []
    }
}
```
### Success Response
```
    Code: 201
```

### Testes
[testes](testes.json)