# HTTP GET .../api/product/{id}/partOf

## SD Diagram
(idêntico ao do pedido GET de uma categoria por ID)

## Rest Documentation

### Title: 
```
    Pesquisar os pais de um produto especificado por ID
```
### Url: 
```
    /api/product/{id}/partOf
```
### Method:
```
    Get
```
### URL Params:
``` 
    id
```
#### Example
```
    .../api/product/1/partOf
```
### Success Response
```
    Code: 200
```

## Testes
[Ficheiro de testes](GET_parents_of_product_tests.postman_collection.json)
