# HTTP Get .../order/:id

## SD Diagram
![Sequence Diagram](order.jpg)

## Rest Documentation

### Title
```
    Pesquisar encomenda por id
```

### Url
```
    /order/:id
```

### Method:
```
    GET
```
### Url Params:
```json
    {
        "id" : String
    }
```
#### Example
```json
    {
        "id" : "asdhgjasdsh8273ccbasd"
    }
}
```
### Success Response
```
    Code: 200
```

### Testes
[testes](testes.json)