# HTTP POST .../order/createMockOrders

## Documentation

### Title
```
   Create mock orders for testing purpose
```

### Url
```
    /order/createMockOrders
```

### Method:
```
    POST
```

#### Example
```
	URL/order/createMockOrders with following body
    {
	"item" : {
        "productId" : 3,
        "material" : 1,
        "finish" : 1,
        "width" : 200,
        "height" : 200,
        "depth" : 200,
        "children" : []
    },
	"postalCodes":["3530-123","3734-123","7230-123","4705-123","5140-123","6000-123","3025-123","7080-123","8500-123","6360-123","2525-123","1150-123","7301-123","4269-123","2130-123","2835-123","4900-123","5400-123"]
}
```
### Success Response
```
    Code: 200
	{
        "mocked orders created"
    }
```
### Error Response
``` json
    Code: 400 Badrequest
	{
		"Error creating mocked orders"
    }
```

### Explanation
``` 
	This request is intented to create orders to test algorithms only.
	
```

