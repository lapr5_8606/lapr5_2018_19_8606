# HTTP GET .../order/getBestDeliveryPath

## Documentation

### Title
```
   Get the best delivery path of existing orders in readyDelivery state
```

### Url
```
    /order/getBestDeliveryPath
```

### Method:
```
    GET
```

#### Example
```
    /order/getBestDeliveryPath
```
### Success Response
```
    Code: 200
	   {
        "origem": "porto",
        "shortestPath": [
            "porto",
            "braga",
            "vianaDoCastelo",
            "aveiro",
            "coimbra",
            "leiria",
            "santarem",
            "lisboa",
            "setubal",
            "evora",
            "beja",
            "faro",
            "portalegre",
            "casteloBranco",
            "guarda",
            "viseu",
            "braganca",
            "porto"
        ],
        "distance": 1516
    }
```
### Error Response
``` json
	{
		null
	}
    
```

### Explanation
``` 
	This request is suposed to run automatically once per week in order to generate the best path for trucks to deliver the orders.
```

