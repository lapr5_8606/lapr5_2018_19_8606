# Bem vindo

Esta é a página com a documentação sobre o nosso projeto

## Modelo de Domínio
[Modelo de Domínio](Modelo_de_dominio/modelo_de_domino.md)

## Pedidos HTTP
1. [Nova Categoria](PedidosHTTP/POST-Categoria/PostCategoria.md)
2. [Categoria com ID](PedidosHTTP/GET-Categoria{id}/GetCategoriaId.md)
3. [Alterar Categoria](PedidosHTTP/PUT-Categoria/categoria.md)
4. [Material e acabamento](PedidosHTTP/GET-material/GetMaterial.md)
5. [Novo Material e acabamento](PedidosHTTP/POST-Material/PostMaterial.md)
6. [Apagar Material e acabamento](PedidosHTTP/DELETE-Material/material.md)
7. [Produto com ID](PedidosHTTP/GET-Produto{id}/GetProdutoId.md)
8. [Produto com nome](PedidosHTTP/GET-Produto{name}/GetProdutoName.md)
9. [Partes de um produto](PedidosHTTP/GET-PartesProduto{id}/GetPartesProdutoId.md)
10. [Associar dois produtos](PedidosHTTP/POST-Associar/associar.md)
11. [Apagar um produto](PedidosHTTP/DELETE-Produto/produto.md)
12. [Alterar um produto](PedidosHTTP/PUT-Produto/produto.md)
13. [Pais de um produto](PedidosHTTP/GET-PaisProduto{id}/GetPaisProdutoId.md)
14. [Restrições de um produto](PedidosHTTP/GET-RestricoesProduto{id}/GetRestricoesProdutoId.md)
15. [Restrição com ID](PedidosHTTP/GET-Restricao{id}/GetRestricaoId.md)
16. [Encomenda](PedidosHTTP/GET-Encomenda/order.md)
17. [Item](PedidosHTTP/GET-ITEM/item.md)
18. [Itens de encomenda](PedidosHTTP/GET-ORDERITEMS/order.md)
19. [Itens de itens de encomenda](PedidosHTTP/GET-ITEMOFITEMORDER/item.md)
20. [Nova encomenda](PedidosHTTP/POST-ENCOMENDA/encomenda.md)
21. [Novo produto](PedidosHTTP/POST-Produto/produto.md)
22. [Alterar material](PedidosHTTP/PUT-Material/material.md)

## Frontend
1. [Apagar agregação](frontend/apagar_agregacao/apagar_agregacao.md)
2. [Apagar material](frontend/apagar_material/apagar_material.md)
3. [Apagar produto](frontend/apagar_produto/apagar_produto.md)
4. [Criar agregação](frontend/criar_agregacao/criar_agregacao.md)
5. [Criar material](frontend/criar_material/criar_material.md)
6. [Criar produto](frontend/criar_produto/criar_produto.md)
7. [Editar agregação](frontend/editar_agregacao/editar_agregacao.md)
8. [Editar material](frontend/editar_material/editar_material.md)
9. [Editar produto](frontend/editar_produto/editar_produto.md)
10. [Listar produtos](frontend/listar_produtos/listar_produtos.md)
11. [Procurar material](frontend/procurar_material/procurar_material.md)
12. [Procurar produto](frontend/procurar_produto/procurar_produto.md)
13. [Criar encomenda](frontend/criar_encomenda/criar_encomenda.md)

## Testes End-To-End
1. [Testes](testes/end.md)

## Testes de Integração
1. [Materiais](Testes_integracao/Material.postman_collection.json)
2. [Produtos](Testes_integracao/Product.postman_collection.json)
3. [Associação de produtos](Testes_integracao/Product_Relationship.postman_collection.json)

## Diagramas de sistema
1. [Componentes](Sistema/Componentes/componente.md)
2. [Processos](Sistema/Processos/processo.md)
3. [Implnatação](Sistema/Implantacao/implantacao.md)
4. [Packages](Sistema/Packages/packages.md)
5. [Classes](Sistema/Classes/classes.md)

## Server link
https://armario.azurewebsites.net/