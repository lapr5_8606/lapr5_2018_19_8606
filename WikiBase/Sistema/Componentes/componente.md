# Diagramas de componentes

Nesta página vemos a vista de componentes da aplicação

## Diagrama geral
![DiagramaGeral](cpo.jpg)

## Diagrama em detalhe sobre o componente Closet
![DiagramaDetalhe](detalhe.jpg)

## Diagrama em detalhe sobre o componente Orders
![Orders](node.jpg)

## Diagrama detalhado sobre o componente ReactApp
![react](react.jpg)

