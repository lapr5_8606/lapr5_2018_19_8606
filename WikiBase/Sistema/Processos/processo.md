# Diagramas de processos

Nesta página vemos a vista de processos do sistema

## Diagrama de processos
![pro](1.jpg)

## Diagrama geral do pedido ao asp net
![Geral](mega_geral.jpg)

## Diagrama asp net
![DiagramaGeral](processos.jpg)

## Diagrama processos do node
![DiagramNode](pro_node.jpg)
