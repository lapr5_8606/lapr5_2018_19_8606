import express from 'express';
import bodyParser = require('body-parser');
import orderRoute = require('./routes/order');

class App {

    public app: any;

    constructor() {
        this.app = express(),
            this.app.use(bodyParser.urlencoded({ extended: false })),
            this.app.use(bodyParser.json()),

            //prevent cors errors
            this.app.use((req, res, next) => {
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Headers', '*');
                if (req.method === 'OPTIONS') {
                    res.header('Access-Control-Allow-Methods', "PUT,POST,PATCH,DELETE,GET");
                    return res.status(200).json({});
                }
                next();
            });
        this.mountRoutes()


    }

    private mountRoutes(): void {

        const router = express.Router();



        let staticServe = express.static(`${__dirname}/public`);
        router.use('/', staticServe);


        this.app.use('/', router);
        this.app.use('/order', orderRoute);

        
        

        this.app.use((req, res, next) => {
            const error: any = new Error('Not found');
            error.status = 404;
            next(error);
        });

        this.app.use((error, req, res, next) => {
            res.status(error.status || 500);
            res.json({
                error: {
                    message: error.message
                }
            });
        });












    }
}

export default new App().app