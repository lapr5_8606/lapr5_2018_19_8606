import { ComponentFactory } from "../factory/ComponentFactory";
import * as THREE from 'three';
import { OrbitControls } from 'three-orbitcontrols-ts'
import { Product } from "../model/product";

var renderer = new THREE.WebGLRenderer({ antialias: true });
var scene: any = null;
var camera: any = null;
var mesh: any = null;
var raycaster: any = null;
var manager: any = null;
var factory: any = null;
var textures: any = null;
var text1: any = null;

var mouse = {
  x: 0,
  y: 0
};

declare var product: Product;

function init() {

  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.BasicShadowMap;

  scene = new THREE.Scene();
  scene.background = new THREE.Color(0xffffff);

  camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
  camera.position.z = -10;


  const material = new THREE.MeshPhongMaterial({ color: 0xff0000, wireframe: true });


  var woodMaterial = new THREE.TextureLoader().load("images/floor.png")
  woodMaterial.wrapS = THREE.RepeatWrapping;
  woodMaterial.wrapT = THREE.RepeatWrapping;
  woodMaterial.repeat.set(4, 4);

  let woodTexture = new THREE.MeshBasicMaterial({ map: woodMaterial })

  var meshFloor = new THREE.Mesh(
    new THREE.PlaneGeometry(20, 20, 20, 20),
    woodTexture/*new THREE.MeshPhongMaterial({ color: 0xf5f5f0, wireframe: true })*/
  );
  meshFloor.rotation.x -= Math.PI / 2; // Rotate the floor 90 degrees


  var ambientLight = new THREE.AmbientLight(0xffffff, 0.2);
  scene.add(ambientLight);

  var light2 = new THREE.PointLight(0xffffff, 0.8, 0, 2);
  //light.position.set(0, 1, 0);
  light2.position.set(0, 0, 8);
  light2.castShadow = true;            // default false
  scene.add(light2);


  light2.shadow.camera.near = 0.1;
  light2.shadow.camera.far = 25;


  var light = new THREE.PointLight(0xffffff, 0.8, 0, 2);
  //light.position.set(0, 1, 0);
  light.position.set(0, 0, -8);
  light.castShadow = true;            // default false
  scene.add(light);


  light.shadow.camera.near = 0.1;
  light.shadow.camera.far = 25;

  factory = new ComponentFactory();

  let schemas: Product[] = [product];


  let rootProduct: THREE.Group = new THREE.Group();

  let itsRoot: boolean = true;

  let drawTool = factory.getComponent(product.category, product.material);
  rootProduct = drawTool.drawObject3D(product, null);
  rootProduct.name = "root"
  scene.add(rootProduct);
  let productGroups: THREE.Group[] = [rootProduct]


  while (schemas.length > 0) {

    var parentSchema: any = schemas.pop();
 
    var parentGroup3d: any

    var groupAux: THREE.Group = new THREE.Group()

    let childProduct3d:THREE.Group = new THREE.Group()

    parentGroup3d = productGroups.pop()
    
    if(parentGroup3d.name != rootProduct.name && itsRoot)
    {
      rootProduct.add(parentGroup3d)
    }


    if (parentSchema.children.length > 0) {

      for (let child of parentSchema.children) {

        let c = createProduct(child);
        let drawToolChild = factory.getComponent(c.category, c.material);
        childProduct3d = drawToolChild.drawObject3D(c, parentSchema);

        parentGroup3d.add(childProduct3d)

        console.log(child)
        console.log(parentGroup3d)


        parentSchema.children = [...parentSchema.children, c];
        //console.log(c)
        schemas.push(c);
        productGroups.push(childProduct3d)
      }

    }
    itsRoot = false


  }


  scene.add(meshFloor);

  const controls = new OrbitControls(camera, renderer.domElement);



  afterInit();


}

function raycast(e) {
  // Step 1: Detect light helper
  //1. sets the mouse position with a coordinate system where the center
  //   of the screen is the origin
  // Metade 70% da width
  mouse.x = (e.clientX / (window.innerWidth * 0.7)) * 2 - 1;
  mouse.y = - (e.clientY / window.innerHeight) * 2 + 1;

  //2. set the picking ray from the camera position and mouse coordinates
  raycaster.setFromCamera(mouse, camera);

  //3. compute intersections (note the 2nd parameter)
  var intersects = raycaster.intersectObjects(scene.children, true);

  /*for (var i = 0; i < intersects.length; i++) {
    console.log(intersects[i].object.parent.parent);
    objSelecionado = intersects[i].object.parent.parent;
    if ((objSelecionado != null && objSelecionado !== 'undefined' && selectionMattersFlag)) {
      tratarObjectoSelecionado(objSelecionado);
    }*/
  return;
  //garantimos que o grupo ativo é sempre o primeiro que encontra (mais proximo da camara)
  //a corrigir quando existirem gavetas (ou nao)
}

function afterInit() {

  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);
  animate();
}


function animate() {
  window.requestAnimationFrame(() => animate());
  renderer.render(scene, camera);
}



function createProduct(order): Product {

  let product: Product =
    new Product(
      order.height,
      order.width,
      order.depth,
      order.category,
      order.material,
      order.children
    )

  return product;

}
window.addEventListener('resize', function () {
  renderer.setSize(window.innerWidth, window.innerHeight);
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
})

window.addEventListener("load", init);

