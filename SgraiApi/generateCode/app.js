"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const bodyParser = require("body-parser");
const orderRoute = require("./routes/order");
class App {
    constructor() {
        this.app = express_1.default(),
            this.app.use(bodyParser.urlencoded({ extended: false })),
            this.app.use(bodyParser.json()),
            //prevent cors errors
            this.app.use((req, res, next) => {
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Headers', '*');
                if (req.method === 'OPTIONS') {
                    res.header('Access-Control-Allow-Methods', "PUT,POST,PATCH,DELETE,GET");
                    return res.status(200).json({});
                }
                next();
            });
        this.mountRoutes();
    }
    mountRoutes() {
        const router = express_1.default.Router();
        let staticServe = express_1.default.static(`${__dirname}/public`);
        router.use('/', staticServe);
        this.app.use('/', router);
        this.app.use('/order', orderRoute);
        this.app.use((req, res, next) => {
            const error = new Error('Not found');
            error.status = 404;
            next(error);
        });
        this.app.use((error, req, res, next) => {
            res.status(error.status || 500);
            res.json({
                error: {
                    message: error.message
                }
            });
        });
    }
}
exports.default = new App().app;
