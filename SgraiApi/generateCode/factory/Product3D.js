"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const THREE = __importStar(require("three"));
const three_1 = require("three");
class Drawer {
    constructor(materialType) {
        this.BASE_IMAGE_URL = "images/";
        this.ARMARIO_ESPESSURA = 0.025;
        this.imageName = materialType.concat(".png");
        this.BASE_IMAGE_URL = this.BASE_IMAGE_URL.concat(this.imageName);
    }
    drawObject3D(product, parentProduct) {
        // console.log("|||")
        // console.log(product)
        // console.log("|||")
        let caberHorizontal = true;
        let caberVertical = true;
        var gaveta = {
            largura: product.width - this.ARMARIO_ESPESSURA, altura: product.height, profundidade: product.depth - this.ARMARIO_ESPESSURA
        };
        var gavetaGroup = new THREE.Group();
        if (parentProduct != null) {
            if (product.width > parentProduct.widthLeft) {
                caberHorizontal = false;
            }
            if (product.height > parentProduct.heightLeft) {
                return gavetaGroup;
            }
            if (!caberHorizontal) {
                parentProduct.heightLeft = parentProduct.heightLeft - product.height;
                gavetaGroup.position.y += parentProduct.height - parentProduct.heightLeft;
                parentProduct.widthLeft = parentProduct.width;
            }
            if (parentProduct.width == parentProduct.widthLeft) {
                gavetaGroup.position.x = ((parentProduct.width / 2) - (product.width / 2));
                parentProduct.widthLeft = parentProduct.widthLeft - product.width + this.ARMARIO_ESPESSURA;
            }
            else {
                gavetaGroup.position.x = ((parentProduct.width / 2) - (product.width / 2)) - (parentProduct.width - parentProduct.widthLeft);
                parentProduct.widthLeft = parentProduct.widthLeft - product.width + this.ARMARIO_ESPESSURA;
            }
        }
        var texturaPuxador = new THREE.MeshPhongMaterial({ color: 0xfd50df, wireframe: false });
        var materialTesteCor = new THREE.MeshPhongMaterial({ color: 0x0f51a, wireframe: true });
        var woodMaterial = new THREE.TextureLoader().load(this.BASE_IMAGE_URL);
        woodMaterial.wrapS = THREE.RepeatWrapping;
        woodMaterial.wrapT = THREE.RepeatWrapping;
        woodMaterial.repeat.set(1, 1);
        let woodTexture = new THREE.MeshPhongMaterial({ map: woodMaterial });
        //var gavetaMaterials = [product.textura, product.textura, product.textura, product.textura, product.textura, product.textura];
        var base = new THREE.Mesh(new THREE.BoxGeometry(gaveta.largura - (2 * this.ARMARIO_ESPESSURA), this.ARMARIO_ESPESSURA, gaveta.profundidade - (2 * this.ARMARIO_ESPESSURA)), woodTexture);
        base.position.y += this.ARMARIO_ESPESSURA; // Move the mesh up 1 meter
        base.position.z -= this.ARMARIO_ESPESSURA / 2.0;
        var ilhargaDireita = new THREE.Mesh(new THREE.BoxGeometry(this.ARMARIO_ESPESSURA, gaveta.altura, gaveta.profundidade - (2 * this.ARMARIO_ESPESSURA)), woodTexture);
        ilhargaDireita.position.y += base.position.y + gaveta.altura / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        ilhargaDireita.position.z -= this.ARMARIO_ESPESSURA / 2.0;
        ilhargaDireita.position.x -= gaveta.largura / 2.0 - this.ARMARIO_ESPESSURA / 2;
        var ilhargaEsquerda = new THREE.Mesh(new THREE.BoxGeometry(this.ARMARIO_ESPESSURA, gaveta.altura, gaveta.profundidade - (2 * this.ARMARIO_ESPESSURA)), woodTexture);
        ilhargaEsquerda.position.z -= this.ARMARIO_ESPESSURA / 2.0;
        ilhargaEsquerda.position.y += base.position.y + gaveta.altura / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        ilhargaEsquerda.position.x += gaveta.largura / 2.0 - this.ARMARIO_ESPESSURA / 2;
        var fundo = new THREE.Mesh(new THREE.BoxGeometry(gaveta.largura, gaveta.altura, this.ARMARIO_ESPESSURA), woodTexture);
        fundo.position.z += gaveta.profundidade / 2.0 - this.ARMARIO_ESPESSURA;
        fundo.position.y += base.position.y + gaveta.altura / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        var porta = new THREE.Mesh(new THREE.BoxGeometry(gaveta.largura, gaveta.altura, this.ARMARIO_ESPESSURA / 2), woodTexture);
        porta.position.z -= gaveta.profundidade / 2.0 - this.ARMARIO_ESPESSURA / 4;
        porta.position.y += base.position.y + gaveta.altura / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        var geometry = new THREE.SphereGeometry(this.ARMARIO_ESPESSURA, 32, 32);
        var sphere = new THREE.Mesh(geometry, woodTexture);
        sphere.position.z -= gaveta.profundidade / 2.0 + this.ARMARIO_ESPESSURA / 4;
        sphere.position.y += base.position.y + gaveta.altura / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        sphere.name = 'NOME_PUXADOR';
        gavetaGroup.add(base);
        gavetaGroup.add(ilhargaDireita);
        gavetaGroup.add(ilhargaEsquerda);
        gavetaGroup.add(fundo);
        gavetaGroup.add(porta);
        gavetaGroup.add(sphere);
        return gavetaGroup;
    }
}
exports.Drawer = Drawer;
class InsideModule {
    constructor(materialType) {
        this.BASE_IMAGE_URL = "images/";
        this.ARMARIO_ESPESSURA = 0.025;
        this.imageName = materialType.concat(".png");
        this.BASE_IMAGE_URL = this.BASE_IMAGE_URL.concat(this.imageName);
    }
    drawObject3D(product, parentProduct) {
        let caberHorizontal = true;
        let caberVertical = true;
        var gaveta = {
            largura: product.width - this.ARMARIO_ESPESSURA, altura: product.height, profundidade: product.depth - this.ARMARIO_ESPESSURA
        };
        var gavetaGroup = new THREE.Group();
        if (parentProduct != null) {
            if (product.width > parentProduct.widthLeft) {
                caberHorizontal = false;
            }
            if (product.height > parentProduct.heightLeft) {
                return gavetaGroup;
            }
            if (!caberHorizontal) {
                parentProduct.heightLeft = parentProduct.heightLeft - product.height;
                gavetaGroup.position.y += parentProduct.height - parentProduct.heightLeft;
                parentProduct.widthLeft = parentProduct.width;
            }
            if (parentProduct.width == parentProduct.widthLeft) {
                gavetaGroup.position.x = ((parentProduct.width / 2) - (product.width / 2));
                parentProduct.widthLeft = parentProduct.widthLeft - product.width + this.ARMARIO_ESPESSURA;
            }
            else {
                gavetaGroup.position.x = ((parentProduct.width / 2) - (product.width / 2)) - (parentProduct.width - parentProduct.widthLeft);
                parentProduct.widthLeft = parentProduct.widthLeft - product.width + this.ARMARIO_ESPESSURA;
            }
        }
        var texturaPuxador = new THREE.MeshPhongMaterial({ color: 0xfd50df, wireframe: false });
        var materialTesteCor = new THREE.MeshPhongMaterial({ color: 0x0f51a, wireframe: true });
        var woodMaterial = new THREE.TextureLoader().load(this.BASE_IMAGE_URL);
        woodMaterial.wrapS = THREE.RepeatWrapping;
        woodMaterial.wrapT = THREE.RepeatWrapping;
        woodMaterial.repeat.set(1, 1);
        let woodTexture = new THREE.MeshPhongMaterial({ map: woodMaterial });
        //var gavetaMaterials = [product.textura, product.textura, product.textura, product.textura, product.textura, product.textura];
        var base = new THREE.Mesh(new THREE.BoxGeometry(gaveta.largura - (2 * this.ARMARIO_ESPESSURA), this.ARMARIO_ESPESSURA, gaveta.profundidade - (2 * this.ARMARIO_ESPESSURA)), woodTexture);
        base.position.y += this.ARMARIO_ESPESSURA * 1.5; // Move the mesh up 1 meter
        base.position.z -= this.ARMARIO_ESPESSURA / 2.0;
        var topo = new THREE.Mesh(new THREE.BoxGeometry(product.width - (this.ARMARIO_ESPESSURA), this.ARMARIO_ESPESSURA, product.depth - this.ARMARIO_ESPESSURA * 2.5), woodTexture);
        topo.name = "topo";
        topo.position.y += product.height - (-this.ARMARIO_ESPESSURA / 2.0); // + ARMARIO_ESPESSURA / 2; // Move the mesh up 1 meter
        topo.position.z -= this.ARMARIO_ESPESSURA / 2;
        var ilhargaDireita = new THREE.Mesh(new THREE.BoxGeometry(this.ARMARIO_ESPESSURA, gaveta.altura, gaveta.profundidade - (2 * this.ARMARIO_ESPESSURA)), woodTexture);
        ilhargaDireita.position.y += base.position.y + gaveta.altura / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        ilhargaDireita.position.z -= this.ARMARIO_ESPESSURA / 2.0;
        ilhargaDireita.position.x -= gaveta.largura / 2.0 - this.ARMARIO_ESPESSURA / 2;
        var ilhargaEsquerda = new THREE.Mesh(new THREE.BoxGeometry(this.ARMARIO_ESPESSURA, gaveta.altura, gaveta.profundidade - (2 * this.ARMARIO_ESPESSURA)), woodTexture);
        ilhargaEsquerda.position.z -= this.ARMARIO_ESPESSURA / 2.0;
        ilhargaEsquerda.position.y += base.position.y + gaveta.altura / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        ilhargaEsquerda.position.x += gaveta.largura / 2.0 - this.ARMARIO_ESPESSURA / 2;
        var fundo = new THREE.Mesh(new THREE.BoxGeometry(gaveta.largura, gaveta.altura, this.ARMARIO_ESPESSURA), woodTexture);
        fundo.position.z += gaveta.profundidade / 2.0 - this.ARMARIO_ESPESSURA;
        fundo.position.y += base.position.y + gaveta.altura / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        gavetaGroup.add(topo);
        gavetaGroup.add(base);
        gavetaGroup.add(ilhargaDireita);
        gavetaGroup.add(ilhargaEsquerda);
        gavetaGroup.add(fundo);
        return gavetaGroup;
    }
}
exports.InsideModule = InsideModule;
class InsideModule2Door {
    constructor(materialType) {
        this.BASE_IMAGE_URL = "images/";
        this.ARMARIO_ESPESSURA = 0.025;
        this.imageName = materialType.concat(".png");
        this.BASE_IMAGE_URL = this.BASE_IMAGE_URL.concat(this.imageName);
    }
    drawObject3D(product, parentProduct) {
        let caberHorizontal = true;
        let caberVertical = true;
        var gaveta = {
            largura: product.width - this.ARMARIO_ESPESSURA, altura: product.height, profundidade: product.depth - this.ARMARIO_ESPESSURA
        };
        var gavetaGroup = new THREE.Group();
        if (parentProduct != null) {
            if (product.width > parentProduct.widthLeft) {
                caberHorizontal = false;
            }
            if (product.height > parentProduct.heightLeft) {
                return gavetaGroup;
            }
            if (!caberHorizontal) {
                parentProduct.heightLeft = parentProduct.heightLeft - product.height;
                gavetaGroup.position.y += parentProduct.height - parentProduct.heightLeft;
                parentProduct.widthLeft = parentProduct.width;
            }
            if (parentProduct.width == parentProduct.widthLeft) {
                gavetaGroup.position.x = ((parentProduct.width / 2) - (product.width / 2));
                parentProduct.widthLeft = parentProduct.widthLeft - product.width + this.ARMARIO_ESPESSURA;
            }
            else {
                gavetaGroup.position.x = ((parentProduct.width / 2) - (product.width / 2)) - (parentProduct.width - parentProduct.widthLeft);
                parentProduct.widthLeft = parentProduct.widthLeft - product.width + this.ARMARIO_ESPESSURA;
            }
        }
        var texturaPuxador = new THREE.MeshPhongMaterial({ color: 0xfd50df, wireframe: false });
        var materialTesteCor = new THREE.MeshPhongMaterial({ color: 0x0f51a, wireframe: true });
        var woodMaterial = new THREE.TextureLoader().load(this.BASE_IMAGE_URL);
        woodMaterial.wrapS = THREE.RepeatWrapping;
        woodMaterial.wrapT = THREE.RepeatWrapping;
        woodMaterial.repeat.set(1, 1);
        let woodTexture = new THREE.MeshPhongMaterial({ map: woodMaterial });
        //var gavetaMaterials = [product.textura, product.textura, product.textura, product.textura, product.textura, product.textura];
        var base = new THREE.Mesh(new THREE.BoxGeometry(gaveta.largura - (2 * this.ARMARIO_ESPESSURA), this.ARMARIO_ESPESSURA, gaveta.profundidade - (2 * this.ARMARIO_ESPESSURA)), woodTexture);
        base.position.y += this.ARMARIO_ESPESSURA * 1.5; // Move the mesh up 1 meter
        base.position.z -= this.ARMARIO_ESPESSURA / 2.0;
        var topo = new THREE.Mesh(new THREE.BoxGeometry(product.width - (this.ARMARIO_ESPESSURA), this.ARMARIO_ESPESSURA, product.depth - this.ARMARIO_ESPESSURA * 2.5), woodTexture);
        topo.name = "topo";
        topo.position.y += product.height - (-this.ARMARIO_ESPESSURA / 2.0); // + ARMARIO_ESPESSURA / 2; // Move the mesh up 1 meter
        topo.position.z -= this.ARMARIO_ESPESSURA / 2;
        var ilhargaDireita = new THREE.Mesh(new THREE.BoxGeometry(this.ARMARIO_ESPESSURA, gaveta.altura, gaveta.profundidade - (2 * this.ARMARIO_ESPESSURA)), woodTexture);
        ilhargaDireita.position.y += base.position.y + gaveta.altura / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        ilhargaDireita.position.z -= this.ARMARIO_ESPESSURA / 2.0;
        ilhargaDireita.position.x -= gaveta.largura / 2.0 - this.ARMARIO_ESPESSURA / 2;
        var ilhargaEsquerda = new THREE.Mesh(new THREE.BoxGeometry(this.ARMARIO_ESPESSURA, gaveta.altura, gaveta.profundidade - (2 * this.ARMARIO_ESPESSURA)), woodTexture);
        ilhargaEsquerda.position.z -= this.ARMARIO_ESPESSURA / 2.0;
        ilhargaEsquerda.position.y += base.position.y + gaveta.altura / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        ilhargaEsquerda.position.x += gaveta.largura / 2.0 - this.ARMARIO_ESPESSURA / 2;
        var fundo = new THREE.Mesh(new THREE.BoxGeometry(gaveta.largura, gaveta.altura, this.ARMARIO_ESPESSURA), woodTexture);
        fundo.position.z += gaveta.profundidade / 2.0 - this.ARMARIO_ESPESSURA;
        fundo.position.y += base.position.y + gaveta.altura / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        gavetaGroup.add(topo);
        gavetaGroup.add(base);
        gavetaGroup.add(ilhargaDireita);
        gavetaGroup.add(ilhargaEsquerda);
        gavetaGroup.add(fundo);
        gavetaGroup.add(this.draw2Door(product));
        return gavetaGroup;
    }
    draw2Door(armario) {
        let duplaPorta = new THREE.Group();
        var textura = new THREE.MeshPhongMaterial({ map: new THREE.TextureLoader().load(this.BASE_IMAGE_URL), side: THREE.DoubleSide });
        //var textura2 = new THREE.MeshPhongMaterial({ map: new THREE.TextureLoader().load(MATERIAL_TEXTURE_PATH2), side: THREE.DoubleSide });
        var portaMaterials1 = [textura, textura, textura, textura, textura, textura];
        //var portaMaterials2 = [textura2, textura2, textura2, textura2, textura2, textura2];
        var texturaPuxador = new THREE.MeshPhongMaterial({ color: 0xffffff, wireframe: true });
        // Porta1
        // Pivot
        var pivot1 = new THREE.Object3D();
        pivot1.position.z -= armario.depth / 2.0 + this.ARMARIO_ESPESSURA / 2.0;
        pivot1.position.y += armario.height / 2.0;
        pivot1.position.x -= armario.width / 2.0 + this.ARMARIO_ESPESSURA / 4.0;
        pivot1.name = "NOME_PIVOT_DUPLA_PORTA1";
        // start rotation at PI
        pivot1.rotation.set(0, Math.PI, 0);
        var porta1 = new THREE.Mesh(new THREE.BoxGeometry(armario.width / 2.0 + this.ARMARIO_ESPESSURA / 2.0, armario.height, this.ARMARIO_ESPESSURA), 
        //new THREE.MeshPhongMaterial({ color: 0xf8f8f0, wireframe: USE_WIREFRAME })
        portaMaterials1);
        porta1.position.x -= armario.width / 4.0;
        var geometry = new THREE.SphereGeometry(this.ARMARIO_ESPESSURA / 2, 32, 32);
        var puxador1 = new THREE.Mesh(geometry, texturaPuxador);
        puxador1.position.z += this.ARMARIO_ESPESSURA;
        puxador1.position.x -= armario.width / 2.0 * 0.8;
        // Porta 2
        var pivot2 = new THREE.Object3D();
        pivot2.position.z -= armario.depth / 2.0 + this.ARMARIO_ESPESSURA / 2.0;
        pivot2.position.y += armario.height / 2.0;
        pivot2.position.x += armario.width / 2.0 + this.ARMARIO_ESPESSURA / 4.0;
        pivot2.name = "NOME_PIVOT_DUPLA_PORTA2";
        // start rotation at PI
        pivot2.rotation.set(0, Math.PI, 0);
        var porta2 = new THREE.Mesh(new THREE.BoxGeometry(armario.width / 2.0 + this.ARMARIO_ESPESSURA / 2.0, armario.height, this.ARMARIO_ESPESSURA), 
        //new THREE.MeshPhongMaterial({ color: 0xf8d7f0, wireframe: USE_WIREFRAME })
        portaMaterials1);
        // porta2.position.z -= armario.profundidade / 2.0 + ARMARIO_ESPESSURA / 2.0;
        // porta2.position.y += armario.altura / 2.0;
        //porta2.position.x += armario.largura / 4.0 + ARMARIO_ESPESSURA / 4.0;
        porta2.position.x += armario.width / 4.0;
        var puxador2 = new THREE.Mesh(geometry, texturaPuxador);
        puxador2.position.z += this.ARMARIO_ESPESSURA;
        puxador2.position.x += armario.width / 2.0 * 0.8;
        puxador1.name = "NOME_PUXADOR";
        puxador2.name = "NOME_PUXADOR";
        porta1.name = "NOME_PORTA_DUPLA_1";
        porta2.name = "NOME_PORTA_DUPLA_2";
        pivot1.add(puxador1);
        pivot1.add(porta1);
        pivot2.add(puxador2);
        pivot2.add(porta2);
        duplaPorta.add(pivot1);
        duplaPorta.add(pivot2);
        return duplaPorta;
    }
}
exports.InsideModule2Door = InsideModule2Door;
class Door {
    constructor(materialType) {
        this.BASE_IMAGE_URL = "images/";
        this.ARMARIO_ESPESSURA = 0.025;
        this.imageName = materialType.concat(".png");
        this.BASE_IMAGE_URL = this.BASE_IMAGE_URL.concat(this.imageName);
    }
    drawObject3D(product) {
        throw new Error("Draw a door!");
    }
}
exports.Door = Door;
class Modulex {
    constructor(materialType) {
        this.BASE_IMAGE_URL = "images/";
        this.ARMARIO_ESPESSURA = 0.025;
        this.imageName = materialType.concat(".png");
        this.BASE_IMAGE_URL = this.BASE_IMAGE_URL.concat(this.imageName);
    }
    drawObject3D(product, parentProduct) {
        //armarioGroup = null;
        var armarioGroup = new THREE.Group();
        armarioGroup.name = "module";
        var componentesGroup = new THREE.Group();
        componentesGroup.name = "componentName";
        let caberHorizontal = true;
        armarioGroup.add(componentesGroup);
        console.log(this.BASE_IMAGE_URL);
        var woodMaterial = new THREE.TextureLoader().load(this.BASE_IMAGE_URL);
        woodMaterial.wrapS = THREE.RepeatWrapping;
        woodMaterial.wrapT = THREE.RepeatWrapping;
        woodMaterial.repeat.set(1, 1);
        let woodTexture = new THREE.MeshPhongMaterial({ map: woodMaterial });
        var base = new THREE.Mesh(new THREE.BoxGeometry(product.width - (this.ARMARIO_ESPESSURA), this.ARMARIO_ESPESSURA, product.depth - this.ARMARIO_ESPESSURA), woodTexture);
        base.name = "base";
        base.position.y += this.ARMARIO_ESPESSURA / 2; // Move the mesh up 1 meter
        base.position.z -= this.ARMARIO_ESPESSURA / 2;
        var topo = new THREE.Mesh(new THREE.BoxGeometry(product.width - (this.ARMARIO_ESPESSURA), this.ARMARIO_ESPESSURA, product.depth - this.ARMARIO_ESPESSURA), woodTexture);
        topo.name = "topo";
        topo.position.y += product.height - this.ARMARIO_ESPESSURA / 2.0; // + ARMARIO_ESPESSURA / 2; // Move the mesh up 1 meter
        topo.position.z -= this.ARMARIO_ESPESSURA / 2;
        var ilhargaDireita = new THREE.Mesh(new THREE.BoxGeometry(this.ARMARIO_ESPESSURA, product.height, product.depth), woodTexture);
        ilhargaDireita.position.y += product.height / 2.0;
        ilhargaDireita.position.x -= product.width / 2.0;
        ilhargaDireita.name = "ilhargadireita";
        var ilhargaEsquerda = new THREE.Mesh(new THREE.BoxGeometry(this.ARMARIO_ESPESSURA, product.height, product.depth), woodTexture);
        ilhargaEsquerda.position.y += product.height / 2.0;
        ilhargaEsquerda.position.x += product.width / 2.0;
        ilhargaEsquerda.name = "ilhargaesquerda";
        var fundo = new THREE.Mesh(new THREE.BoxGeometry(product.width - this.ARMARIO_ESPESSURA, product.height, this.ARMARIO_ESPESSURA), woodTexture);
        fundo.position.z += product.depth / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        fundo.position.y += product.height / 2.0;
        fundo.name = "fundo";
        var componentes = [base, fundo, ilhargaDireita, ilhargaEsquerda, topo];
        componentes.forEach(componente => componentesGroup.add(componente));
        return armarioGroup;
    }
}
exports.Modulex = Modulex;
class ModulexDoor {
    constructor(materialType) {
        this.BASE_IMAGE_URL = "images/";
        this.ARMARIO_ESPESSURA = 0.025;
        this.imageName = materialType.concat(".png");
        this.BASE_IMAGE_URL = this.BASE_IMAGE_URL.concat(this.imageName);
    }
    drawObject3D(product, parentProduct) {
        //armarioGroup = null;
        var armarioGroup = new THREE.Group();
        armarioGroup.name = "module";
        var componentesGroup = new THREE.Group();
        componentesGroup.name = "componentName";
        let caberHorizontal = true;
        armarioGroup.add(componentesGroup);
        console.log(this.BASE_IMAGE_URL);
        var woodMaterial = new THREE.TextureLoader().load(this.BASE_IMAGE_URL);
        woodMaterial.wrapS = THREE.RepeatWrapping;
        woodMaterial.wrapT = THREE.RepeatWrapping;
        woodMaterial.repeat.set(1, 1);
        let woodTexture = new THREE.MeshPhongMaterial({ map: woodMaterial });
        var base = new THREE.Mesh(new THREE.BoxGeometry(product.width - (this.ARMARIO_ESPESSURA), this.ARMARIO_ESPESSURA, product.depth - this.ARMARIO_ESPESSURA), woodTexture);
        base.name = "base";
        base.position.y += this.ARMARIO_ESPESSURA / 2; // Move the mesh up 1 meter
        base.position.z -= this.ARMARIO_ESPESSURA / 2;
        var topo = new THREE.Mesh(new THREE.BoxGeometry(product.width - (this.ARMARIO_ESPESSURA), this.ARMARIO_ESPESSURA, product.depth - this.ARMARIO_ESPESSURA), woodTexture);
        topo.name = "topo";
        topo.position.y += product.height - this.ARMARIO_ESPESSURA / 2.0; // + ARMARIO_ESPESSURA / 2; // Move the mesh up 1 meter
        topo.position.z -= this.ARMARIO_ESPESSURA / 2;
        var ilhargaDireita = new THREE.Mesh(new THREE.BoxGeometry(this.ARMARIO_ESPESSURA, product.height, product.depth), woodTexture);
        ilhargaDireita.position.y += product.height / 2.0;
        ilhargaDireita.position.x -= product.width / 2.0;
        ilhargaDireita.name = "ilhargadireita";
        var ilhargaEsquerda = new THREE.Mesh(new THREE.BoxGeometry(this.ARMARIO_ESPESSURA, product.height, product.depth), woodTexture);
        ilhargaEsquerda.position.y += product.height / 2.0;
        ilhargaEsquerda.position.x += product.width / 2.0;
        ilhargaEsquerda.name = "ilhargaesquerda";
        var fundo = new THREE.Mesh(new THREE.BoxGeometry(product.width - this.ARMARIO_ESPESSURA, product.height, this.ARMARIO_ESPESSURA), woodTexture);
        fundo.position.z += product.depth / 2.0 - this.ARMARIO_ESPESSURA / 2.0;
        fundo.position.y += product.height / 2.0;
        fundo.name = "fundo";
        var componentes = [base, fundo, ilhargaDireita, ilhargaEsquerda, topo];
        componentes.forEach(componente => componentesGroup.add(componente));
        let doorGroup = this.drawDoor(product);
        console.log(doorGroup);
        armarioGroup.add(doorGroup);
        return armarioGroup;
    }
    drawDoor(armario) {
        var porta = new three_1.Group();
        var woodMaterial = new THREE.TextureLoader().load(this.BASE_IMAGE_URL);
        woodMaterial.wrapS = THREE.RepeatWrapping;
        woodMaterial.wrapT = THREE.RepeatWrapping;
        woodMaterial.repeat.set(1, 1);
        let woodTexture = new THREE.MeshPhongMaterial({ map: woodMaterial });
        var texturaPuxador = new THREE.MeshPhongMaterial({ color: 0xffffff, wireframe: true });
        // Pivot
        var pivot = new THREE.Object3D();
        pivot.position.z -= armario.depth / 2.0 + this.ARMARIO_ESPESSURA / 2.0;
        pivot.position.y += armario.height / 2.0;
        pivot.position.x += armario.width / 2.0;
        pivot.name = "pivotname";
        // start rotation at PI
        pivot.rotation.set(0, Math.PI, 0);
        var porta1 = new THREE.Mesh(new THREE.BoxGeometry(armario.width * 1.0 + this.ARMARIO_ESPESSURA * 1.0, armario.height, this.ARMARIO_ESPESSURA), 
        //new THREE.MeshPhongMaterial({ color: 0xf86ff0, wireframe: USE_WIREFRAME })
        woodTexture);
        // porta1.position.z -= armario.profundidade / 2.0 + ARMARIO_ESPESSURA / 2.0;
        // porta1.position.y += armario.altura / 2.0;
        porta1.position.x += armario.width / 2.0;
        var geometry = new THREE.SphereGeometry(this.ARMARIO_ESPESSURA / 2, 32, 32);
        var sphere = new THREE.Mesh(geometry, texturaPuxador);
        sphere.position.z += this.ARMARIO_ESPESSURA;
        sphere.position.x += armario.width * 0.8;
        sphere.name = "sphere";
        porta1.name = "NOME_PORTA_1";
        pivot.add(porta1);
        pivot.add(sphere);
        porta.add(pivot);
        return porta;
    }
}
exports.ModulexDoor = ModulexDoor;
class NullProduct {
    constructor() {
        this.BASE_IMAGE_URL = "images/";
        this.ARMARIO_ESPESSURA = 0.025;
    }
    drawObject3D(product) {
        throw new Error("OBJECT IS NULL");
    }
}
exports.NullProduct = NullProduct;
