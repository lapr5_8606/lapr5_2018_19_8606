"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Product3D_1 = require("../factory/Product3D");
class ComponentFactory {
    getComponent(componentType, materialType) {
        if (componentType == null) {
            return new Product3D_1.NullProduct();
        }
        if (componentType === 'drawer') {
            return new Product3D_1.Drawer(materialType);
        }
        else if (componentType === 'moduleDoor') {
            return new Product3D_1.ModulexDoor(materialType);
        }
        else if (componentType === 'module') {
            return new Product3D_1.Modulex(materialType);
        }
        else if (componentType === 'insideModule') {
            return new Product3D_1.InsideModule(materialType);
        }
        else if (componentType === 'insideModule2Door') {
            return new Product3D_1.InsideModule2Door(materialType);
        }
        return new Product3D_1.NullProduct();
    }
}
exports.ComponentFactory = ComponentFactory;
