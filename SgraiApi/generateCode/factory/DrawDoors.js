"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const three_1 = __importDefault(require("three"));
const three_2 = require("three");
function drawDoor(armario, ARMARIO_ESPESSURA, MATERIAL_TEXTURE_PATH) {
    var porta = new three_2.Group();
    var woodMaterial = new three_1.default.TextureLoader().load(this.BASE_IMAGE_URL);
    woodMaterial.wrapS = three_1.default.RepeatWrapping;
    woodMaterial.wrapT = three_1.default.RepeatWrapping;
    woodMaterial.repeat.set(1, 1);
    let woodTexture = new three_1.default.MeshPhongMaterial({ map: woodMaterial });
    var texturaPuxador = new three_1.default.MeshPhongMaterial({ color: 0xffffff, wireframe: true });
    // Pivot
    var pivot = new three_1.default.Object3D();
    pivot.position.z -= armario.profundidade / 2.0 + ARMARIO_ESPESSURA / 2.0;
    pivot.position.y += armario.altura / 2.0;
    pivot.position.x += armario.largura / 2.0;
    pivot.name = "pivotname";
    // start rotation at PI
    pivot.rotation.set(0, Math.PI, 0);
    var porta1 = new three_1.default.Mesh(new three_1.default.BoxGeometry(armario.largura + ARMARIO_ESPESSURA, armario.altura, ARMARIO_ESPESSURA), 
    //new THREE.MeshPhongMaterial({ color: 0xf86ff0, wireframe: USE_WIREFRAME })
    woodTexture);
    // porta1.position.z -= armario.profundidade / 2.0 + ARMARIO_ESPESSURA / 2.0;
    // porta1.position.y += armario.altura / 2.0;
    porta1.position.x += armario.largura / 2.0;
    var geometry = new three_1.default.SphereGeometry(ARMARIO_ESPESSURA / 2, 32, 32);
    var sphere = new three_1.default.Mesh(geometry, texturaPuxador);
    sphere.position.z += ARMARIO_ESPESSURA;
    sphere.position.x += armario.largura * 0.8;
    sphere.name = "sphere";
    porta1.name = "NOME_PORTA_1";
    pivot.add(porta1);
    pivot.add(sphere);
    porta.add(pivot);
    return porta;
}
exports.drawDoor = drawDoor;
