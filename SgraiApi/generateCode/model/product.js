"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Product {
    constructor(altura, largura, profundidade, category, material, children) {
        this.height = altura;
        this.width = largura;
        this.depth = profundidade;
        this.category = category;
        this.material = material;
        this.children = children;
        this.heightLeft = altura;
        this.widthLeft = largura;
    }
}
exports.Product = Product;
