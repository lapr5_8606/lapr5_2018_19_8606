"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const product_1 = require("../model/product");
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
var product;
router.get('/', (req, res, next) => {
    let body = `
<!DOCTYPE html>
<html lang="en">

<head>
    <title>SGRAI - Armario</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
</head>

<body style="text-align:center;" >
    <h1>Visualizar encomenda 3D</h1>

    <script>window.product = ${JSON.stringify(product)}</script>
    <script> var exports = {}; </script>
    <script type="text/javascript" src="/bundle.js"></script>
</body>

</html>
`;
    res.header("Content-Type", "text/html").send(body);
});
router.post('/', (req, res, next) => {
    product = new product_1.Product(req.body.item.height, req.body.item.width, req.body.item.depth, req.body.item.category, req.body.item.material, req.body.item.children);
    res.status(200).send(JSON.stringify(req.body.item));
});
module.exports = router;
