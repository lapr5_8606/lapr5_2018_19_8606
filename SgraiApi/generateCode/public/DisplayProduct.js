"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const ComponentFactory_1 = require("../factory/ComponentFactory");
const THREE = __importStar(require("three"));
const three_orbitcontrols_ts_1 = require("three-orbitcontrols-ts");
const product_1 = require("../model/product");
var renderer = new THREE.WebGLRenderer({ antialias: true });
var scene = null;
var camera = null;
var mesh = null;
var raycaster = null;
var manager = null;
var factory = null;
var textures = null;
var text1 = null;
var mouse = {
    x: 0,
    y: 0
};
function init() {
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.BasicShadowMap;
    scene = new THREE.Scene();
    scene.background = new THREE.Color(0xffffff);
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.z = -10;
    const material = new THREE.MeshPhongMaterial({ color: 0xff0000, wireframe: true });
    var woodMaterial = new THREE.TextureLoader().load("images/floor.png");
    woodMaterial.wrapS = THREE.RepeatWrapping;
    woodMaterial.wrapT = THREE.RepeatWrapping;
    woodMaterial.repeat.set(4, 4);
    let woodTexture = new THREE.MeshBasicMaterial({ map: woodMaterial });
    var meshFloor = new THREE.Mesh(new THREE.PlaneGeometry(20, 20, 20, 20), woodTexture /*new THREE.MeshPhongMaterial({ color: 0xf5f5f0, wireframe: true })*/);
    meshFloor.rotation.x -= Math.PI / 2; // Rotate the floor 90 degrees
    var ambientLight = new THREE.AmbientLight(0xffffff, 0.2);
    scene.add(ambientLight);
    var light2 = new THREE.PointLight(0xffffff, 0.8, 0, 2);
    //light.position.set(0, 1, 0);
    light2.position.set(0, 0, 8);
    light2.castShadow = true; // default false
    scene.add(light2);
    light2.shadow.camera.near = 0.1;
    light2.shadow.camera.far = 25;
    var light = new THREE.PointLight(0xffffff, 0.8, 0, 2);
    //light.position.set(0, 1, 0);
    light.position.set(0, 0, -8);
    light.castShadow = true; // default false
    scene.add(light);
    light.shadow.camera.near = 0.1;
    light.shadow.camera.far = 25;
    factory = new ComponentFactory_1.ComponentFactory();
    let schemas = [product];
    let rootProduct = new THREE.Group();
    let itsRoot = true;
    let drawTool = factory.getComponent(product.category, product.material);
    rootProduct = drawTool.drawObject3D(product, null);
    rootProduct.name = "root";
    scene.add(rootProduct);
    let productGroups = [rootProduct];
    while (schemas.length > 0) {
        var parentSchema = schemas.pop();
        var parentGroup3d;
        var groupAux = new THREE.Group();
        let childProduct3d = new THREE.Group();
        parentGroup3d = productGroups.pop();
        if (parentGroup3d.name != rootProduct.name && itsRoot) {
            rootProduct.add(parentGroup3d);
        }
        if (parentSchema.children.length > 0) {
            for (let child of parentSchema.children) {
                let c = createProduct(child);
                let drawToolChild = factory.getComponent(c.category, c.material);
                childProduct3d = drawToolChild.drawObject3D(c, parentSchema);
                parentGroup3d.add(childProduct3d);
                console.log(child);
                console.log(parentGroup3d);
                parentSchema.children = [...parentSchema.children, c];
                //console.log(c)
                schemas.push(c);
                productGroups.push(childProduct3d);
            }
        }
        itsRoot = false;
    }
    scene.add(meshFloor);
    const controls = new three_orbitcontrols_ts_1.OrbitControls(camera, renderer.domElement);
    afterInit();
}
function raycast(e) {
    // Step 1: Detect light helper
    //1. sets the mouse position with a coordinate system where the center
    //   of the screen is the origin
    // Metade 70% da width
    mouse.x = (e.clientX / (window.innerWidth * 0.7)) * 2 - 1;
    mouse.y = -(e.clientY / window.innerHeight) * 2 + 1;
    //2. set the picking ray from the camera position and mouse coordinates
    raycaster.setFromCamera(mouse, camera);
    //3. compute intersections (note the 2nd parameter)
    var intersects = raycaster.intersectObjects(scene.children, true);
    /*for (var i = 0; i < intersects.length; i++) {
      console.log(intersects[i].object.parent.parent);
      objSelecionado = intersects[i].object.parent.parent;
      if ((objSelecionado != null && objSelecionado !== 'undefined' && selectionMattersFlag)) {
        tratarObjectoSelecionado(objSelecionado);
      }*/
    return;
    //garantimos que o grupo ativo é sempre o primeiro que encontra (mais proximo da camara)
    //a corrigir quando existirem gavetas (ou nao)
}
function afterInit() {
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);
    animate();
}
function animate() {
    window.requestAnimationFrame(() => animate());
    renderer.render(scene, camera);
}
function createProduct(order) {
    let product = new product_1.Product(order.height, order.width, order.depth, order.category, order.material, order.children);
    return product;
}
window.addEventListener('resize', function () {
    renderer.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
});
window.addEventListener("load", init);
