import { Product } from '../model/product';
import express from 'express';


const router = express.Router()

var product: Product

router.get('/', (req, res, next) => {
    let body = `
<!DOCTYPE html>
<html lang="en">

<head>
    <title>SGRAI - Armario</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
</head>

<body style="text-align:center;" >
    <h1>Visualizar encomenda 3D</h1>

    <script>window.product = ${JSON.stringify(product)}</script>
    <script> var exports = {}; </script>
    <script type="text/javascript" src="/bundle.js"></script>
</body>

</html>
`;
    res.header("Content-Type", "text/html").send(body)
})

router.post('/', (req, res, next) => {
    product = new Product(
        req.body.item.height,
        req.body.item.width,
        req.body.item.depth,
        req.body.item.category,
        req.body.item.material,
        req.body.item.children
    )

    res.status(200).send(JSON.stringify(req.body.item));
})



module.exports = router;