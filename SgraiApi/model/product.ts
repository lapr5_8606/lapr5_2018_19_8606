export class Product {

    height: number;
    width: number;
    depth: number;
    category: string;
    material: string;
    children: object[];
    heightLeft: number;
    widthLeft: number;

    constructor(altura:number, largura:number, profundidade:number, category:string ,material:string, children:object[]){
        this.height = altura;
        this.width = largura;
        this.depth = profundidade;
        this.category = category;
        this.material = material;
        this.children = children
        this.heightLeft = altura;
        this.widthLeft = largura;
    }
    
}