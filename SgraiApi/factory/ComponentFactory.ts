
import { Product3D, Drawer, Door, NullProduct, Modulex, InsideModule, ModulexDoor, InsideModule2Door } from '../factory/Product3D'




export class ComponentFactory {

    getComponent(componentType: string, materialType: string): Product3D {
        if (componentType == null) {
            return new NullProduct();
        }

        if (componentType === 'drawer') {
            return new Drawer(materialType);
        }

        else if (componentType === 'moduleDoor') {
            return new ModulexDoor(materialType);
        }

        else if (componentType === 'module') {
            return new Modulex(materialType);
        }
        else if (componentType === 'insideModule') {
            return new InsideModule(materialType);
        }
        else if (componentType === 'insideModule2Door') {
            return new InsideModule2Door(materialType);
        }

        return new NullProduct();
    }
}