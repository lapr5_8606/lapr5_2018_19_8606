var express = require('express');
var router = express.Router();
var admin = require('firebase-admin');

router.get('/roles', (request, response, next) => {
    admin.auth().verifyIdToken(request.headers.authorization)
        .then((idToken) => {
            admin.firestore()
                .collection('users')
                .doc(idToken.uid)
                .get()
                .then((userData) => {
                    response.status(200).json(userData.data().roles)
                })
                .catch((error) => {
                    response.status(400).json({ error })
                })
        })
        .catch((error) => {
            response.status(400).json({ error })
        })
});

router.get('/info', (request, response, next) => {
    admin.auth().verifyIdToken(request.headers.authorization)
        .then((idToken) => {
            admin.firestore()
                .collection('users')
                .doc(idToken.uid)
                .get()
                .then((userData) => {
                    response.status(200).json(userData.data())
                })
                .catch((error) => {
                    response.status(400).json({ error })
                })
        })
        .catch((error) => {
            response.status(400).json({ error })
        })
});

module.exports = router;