var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');

var email;
var code;

function generateCode(length) {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';

    for (var i = length; i > 0; i--) {
        result += chars[Math.round(Math.random() * (chars.length - 1))];
    };

    return result;
};

router.post('/', (request, response, next) => {
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'pedro.miller.pinho@gmail.com',
            pass: 'Pg100698'
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    email = request.body.email;
    code = generateCode(5);

    const mailOptions = {
        from: 'Pedro Pinho LAPR5 <pedro.miller.pinho@gmail.com>', // sender address
        to: email, // list of receivers
        subject: 'LAPR5 - Your 2-factor Authentication code', // Subject line
        html: '<p>Please enter this code in the app, so we can verify it is really you before logging you in:&nbsp;</p><p><strong>' + code + '</strong></p>'
    };

    transporter.sendMail(mailOptions, function (err, info) {
        if (err)
            console.log(err)
        else
            console.log(info);
    });

    response.status(201).json({
        message: "ok",
        code: code
    });
});

module.exports = router;