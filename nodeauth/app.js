var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var admin = require("firebase-admin");
var morgan = require('morgan');
var fs = require('fs');
var path = require('path')


// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })

// setup the logger
app.use(morgan('combined', { stream: accessLogStream }))


var serviceAccount = require("./secrets.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://lapr5-8606.firebaseio.com"
});
admin.firestore().settings({ timestampsInSnapshots: true })

var authRoutes = require('./routes/auth');
var twofactorRoutes = require('./routes/twofactor');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((request, response, next) => {
	response.header('Access-Control-Allow-Origin', '*');
	response.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
	response.header('Access-Control-Allow-Headers', '*');
	next();
});

app.use('/auth', authRoutes);
app.use('/twofactor', twofactorRoutes);

//if there is no request or response, status is 404
app.use((request, response, next) => {
	const error = new Error('404 not found');
	error.status = 404;
	next(error);
});

//detecting error 500
app.use((error, request, response, next) => {
	response.status(error.status || 500);
	response.json({
		error: {
			message: error.message
		}
	});
});

module.exports = app;