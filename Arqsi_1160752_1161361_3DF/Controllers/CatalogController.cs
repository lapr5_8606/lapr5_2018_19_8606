using Arqsi_1160752_1161361_3DF.Data.Repositories;
using Arqsi_1160752_1161361_3DF.Models;
using Arqsi_1160752_1161361_3DF.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Configuration;

namespace Arqsi_1160752_1161361_3DF.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class CatalogController : ControllerBase
    {
        readonly IConfiguration _configuration;
        private static readonly HttpClient client = new HttpClient();
        private const string CreateWithSuccess = "Catalog created with success.";
        private const string CatalogWithNameDoenstExists = "Catalog não existe";
        private const string CatalogAlreadyExists = "Catalog already exists.";
        private const string CatalogWithIDNotFound = "Catalog with specified ID not found.";

        private readonly ICatalogRepository _catalogRepository;
        private readonly IProductRepository _productRepository;
        private readonly IProductCatalogRelationshipRepository _productCatalogRepository;

        public CatalogController(ICatalogRepository catalogRepository,
                                 IProductRepository productRepository,
                                 IProductCatalogRelationshipRepository productCatalogRepository,
                                 IConfiguration configuration)
        {
            _configuration = configuration;
            _catalogRepository = catalogRepository;
            _productRepository = productRepository;
            _productCatalogRepository = productCatalogRepository;
        }

        [HttpGet("names/{name}")]
        public async Task<IActionResult> FindCatalogByName(string name, [FromHeader]string authorization)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authorization);
           
            var responseString = await client.GetStringAsync( _configuration.GetSection("Config").GetValue<string>("AuthorizationRolesURL"));
            if (responseString == "[\"manager\"]" || responseString == "[\"client\"]")
            {
                Catalog catalog = await _catalogRepository.FindByName(name);

                if (catalog == null)
                {
                    return StatusCode(404, new ErrorDto { ErrorMessage = CatalogWithNameDoenstExists });
                }

                CatalogDto catalogDto = await CreateCatalogDto(catalog);

                return StatusCode(200, catalogDto);
            }
            else
            {
                return StatusCode(403, "Not Allowed");
            }
        }

        [HttpGet("withnames/{name}")]
        public async Task<IActionResult> FindCatalogWithNameByName(string name, [FromHeader]string authorization)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authorization);
            var responseString = await client.GetStringAsync( _configuration.GetSection("Config").GetValue<string>("AuthorizationRolesURL"));
            if (responseString == "[\"manager\"]" || responseString == "[\"client\"]")
            {
                Catalog catalog = await _catalogRepository.FindByName(name);

                if (catalog == null)
                {
                    return StatusCode(404, new ErrorDto { ErrorMessage = CatalogWithNameDoenstExists });
                }

                CatalogWithNamesDto catalogDto = await CreateCatalogWithNamesDto(catalog);

                return StatusCode(200, catalogDto);
            }
            else
            {
                return StatusCode(403, "Not Allowed");
            }
        }

        [HttpGet("all/")]
        public async Task<IActionResult> GetAllCatalogs([FromHeader]string authorization)
        {

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authorization);
            var responseString = await client.GetStringAsync( _configuration.GetSection("Config").GetValue<string>("AuthorizationRolesURL"));
            if (responseString == "[\"manager\"]" || responseString == "[\"client\"]")
            {
                ICollection<Catalog> list = await _catalogRepository.GetAllCatalogs();

                GetAllCatalogsDto final = new GetAllCatalogsDto();
                final.catalogs = new List<CatalogDto>();
                foreach (Catalog catalog in list)
                {
                    CatalogDto catalogDto = await CreateCatalogDto(catalog);
                    final.catalogs.Add(catalogDto);
                }
                return Ok(final);
            }
            else
            {
                return StatusCode(403, "Not Allowed");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateNewCatalog(CatalogDto newCatalogDto, [FromHeader]string authorization)
        {

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authorization);
            var responseString = await client.GetStringAsync( _configuration.GetSection("Config").GetValue<string>("AuthorizationRolesURL"));
            if (responseString == "[\"manager\"]")
            {
                //Trys to fetch a catalog with the same name passed in the DTO
                Catalog catalog = await _catalogRepository.FindByName(newCatalogDto.CatalogName);

                ICollection<Product> prods = new List<Product>();

                //If the catalog was found return a HTTP 400 BadRequest
                if (catalog != null)
                {
                    return StatusCode(409, new ErrorDto { ErrorMessage = CatalogAlreadyExists });
                }


                //Create the new Catalog
                catalog = new Catalog();
                catalog.Name = newCatalogDto.CatalogName;

                catalog = await _catalogRepository.NewCatalog(catalog);

                foreach (int ii in newCatalogDto.ProductIDs)
                {
                Product p = await _productRepository.FindProductById(ii);

                ProductCatalogRelationship productCatalogRelationship = new ProductCatalogRelationship
                {
                    Catalog = catalog,
                    Product = p
                };

                 await _productCatalogRepository.NewProductCatalogRelationship(productCatalogRelationship);

            }
            await _productCatalogRepository.SaveChanges();

                CatalogDto catalogDto = await CreateCatalogDto(catalog);

                return CreatedAtRoute("GetCatalog", new { id = catalog.ID }, catalogDto);
            }
            else
            {
                return StatusCode(403, "Not Allowed");
            }
        }

        [HttpGet("{id}", Name = "GetCatalog")]
        public async Task<IActionResult> FindCatalogByID(int id)
        {
            Catalog catalog = await _catalogRepository.FindById(id);

            if (catalog == null)
            {
                return NotFound(new ErrorDto { ErrorMessage = CatalogWithIDNotFound });
            }

            CatalogDto catalogDto = await CreateCatalogDto(catalog);

            return StatusCode(200, catalogDto);
        }

        /* 

        [HttpPut]
        public async Task<IActionResult> UpdateCatalog(CatalogDto changeCatalogDto)
        {
            Catalog catalog = await _catalogRepository.FindById(changeCatalogDto.CatalogID);

            ICollection<Product> prods = new List<Product>();

            if (catalog == null)
            {
                return NotFound(new ErrorDto { ErrorMessage = CatalogWithIDNotFound });
            }


            catalog.Name = changeCatalogDto.CatalogName;

            foreach (int i in changeCatalogDto.ProductIDs)
            {
                Product p = await _productRepository.FindProductById(i);
                if (p != null)
                {
                    prods.Add(p);
                }
            }

            catalog.Products = prods;

            catalog = await _catalogRepository.UpdateCatalog(catalog);


            CatalogDto returnCatalog = await CreateCatalogDto(catalog);
            return Ok(returnCatalog);
        } */

        [HttpDelete("names/{name}")]
        public async Task<IActionResult> DeleteCatalogWithName(string name, [FromHeader]string authorization)
        {

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authorization);
            var responseString = await client.GetStringAsync( _configuration.GetSection("Config").GetValue<string>("AuthorizationRolesURL"));
            if (responseString == "[\"manager\"]")
            {
                Catalog catalog = await _catalogRepository.FindByName(name);

                if (catalog == null)
                {
                    return NotFound(new ErrorDto { ErrorMessage = CatalogWithIDNotFound });
                }


                List<ProductCatalogRelationship> lista = await _productCatalogRepository.FindRelationshipsOfCatalog(catalog.ID);

            foreach (ProductCatalogRelationship relat in lista)
            {
                _productCatalogRepository.DeleteWithoutSave(relat);
            }
            await _productCatalogRepository.SaveChanges();

                await _catalogRepository.DeleteCatalog(catalog);

                return StatusCode(200, "Apagado com sucesso");
            }
            else
            {
                return StatusCode(403, "Not Allowed");
            }
        }




        /*
            Creates and returns a Catalog Dto based on the catalog passed as parameter
         */
        private async Task<CatalogDto> CreateCatalogDto(Catalog catalog)
        {
            CatalogDto catalogDto;
            catalogDto = new CatalogDto
            {
                CatalogID = catalog.ID,
                CatalogName = catalog.Name,
                ProductIDs = new List<int>()
            };

            List<ProductCatalogRelationship> prodCat = await _productCatalogRepository.FindRelationshipsOfCatalog(catalog.ID);
            if(prodCat != null){
                foreach(ProductCatalogRelationship relat in prodCat){
                    catalogDto.ProductIDs.Add(relat.Product.ID);
                }
            }

            return catalogDto;
        }


        private async Task<CatalogWithNamesDto> CreateCatalogWithNamesDto(Catalog catalog)
        {
            CatalogWithNamesDto catalogDto;
            catalogDto = new CatalogWithNamesDto
            {
                CatalogID = catalog.ID,
                CatalogName = catalog.Name,
                ProductNames = new List<string>()
            };

           List<ProductCatalogRelationship> prodCat = await _productCatalogRepository.FindRelationshipsOfCatalog(catalog.ID);
            if(prodCat != null){
                foreach(ProductCatalogRelationship relat in prodCat){
                    catalogDto.ProductNames.Add(relat.Product.Name);
                }
            }

            return catalogDto;
        }
    }
}