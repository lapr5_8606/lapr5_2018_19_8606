using Arqsi_1160752_1161361_3DF.Data.Repositories;
using Arqsi_1160752_1161361_3DF.Models;
using Arqsi_1160752_1161361_3DF.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;

namespace Arqsi_1160752_1161361_3DF.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionController : ControllerBase
    {
        private const string CreateWithSuccess = "Collection created with success.";
        private const string CollectionWithNameDoenstExists = "Collection doesn't exist";
        private const string CollectionAlreadyExists = "Collection already exists.";
        private const string CollectionWithIDNotFound = "Collection with specified ID not found.";

        private readonly ICollectionRepository _collectionRepository;
        private readonly IProductRepository _productRepository;
        private readonly IProductAndCollectionRepository _productAndCollectionRepository;


        public CollectionController(ICollectionRepository collectionRepository,
                                 IProductRepository productRepository, IProductAndCollectionRepository productAndCollectionRepository)
        {
            _collectionRepository = collectionRepository;
            _productRepository = productRepository;
            _productAndCollectionRepository = productAndCollectionRepository;
        }

        //comentario
        [HttpGet("names/{name}")]
        public async Task<IActionResult> FindCollectionByName(string name)
        {
            Collection collection = await _collectionRepository.FindByName(name);

            if (collection == null)
            {
                return StatusCode(404, new ErrorDto { ErrorMessage = CollectionWithNameDoenstExists });
            }

            CollectionDto collectionDto = await CreateCollectionDto(collection);

            return StatusCode(200, collectionDto);
        }


        [HttpGet("all/")]
        public async Task<IActionResult> GetAllCollections()
        {
            ICollection<Collection> list = await _collectionRepository.GetAllCollections();

            GetAllCollectionsDto final = new GetAllCollectionsDto();
            final.collections = new List<CollectionDto>();
            foreach (Collection collection in list)
            {
                CollectionDto collectionDto = await CreateCollectionDto(collection);
                final.collections.Add(collectionDto);
            }
            return Ok(final);
        }

        [HttpPost]
        public async Task<IActionResult> CreateNewCollection(CollectionDto newCollectionDto)
        {
            //Trys to fetch a collection with the same name passed in the DTO
            Collection collection = await _collectionRepository.FindByName(newCollectionDto.CollectionName);

            // ICollection<ProductCollectionRelationship> prods = new List<ProductCollectionRelationship>();

            //If the collection was found return a HTTP 400 BadRequest
            if (collection != null)
            {
                return StatusCode(409, new ErrorDto { ErrorMessage = CollectionAlreadyExists });
            }

            collection = new Collection();
            //Create the new Collection
            collection.Name = newCollectionDto.CollectionName;
            collection.AestheticLine = newCollectionDto.AestheticLine;


            //Save the new collection and returns the database instance of the collection
            collection = await _collectionRepository.NewCollection(collection);

            foreach (string name in newCollectionDto.ProductsNameList)
            {
                Product p = await _productRepository.FindProductByName(name);

                ProductCollectionRelationship productCollectionRelationship = new ProductCollectionRelationship
                {
                    Collection = collection,
                    Product = p
                };

                await _productAndCollectionRepository.NewProductAndCollectionRelationship(productCollectionRelationship);

            }
            await _productAndCollectionRepository.SaveChanges();




            CollectionDto collectionDto = await CreateCollectionDto(collection);

            return CreatedAtRoute("GetCollection", new { id = collection.ID }, collectionDto);
        }

        [HttpGet("{id}", Name = "GetCollection")]
        public async Task<IActionResult> FindCollectionByID(int id)
        {
            Collection collection = await _collectionRepository.FindById(id);

            if (collection == null)
            {
                return NotFound(new ErrorDto { ErrorMessage = CollectionWithIDNotFound });
            }

            CollectionDto collectionDto = await CreateCollectionDto(collection);

            return StatusCode(200, collectionDto);
        }

        // [HttpPut]
        // public async Task<IActionResult> UpdateCollection(CollectionDto changeCollectionDto)
        // {
        //     Collection collection = await _collectionRepository.FindById(changeCollectionDto.CollectionID);

        //     ICollection<Product> prods = new List<Product>();

        //     if (collection == null)
        //     {
        //         return NotFound(new ErrorDto { ErrorMessage = CollectionWithIDNotFound });
        //     }


        //     collection.Name = changeCollectionDto.CollectionName;

        //     foreach (string i in changeCollectionDto.ProductsNameList)
        //     {
        //         Product p = await _productRepository.FindProductByName(i);
        //         if (p != null)
        //         {
        //             prods.Add(p);
        //         }
        //     }

        //     collection.ProductList = prods;

        //     collection = await _collectionRepository.UpdateCollection(collection);


        //     CollectionDto returnCollection = await CreateCollectionDto(collection);
        //     return Ok(returnCollection);
        // }

        [HttpDelete("names/{name}")]
        public async Task<IActionResult> DeleteCollectionWithName(string name)
        {
            Collection collection = await _collectionRepository.FindByName(name);

            if (collection == null)
            {
                return NotFound(new ErrorDto { ErrorMessage = CollectionWithNameDoenstExists });
            }

            List<ProductCollectionRelationship> lista = await _productAndCollectionRepository.FindRelationshipsOfCollection(collection.ID);

            foreach (ProductCollectionRelationship relat in lista)
            {
                _productAndCollectionRepository.DeleteWithoutSave(relat);
            }
            await _productAndCollectionRepository.SaveChanges();


            await _collectionRepository.DeleteCollection(collection);

            return Ok();
        }

        /*
            Creates and returns a Collection Dto based on the collection passed as parameter
         */
        private async Task<CollectionDto> CreateCollectionDto(Collection collection)
        {

            CollectionDto collectionDto = new CollectionDto
            {
                CollectionID = collection.ID,
                CollectionName = collection.Name,
                AestheticLine = collection.AestheticLine,
                ProductsNameList = new List<string>()
            };

            List<ProductCollectionRelationship> productCollectionRelationships = await _productAndCollectionRepository.FindRelationshipsOfCollection(collection.ID);
            if (productCollectionRelationships != null)
            {
                foreach (ProductCollectionRelationship relat in productCollectionRelationships)
                {
                    collectionDto.ProductsNameList.Add(relat.Product.Name);
                }
            }

            return collectionDto;
        }
    }
}