using System.Collections.Generic;
using System.Threading.Tasks;
using Arqsi_1160752_1161361_3DF.Models;

namespace Arqsi_1160752_1161361_3DF.Data.Repositories
{
    public interface IProductAndCollectionRepository
    {
        Task<ProductCollectionRelationship> NewProductAndCollectionRelationship(ProductCollectionRelationship productCollectionRelationship);
        Task<List<ProductCollectionRelationship>> FindRelationshipsOfProduct(int productID);
        Task<List<ProductCollectionRelationship>> FindRelationshipsOfCollection(int collectionID);

        Task SaveChanges();
        void DeleteWithoutSave(ProductCollectionRelationship productCollectionRelationship);
    }
}