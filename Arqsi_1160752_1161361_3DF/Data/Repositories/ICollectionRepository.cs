using System.Threading.Tasks;
using Arqsi_1160752_1161361_3DF.Models;
using System.Collections.Generic;

namespace Arqsi_1160752_1161361_3DF.Data.Repositories
{
    public interface ICollectionRepository
    {
        Task<Collection> FindById(int id);
        Task<Collection> FindByName(string collectionName);
        Task<ICollection<Collection>> GetAllCollections();

        Task<Collection> NewCollection(Collection newCollection);
        Task<Collection> UpdateCollection(Collection collection);
        Task<Collection> DeleteCollection(Collection collection);

    }
}