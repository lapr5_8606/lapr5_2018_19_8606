using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;
using Arqsi_1160752_1161361_3DF.Models;
using Microsoft.EntityFrameworkCore;

namespace Arqsi_1160752_1161361_3DF.Data.Repositories
{
    public class ProductAndCollectionRepository : IProductAndCollectionRepository
    {
        private readonly ClosetContext _context;

        public ProductAndCollectionRepository(ClosetContext context)
        {
            _context = context;
        }

        public async Task<ProductCollectionRelationship> NewProductAndCollectionRelationship(ProductCollectionRelationship productCollectionRelationship)
        {

            Console.WriteLine("CHEGOU AQUI CRL? : " + productCollectionRelationship.Id);
            await _context.ProductCollectionRelationships.AddAsync(productCollectionRelationship);

            return productCollectionRelationship;
        }


        /*
            Returns the relationships associated to the product with the specified ID.
        */
        public async Task<List<ProductCollectionRelationship>> FindRelationshipsOfProduct(int productID)
        {
            return await _context.ProductCollectionRelationships.Include(c => c.Collection).Where(i => i.ProductId == productID).ToListAsync();
        }

        public async Task<List<ProductCollectionRelationship>> FindRelationshipsOfCollection(int collectionID)
        {
            return await _context.ProductCollectionRelationships.Include(p => p.Product).Where(i => i.CollectionId == collectionID).ToListAsync();
        }

        public void DeleteWithoutSave(ProductCollectionRelationship productCollectionRelationship)
        {
            _context.ProductCollectionRelationships.Remove(productCollectionRelationship);
        }

        public async Task SaveChanges()
        {
            await _context.SaveChangesAsync();

            return;
        }
    }
}