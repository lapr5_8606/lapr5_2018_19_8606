using System.Threading.Tasks;
using Arqsi_1160752_1161361_3DF.Models;
using System.Collections.Generic;

namespace Arqsi_1160752_1161361_3DF.Data.Repositories
{
    public interface ICatalogRepository
    {
        Task<Catalog> FindById(int id);
        Task<Catalog> FindByName(string name);
        Task<ICollection<Catalog>> GetAllCatalogs();
        Task<Catalog> NewCatalog(Catalog newCatalog);
        Task<Catalog> UpdateCatalog(Catalog catalog);
        Task<Catalog> DeleteCatalog(Catalog catalog);
    }
}