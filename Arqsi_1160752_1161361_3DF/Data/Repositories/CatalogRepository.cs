using System.Threading.Tasks;
using Arqsi_1160752_1161361_3DF.Exceptions;
using Arqsi_1160752_1161361_3DF.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Arqsi_1160752_1161361_3DF.Data.Repositories
{
    public class CatalogRepository : ICatalogRepository
    {
        private readonly ClosetContext _context;

        public CatalogRepository(ClosetContext context)
        {
            _context = context;
        }

        /*
            Searchs for a catalog with the given id
        */
        public async Task<Catalog> FindById(int id)
        {
            var catalog = await _context.Catalogs.Include(c => c.Name).FirstOrDefaultAsync(i => i.ID == id);

            return catalog;
        }

        /*
            Searchs for a catalog with the given name
            Name - Name to look for
        */
        public async Task<Catalog> FindByName(string name)
        {
            var catalog = await _context.Catalogs.FirstOrDefaultAsync(x => x.Name == name);

            return catalog;
        }

        public async Task<ICollection<Catalog>> GetAllCatalogs()
        {
            var catalogs = await _context.Catalogs.ToListAsync();
            return catalogs;
        }

        /*
            Adds a new catalog to the database context
        */
        public async Task<Catalog> NewCatalog(Catalog newCatalog)
        {
            await _context.Catalogs.AddAsync(newCatalog);
            await _context.SaveChangesAsync();

            return newCatalog;
        }

        public async Task<Catalog> DeleteCatalog(Catalog catalog)
        {
            _context.Catalogs.Remove(catalog);
            await _context.SaveChangesAsync();

            return catalog;
        }

        /*
            Updates a given catalog
        */
        public async Task<Catalog> UpdateCatalog(Catalog catalog)
        {
            _context.Update(catalog);
            await _context.SaveChangesAsync();

            return catalog;
        }
    }
}