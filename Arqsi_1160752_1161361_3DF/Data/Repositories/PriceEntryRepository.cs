using System.Collections.Generic;
using System.Threading.Tasks;
using Arqsi_1160752_1161361_3DF.Models;
using Microsoft.EntityFrameworkCore;

namespace Arqsi_1160752_1161361_3DF.Data.Repositories
{
    public class PriceEntryRepository
    {
        private readonly ClosetContext _context;

        public PriceEntryRepository(ClosetContext context)
        {
            _context = context;
        }

        /*
            Deletes a given PriceEntry.
         */
        public async Task DeletePriceEntry(PriceEntry PriceEntry)
        {
            var PriceEntryVar = _context.PriceEntries.Remove(PriceEntry);

            await _context.SaveChangesAsync();
        }

        /*
            Searchs for a PriceEntry with the given ID.
        */
        public async Task<PriceEntry> FindById(int id)
        {
            var PriceEntry = await _context.PriceEntries.FirstOrDefaultAsync(x => x.ID == id);

            return PriceEntry;
        }

        /*
            Saves the new PriceEntry to the context database.
        */
        public async Task<PriceEntry> NewPriceEntry(PriceEntry PriceEntry)
        {
            await _context.PriceEntries.AddAsync(PriceEntry);
            await _context.SaveChangesAsync();

            return PriceEntry;
        }
    }
}