using System.Threading.Tasks;
using Arqsi_1160752_1161361_3DF.Exceptions;
using Arqsi_1160752_1161361_3DF.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Arqsi_1160752_1161361_3DF.Data.Repositories
{
    public class CollectionRepository : ICollectionRepository
    {
        private readonly ClosetContext _context;

        public CollectionRepository(ClosetContext context)
        {
            _context = context;
        }

        /*
            Searchs for a collection with the given id
        */
        public async Task<Collection> FindById(int id)
        {
            var collection = await _context.Collections.Include(c => c.Name).Include(a => a.AestheticLine).FirstOrDefaultAsync(i => i.ID == id);

            return collection;
        }

        /*
            Searchs for a collection with the given name
            Name - Name to look for
        */
        public async Task<Collection> FindByName(string name)
        {
            var collection = await _context.Collections.FirstOrDefaultAsync(x => x.Name == name);

            return collection;
        }


        public async Task<ICollection<Collection>> GetAllCollections()
        {
            var collections = await _context.Collections.ToListAsync();
            return collections;
        }

        /*
            Adds a new collection to the database contex
        */
        public async Task<Collection> NewCollection(Collection newCollection)
        {
            await _context.Collections.AddAsync(newCollection);
            await _context.SaveChangesAsync();

            return newCollection;
        }

        public async Task<Collection> DeleteCollection(Collection collection)
        {
            _context.Collections.Remove(collection);
            await _context.SaveChangesAsync();

            return collection;
        }

        /*
            Updates a given collection
        */
        public async Task<Collection> UpdateCollection(Collection collection)
        {
            _context.Update(collection);

            await _context.SaveChangesAsync();

            return collection;
        }


    }
}