using System.Collections.Generic;
using System.Threading.Tasks;
using Arqsi_1160752_1161361_3DF.Models;

namespace Arqsi_1160752_1161361_3DF.Data.Repositories
{
    public interface IProductCatalogRelationshipRepository
    {
        Task<ProductCatalogRelationship> NewProductCatalogRelationship(ProductCatalogRelationship productCatalogRelationship);
        Task<List<ProductCatalogRelationship>> FindRelationshipsOfProduct(int productID);
        Task<List<ProductCatalogRelationship>> FindRelationshipsOfCatalog(int catalogID);

        Task SaveChanges();
        void DeleteWithoutSave(ProductCatalogRelationship productCatalogRelationship);
    }
}