using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;
using Arqsi_1160752_1161361_3DF.Models;
using Microsoft.EntityFrameworkCore;

namespace Arqsi_1160752_1161361_3DF.Data.Repositories
{
    public class ProductCatalogRelationshipRepository : IProductCatalogRelationshipRepository
    {
        private readonly ClosetContext _context;

        public ProductCatalogRelationshipRepository(ClosetContext context)
        {
            _context = context;
        }

        public async Task<ProductCatalogRelationship> NewProductCatalogRelationship(ProductCatalogRelationship productCatalogRelationship)
        {

            Console.WriteLine("CHEGOU AQUI ? : " + productCatalogRelationship.Id);
            await _context.ProductCatalogRelationships.AddAsync(productCatalogRelationship);

            return productCatalogRelationship;
        }


        /*
            Returns the relationships associated to the product with the specified ID.
        */
        public async Task<List<ProductCatalogRelationship>> FindRelationshipsOfProduct(int productID)
        {
            return await _context.ProductCatalogRelationships.Include(c => c.Catalog).Where(i => i.ProductId == productID).ToListAsync();
        }

        public async Task<List<ProductCatalogRelationship>> FindRelationshipsOfCatalog(int catalogID)
        {
            return await _context.ProductCatalogRelationships.Include(p => p.Product).Where(i => i.CatalogId == catalogID).ToListAsync();
        }

        public void DeleteWithoutSave(ProductCatalogRelationship productCatalogRelationship)
        {
            _context.ProductCatalogRelationships.Remove(productCatalogRelationship);
        }

        public async Task SaveChanges()
        {
            await _context.SaveChangesAsync();

            return;
        }
    }
}