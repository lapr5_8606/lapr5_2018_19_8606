using System.Collections.Generic;

namespace Arqsi_1160752_1161361_3DF.Dtos
{
    public class GetAllProductDto
    {
        public List<GetProductWithNamesDto> produtos { get; set; }
    }
}