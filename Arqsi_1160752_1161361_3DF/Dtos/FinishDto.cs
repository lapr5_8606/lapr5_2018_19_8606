using System.ComponentModel.DataAnnotations;

namespace Arqsi_1160752_1161361_3DF.Dtos
{
    public class FinishDto
    {
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        
        public double PriceIncrement { get; set; }
    }
}