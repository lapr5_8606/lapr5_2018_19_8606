using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Arqsi_1160752_1161361_3DF.Models;

namespace Arqsi_1160752_1161361_3DF.Dtos
{
    public class MaterialDto
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        
        [Required]
        public double Price { get; set; }

        [Required]
        public ICollection<FinishDto> AvailableFinishes { get; set; }
        
        [Required]
        public ICollection<PriceEntry> PriceHistory { get; set; }
    }
}