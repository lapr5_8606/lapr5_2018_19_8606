using System.Collections.Generic;

namespace Arqsi_1160752_1161361_3DF.Dtos
{
    public class GetAllMaterialDto
    {
        public List<MaterialDto> materiais { get; set; }
    }
}