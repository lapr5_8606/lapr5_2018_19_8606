using System.Collections.Generic;

namespace Arqsi_1160752_1161361_3DF.Dtos
{
    public class GetAllCollectionsDto
    {
        public List<CollectionDto> collections { get; set; }
    }
}