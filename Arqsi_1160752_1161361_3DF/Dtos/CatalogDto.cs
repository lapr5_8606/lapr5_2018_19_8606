using System.Collections.Generic;

namespace Arqsi_1160752_1161361_3DF.Dtos
{
    public class CatalogDto
    {
        public int CatalogID { get; set; }

        public string CatalogName { get; set; }

        public ICollection<int> ProductIDs { get; set; }
    }
}