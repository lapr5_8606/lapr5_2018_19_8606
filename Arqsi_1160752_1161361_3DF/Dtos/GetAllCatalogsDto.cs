using System.Collections.Generic;

namespace Arqsi_1160752_1161361_3DF.Dtos
{
    public class GetAllCatalogsDto
    {
        public List<CatalogDto> catalogs { get; set; }
    }
}