using System.Collections.Generic;

namespace Arqsi_1160752_1161361_3DF.Dtos
{
    public class CatalogWithNamesDto
    {
        public int CatalogID { get; set; }

        public string CatalogName { get; set; }

        public ICollection<string> ProductNames { get; set; }
    }
}