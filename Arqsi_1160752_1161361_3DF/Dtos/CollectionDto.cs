using System.Collections.Generic;

namespace Arqsi_1160752_1161361_3DF.Dtos
{
    public class CollectionDto
    {
        public int CollectionID { get; set; }

        public string CollectionName { get; set; }

        public string AestheticLine { get; set; }

        public ICollection<string> ProductsNameList { get; set; }
    }
}