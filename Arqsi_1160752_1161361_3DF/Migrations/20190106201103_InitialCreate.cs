﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Arqsi_1160752_1161361_3DF.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Catalogs",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Catalogs", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    ParentCategoryID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Categories_Categories_ParentCategoryID",
                        column: x => x.ParentCategoryID,
                        principalTable: "Categories",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Collections",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    AestheticLine = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Collections", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Materials",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MaterialName = table.Column<string>(nullable: true),
                    Price = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Materials", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "PossibleValues",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discriminator = table.Column<string>(nullable: false),
                    MinValue = table.Column<float>(nullable: true),
                    MaxValue = table.Column<float>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PossibleValues", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Finishes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FinishName = table.Column<string>(nullable: true),
                    PriceIncrement = table.Column<double>(nullable: false),
                    MaterialID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Finishes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Finishes_Materials_MaterialID",
                        column: x => x.MaterialID,
                        principalTable: "Materials",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PriceEntries",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChangedOn = table.Column<string>(nullable: true),
                    Value = table.Column<double>(nullable: false),
                    MaterialID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceEntries", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PriceEntries_Materials_MaterialID",
                        column: x => x.MaterialID,
                        principalTable: "Materials",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Float",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FloatValue = table.Column<float>(nullable: false),
                    DiscretePossibleValuesID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Float", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Float_PossibleValues_DiscretePossibleValuesID",
                        column: x => x.DiscretePossibleValuesID,
                        principalTable: "PossibleValues",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PossibleDimensions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HeightPossibleValuesID = table.Column<int>(nullable: false),
                    WidthPossibleValuesID = table.Column<int>(nullable: false),
                    DepthPossibleValuesID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PossibleDimensions", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PossibleDimensions_PossibleValues_DepthPossibleValuesID",
                        column: x => x.DepthPossibleValuesID,
                        principalTable: "PossibleValues",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PossibleDimensions_PossibleValues_HeightPossibleValuesID",
                        column: x => x.HeightPossibleValuesID,
                        principalTable: "PossibleValues",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PossibleDimensions_PossibleValues_WidthPossibleValuesID",
                        column: x => x.WidthPossibleValuesID,
                        principalTable: "PossibleValues",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<float>(nullable: false),
                    ProductCategoryID = table.Column<int>(nullable: false),
                    PossibleDimensionsID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Products_PossibleDimensions_PossibleDimensionsID",
                        column: x => x.PossibleDimensionsID,
                        principalTable: "PossibleDimensions",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_Categories_ProductCategoryID",
                        column: x => x.ProductCategoryID,
                        principalTable: "Categories",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductCatalogRelationships",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    CatalogId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCatalogRelationships", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductCatalogRelationships_Catalogs_CatalogId",
                        column: x => x.CatalogId,
                        principalTable: "Catalogs",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductCatalogRelationships_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductCollectionRelationships",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    CollectionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCollectionRelationships", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductCollectionRelationships_Collections_CollectionId",
                        column: x => x.CollectionId,
                        principalTable: "Collections",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductCollectionRelationships_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductMaterialRelationships",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    MaterialId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductMaterialRelationships", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductMaterialRelationships_Materials_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Materials",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductMaterialRelationships_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductRelationships",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ParentProductID = table.Column<int>(nullable: false),
                    ChildProductID = table.Column<int>(nullable: false),
                    IsMandatory = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductRelationships", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ProductRelationships_Products_ChildProductID",
                        column: x => x.ChildProductID,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductRelationships_Products_ParentProductID",
                        column: x => x.ParentProductID,
                        principalTable: "Products",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Restriction",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discriminator = table.Column<string>(nullable: false),
                    ProductRelationshipID = table.Column<int>(nullable: true),
                    MinHeightValue = table.Column<float>(nullable: true),
                    MaxHeightValue = table.Column<float>(nullable: true),
                    MinWidthValue = table.Column<float>(nullable: true),
                    MaxWidthValue = table.Column<float>(nullable: true),
                    MinDepthValue = table.Column<float>(nullable: true),
                    MaxDepthValue = table.Column<float>(nullable: true),
                    MinHeightPercentage = table.Column<float>(nullable: true),
                    MaxHeightPercentage = table.Column<float>(nullable: true),
                    MinWidthPercentage = table.Column<float>(nullable: true),
                    MaxWidthPercentage = table.Column<float>(nullable: true),
                    MinDepthPercentage = table.Column<float>(nullable: true),
                    MaxDepthPercentage = table.Column<float>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Restriction", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Restriction_ProductRelationships_ProductRelationshipID",
                        column: x => x.ProductRelationshipID,
                        principalTable: "ProductRelationships",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Categories_ParentCategoryID",
                table: "Categories",
                column: "ParentCategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_Finishes_MaterialID",
                table: "Finishes",
                column: "MaterialID");

            migrationBuilder.CreateIndex(
                name: "IX_Float_DiscretePossibleValuesID",
                table: "Float",
                column: "DiscretePossibleValuesID");

            migrationBuilder.CreateIndex(
                name: "IX_PossibleDimensions_DepthPossibleValuesID",
                table: "PossibleDimensions",
                column: "DepthPossibleValuesID");

            migrationBuilder.CreateIndex(
                name: "IX_PossibleDimensions_HeightPossibleValuesID",
                table: "PossibleDimensions",
                column: "HeightPossibleValuesID");

            migrationBuilder.CreateIndex(
                name: "IX_PossibleDimensions_WidthPossibleValuesID",
                table: "PossibleDimensions",
                column: "WidthPossibleValuesID");

            migrationBuilder.CreateIndex(
                name: "IX_PriceEntries_MaterialID",
                table: "PriceEntries",
                column: "MaterialID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCatalogRelationships_CatalogId",
                table: "ProductCatalogRelationships",
                column: "CatalogId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCatalogRelationships_ProductId",
                table: "ProductCatalogRelationships",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCollectionRelationships_CollectionId",
                table: "ProductCollectionRelationships",
                column: "CollectionId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCollectionRelationships_ProductId",
                table: "ProductCollectionRelationships",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductMaterialRelationships_MaterialId",
                table: "ProductMaterialRelationships",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductMaterialRelationships_ProductId",
                table: "ProductMaterialRelationships",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductRelationships_ChildProductID",
                table: "ProductRelationships",
                column: "ChildProductID");

            migrationBuilder.CreateIndex(
                name: "IX_ProductRelationships_ParentProductID",
                table: "ProductRelationships",
                column: "ParentProductID");

            migrationBuilder.CreateIndex(
                name: "IX_Products_PossibleDimensionsID",
                table: "Products",
                column: "PossibleDimensionsID");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ProductCategoryID",
                table: "Products",
                column: "ProductCategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_Restriction_ProductRelationshipID",
                table: "Restriction",
                column: "ProductRelationshipID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Finishes");

            migrationBuilder.DropTable(
                name: "Float");

            migrationBuilder.DropTable(
                name: "PriceEntries");

            migrationBuilder.DropTable(
                name: "ProductCatalogRelationships");

            migrationBuilder.DropTable(
                name: "ProductCollectionRelationships");

            migrationBuilder.DropTable(
                name: "ProductMaterialRelationships");

            migrationBuilder.DropTable(
                name: "Restriction");

            migrationBuilder.DropTable(
                name: "Catalogs");

            migrationBuilder.DropTable(
                name: "Collections");

            migrationBuilder.DropTable(
                name: "Materials");

            migrationBuilder.DropTable(
                name: "ProductRelationships");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "PossibleDimensions");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "PossibleValues");
        }
    }
}
