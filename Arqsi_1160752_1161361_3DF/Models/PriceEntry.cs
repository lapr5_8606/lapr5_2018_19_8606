using System;

namespace Arqsi_1160752_1161361_3DF.Models
{
    public class PriceEntry
    {
        public int ID { get; set; }
        
        public string ChangedOn { get; set; }
        
        public double Value { get; set; }

        protected PriceEntry()
        {
            
        }

        public PriceEntry(double value)
        {
            ChangedOn = DateTime.Now.ToString("d/M/yyyy");
            Value = value;
        }
    }
}