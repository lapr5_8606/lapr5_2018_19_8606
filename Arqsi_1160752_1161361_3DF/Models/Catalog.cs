using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Arqsi_1160752_1161361_3DF.Models
{
    public class Catalog
    {
        //ID of the catalog
        public int ID {get;set;}

        //name of the catalog
        public string Name {get;set;}

         public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Catalog catalog = (Catalog)obj;
                return (
                    Name == catalog.Name);
            }
        }

        public override int GetHashCode() => HashCode.Combine(Name);

    }
}