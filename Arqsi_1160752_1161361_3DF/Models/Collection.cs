using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Arqsi_1160752_1161361_3DF.Models
{
    public class Collection
    {
        //Id of the Collection
        public int ID { get; set; }

        //Name of the Collection
        public string Name { get; set; }

        public string AestheticLine { get; set; }

    }
}